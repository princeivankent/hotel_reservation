<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ReceptionistController extends Controller
{
    public function index()
    {
        return response()->json(User::where('user_type', 'receptionist')->get());
    }

    public function show($user_id)
    {
        return response()->json(User::findOrFail($user_id));
    }

    public function store(Request $request)
    {
        $query = new User;
        $query->name = $request->name;
        $query->email = $request->email;
        $query->user_type = 'receptionist';
        $query->password = bcrypt($request->password);
        $query->save();

        return response()->json($query);
    }

    public function update(Request $request, $user_id)
    {
        $query = User::findOrFail($user_id);
        $query->name = $request->name;
        $query->email = $request->email;
        $query->user_type = 'receptionist';

        if ($request->password) {
            $query->password = bcrypt($request->password);
        }

        $query->save();

        return response()->json($query);
    }

    public function destroy($user_id)
    {
        $query = User::findOrFail($user_id);
        $query->delete();

        return response()->json($query);
    }
}
