<v-layout
column
wrap
class="my-5"
>
    <v-flex xs12 sm4 class="my-3">
        <div class="text-xs-center">
            <h2 class="headline">We provide the best suites to your need</h2>
        </div>
    </v-flex>

    <v-flex xs12>
        <v-container grid-list-xl>
            <v-layout row wrap>
                <v-flex xs12 md4>
                    <v-card flat class="transparent">
                        <v-card-text class="text-xs-center">
                            <v-icon x-large class="blue--text text--lighten-2">ion ion-android-home</v-icon>
                        </v-card-text>
                        <v-card-title primary-title class="layout justify-center" style="margin-top: -25px;">
                            <div class="headline font-weight-thin text-xs-center">Convenient Place</div>
                        </v-card-title>
                        <v-carousel
                            delimiter-icon="stop"
                            prev-icon="mdi-arrow-left"
                            next-icon="mdi-arrow-right"
                            style="height: 350px;"
                        >
                            <v-carousel-item
                                v-for="(item,i) in place"
                                :key="i"
                                :src="item.src"
                            ></v-carousel-item>
                        </v-carousel>
                    </v-card>
                </v-flex>
                <v-flex xs12 md4>
                    <v-card flat class="transparent">
                        <v-card-text class="text-xs-center">
                            <v-icon x-large class="blue--text text--lighten-2">ion ion-ios-body</v-icon>
                        </v-card-text>
                        <v-card-title primary-title class="layout justify-center" style="margin-top: -25px;">
                            <div class="headline font-weight-thin text-xs-center">Spacius</div>
                        </v-card-title>
                        <v-carousel
                            delimiter-icon="stop"
                            prev-icon="mdi-arrow-left"
                            next-icon="mdi-arrow-right"
                            style="height: 350px;"
                        >
                            <v-carousel-item
                                v-for="(item,i) in spacius"
                                :key="i"
                                :src="item.src"
                            ></v-carousel-item>
                        </v-carousel>
                    </v-card>
                </v-flex>
                <v-flex xs12 md4>
                    <v-card flat class="transparent">
                        <v-card-text class="text-xs-center">
                            <v-icon x-large class="blue--text text--lighten-2">ion ion-model-s</v-icon>
                        </v-card-text>
                        <v-card-title primary-title class="layout justify-center"  style="margin-top: -25px;">
                            <div class="headline font-weight-thin text-xs-center">Wide Road</div>
                        </v-card-title>
                        <v-carousel
                            delimiter-icon="stop"
                            prev-icon="mdi-arrow-left"
                            next-icon="mdi-arrow-right"
                            style="height: 350px;"
                        >
                            <v-carousel-item
                                v-for="(item,i) in road"
                                :key="i"
                                :src="item.src"
                            ></v-carousel-item>
                        </v-carousel>
                    </v-card>
                </v-flex>
            </v-layout>
        </v-container>
    </v-flex>
</v-layout>