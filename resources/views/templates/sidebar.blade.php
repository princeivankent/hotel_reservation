

		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
	
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="{{ asset('admin-images/avatar5.png') }}" class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info white--text">
					<p>{{ Auth::user()->name }}</p>
					<!-- Status -->
					<a href="#">
						<i class="fa fa-circle text-success"></i>
						@if(Auth::user()->user_type == 'admin')
							Administrator
						@else
							Receptionist
						@endif
					</a>
				</div>
			</div>
	
			<!-- search form (Optional) -->
			<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					</button>
				</span>
			</div>
			</form>
			<!-- /.search form -->
	
			<!-- Sidebar Menu -->
			<ul class="sidebar-menu" data-widget="tree">
				<li id="_dashboard"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> 
					<span>Dashboard</span></a>
				</li>

				@if (Auth::user()->user_type == 'admin')
				<li class="header text-center"><strong>ROOMS & UTILITIES</strong></li>
				<li id="_room_types_v2"><a href="{{ route('room_types_v2') }}"><i class="fa fa-caret-right"></i> <span>Room Types</span></a></li>
				<li id="_rooms"><a href="{{ route('rooms') }}"><i class="fa fa-caret-right"></i> <span>Rooms</span></a></li>
				<li id="_room_rates"><a href="{{ route('room_rates') }}"><i class="fa fa-caret-right"></i> <span>Room Rates</span></a></li>
				<li id="function_room_tab"><a href="{{ url('admin/function_rooms') }}"><i class="fa fa-caret-right"></i> <span>Function Rooms</span></a></li>
				
				<li class="header text-center"><strong>CUSTOMER'S TRANSACTIONS</strong></li>
				<li id="billing_tab"><a href="{{ route('billing_information') }}"><i class="fa fa-caret-right"></i> <span>Billing Information</span></a></li>

				<li class="header text-center"><strong>WEBSITES'S CONTENT</strong></li>
				<li id="contact_us_tab"><a href="{{ url('/admin/contact_us') }}"><i class="fa fa-caret-right"></i> <span>Contact Us</span></a></li>
				<li id="comment_tab"><a href="{{ url('/admin/comments') }}"><i class="fa fa-caret-right"></i> <span>Comments</span></a></li>
				<li id="faq_tab"><a href=""><i class="fa fa-caret-right"></i> <span>Frequently Asked Questions</span></a></li>
				<li id="page_content_tab"><a href="{{ url('/admin/page_content') }}"><i class="fa fa-caret-right"></i> <span>Page Content</span></a></li>
				
				<li class="header text-center"><strong>Employees</strong></li>
				<li id="receptionist_tab"><a href="{{ url('/admin/receptionist') }}"><i class="fa fa-caret-right"></i> <span>Receptionist</span></a></li>

				<li class="header text-center"><strong>ADMINISTRATOR'S BANK ACCOUNT</strong></li>
				<li id="bank_account_tab"><a href="{{ url('/admin/bank_accounts') }}"><i class="fa fa-caret-right"></i> <span>Bank Account Number</span></a></li>
				@else
				<li class="header text-center"><strong>ROOMS & UTILITIES</strong></li>
				<li id="_room_types_v2"><a href="{{ route('room_types_v2') }}"><i class="fa fa-caret-right"></i> <span>Room Types</span></a></li>
				<li id="_rooms"><a href="{{ route('rooms') }}"><i class="fa fa-caret-right"></i> <span>Rooms</span></a></li>
				<li id="_room_rates"><a href="{{ route('room_rates') }}"><i class="fa fa-caret-right"></i> <span>Room Rates</span></a></li>
				<li id="function_room_tab"><a href="{{ url('admin/function_rooms') }}"><i class="fa fa-caret-right"></i> <span>Function Rooms</span></a></li>

				<li class="header text-center"><strong>CUSTOMER'S TRANSACTIONS</strong></li>
				<li id="billing_tab"><a href="{{ route('billing_information') }}"><i class="fa fa-caret-right"></i> <span>Billing Information</span></a></li>
				@endif
			</ul>
		</section>