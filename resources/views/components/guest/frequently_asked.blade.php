@extends('welcome')

@push('styles')
	<style>
		.card--flex-toolbar {
			margin-top: -64px;
		}

		.swal-title {
			font-family: sans-serif;
		}

		.swal-text {
			font-family: sans-serif;
			text-align: center;
		}

		.swal-button {
			font-family: sans-serif;
			background-color: #A5DC86;
		}
	</style>
@endpush

@section('content')
<section>
    <v-container grid-list-xl>
        <v-layout row wrap justify-center class="mt-5">
            <v-flex xs12 md6>
                <v-card
                    class="text-md-center font-weight-light title"
                    color="grey lighten-4"
                    flat
                    hover
                    dark
                >
                    <v-toolbar color="#21130A">
                        <v-spacer></v-spacer>
                        <v-icon x-large color="orange">ion ion-ios-help</v-icon>
                        <v-toolbar-title primary-title class="headline font-weight-regular">
                            FREQUENTLY ASKED QUESTIONS
                        </v-toolbar-title>
                        <v-spacer></v-spacer>
                    </v-toolbar>

                    <v-list two-line light>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">How do I reserve a room?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    First is you need choose room and upload deposit slip to be approved by Admin</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
    
                        <v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>


                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Where is the hotel located? How will I get there?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    Brgy San Lucas 1, Alaminos, Laguna, on our web page showing the location of our hotel.</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
    
                        <v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">How can I know if my Reservation are approved?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    After you sign in you need to click the Booking Status to See if your reservation is approved </v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>

                        <v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Is there swimming pool ?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    We don't have one</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>

                        <v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">How do I view my Reservation status?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    You can see on Guest Page on Name on Top Bar</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>

                        <v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Do I pay when I reserve? or after Check-in</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    You are Required to pay 50% on Bank to Bank</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
						
						
						<v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Do you have Function Hall?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    Yes, In our main Hotel Ricardo Hotel</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
						
						
						
						<v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">How many room types do you have?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    We only have 6 Types of Rooms</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
						
						
						<v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Can I reserve room for someone else?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    Yes, but it will be under your name</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
						
						
						<v-divider></v-divider>
                        <v-list-tile v-on:click="">
                            <v-list-tile-action>
                                <v-icon color="green">ion-android-checkmark-circle</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                                <v-list-tile-title style="font-size: 18px;">Is Paradise Hotel use Standard Check-in and Check-out?</v-list-tile-title>
                                <v-list-tile-sub-title class="mt-2" style="font-size: 16px;">
                                    <strong>Answer:</strong>
                                    Yes, our Hotel uses Standard Check-in</v-list-tile-sub-title>
                            </v-list-tile-content>
                        </v-list-tile>
    
    
                        <v-divider></v-divider>
                    </v-list>
                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
</section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        new Vue({
            el: '#app',
            data() {
                return {
					
                }
			},
			created() {
				
			},
			methods: {
				
			}
        })
    </script>
@endpush