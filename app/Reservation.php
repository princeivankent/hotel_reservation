<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['room_schedule_id','room_id','transaction_type_id','user_id'];
    protected $primaryKey = 'reservation_id';
    public $timestamps = false;
    
    public function room_schedule()
    {
        return $this->belongsTo('App\RoomSchedule', 'room_schedule_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function room_addons()
    {
        return $this->hasMany('App\RoomAddOn', 'room_schedule_id', 'room_schedule_id');
    }

    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id', 'room_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Payment', 'payment_id', 'payment_id');
    }
}
