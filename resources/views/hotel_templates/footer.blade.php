<v-footer color="#190E08">
    <v-layout row wrap align-center>
        <v-flex xs12>
            <div class="white--text ml-3 text-xs-center">
                <strong>Copyright &copy; 2018<a class="white--text text--darken-2" href="http://paradise.com">Paradise Hotel</a>.</strong> All rights reserved.
            </div>
        </v-flex>
    </v-layout>
</v-footer>