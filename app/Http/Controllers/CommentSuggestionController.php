<?php

namespace App\Http\Controllers;

use App\Services\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommentSuggestionController extends Controller
{
    protected $mail;

    public function __construct(SendEmail $mail)
    {
        $this->mail = $mail;
    }

    public function send(Request $request)
    {
        $data = [
            'subject'	        => 'Comments & Suggestions',
            'sender'	        => config('mail.from.address'),
            'recipient'	        => 'admin@gmail.com',
            'cc'	            => NULL,
            'attachment'	    => NULL,
            'content'	        => [
                'title' => 'Customer\'s <span style="color: #5caad2;">Comments & Suggestions</span>',
                'message' => $request->message
            ]
        ];

        $mail = Mail::send('emails.comment_suggestion', ['content' => $data['content']], function ($mail) use ($data) {
            $mail->from($data['sender'], 'Paradise Hotel Reservation System');
            $mail->to($data['recipient'])->subject($data['subject']);
            
            if (isset($data['cc'])) $mail->cc($data['cc']);
            if (isset($data['attachment'])) $mail->attach($data['attachment']);
        });

        return response()->json($mail);
    }
}
