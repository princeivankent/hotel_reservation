<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1->0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
</head>
<body>
    {{-- <div class="page-break"></div> --}}
    <section class="invoice">
        <div class="row" style="margin-left: 10px; margin-right: 10px;">
            <div class="col-md-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Paradise Hotel
                    <small class="pull-right">{{ Carbon\Carbon::parse(now())->format('M d, Y') }}</small>
                </h2>
            </div>
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    <address>
                        Guest: <strong>{{ $header['guest'] }}</strong><br>
                        Email: {{ $header['email'] }}<br>
                        Check In: {{ $header['checkin'] }}<br>
                        Check Out: {{ $header['checkout'] }}<br>
                    </address>
                </div>
                <div class="col-sm-6 invoice-col">
                    <b>Official Receipt: #{{ $header['official_receipt_code'] }}</b><br>
                </div>
            </div>
    
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Days</th>
                                <th>Amount Per Day</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list as $item)
                                <tr>
                                    <td>{{ $item['date'] }}</td>
                                    <td>{{ $item['description'] }}</td>
                                    <td>{{ $item['days'] }}</td>
                                    <td>P {{ $item['amount_per_day']}}</td>
                                    <td>P {{ $item['total']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    
            <div class="row">
                <div class="col-xs-6">
                    
                </div>
                <div class="col-xs-6">
                    
                </div>
            </div>

            <div class="row" style="margin-top: 30px;">
                <div class="col-xs-4 text-center">
                    <strong>{{ Auth::user()->name }}</strong>
                    <hr style="margin-bottom: 0px; margin-top: 5px; font-size: 24px;">
                    Cashier
                </div>
                <div class="col-xs-4">
                    
                </div>
                <div class="col-xs-4 text-center">
                    <strong>{{ $header['guest'] }}</strong>
                    <hr style="margin-bottom: 0px; margin-top: 5px; font-size: 24px;">
                    Guest
                </div>
            </div>
        </div>
    </section>
</body>
</html>