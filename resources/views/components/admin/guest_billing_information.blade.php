@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/all.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/bootstrap-colorpicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/bootstrap-timepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/select2.min.css') }}">
@endpush

@section('content')
	<section class="content-header">
		<h1>
			Billing Information System
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="{{ route('billing_information') }}">
					<i class="fa fa-chevron-left"></i> Billing Information System
				</a>
			</li>
			<li class="active">Here</li>
		</ol>
	</section>
	
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fa fa-user-o"></i></span>
		
					<div class="info-box-content">
						<span class="info-box-text">Guest</span>
						<span class="info-box-number">@{{ items.name }}</span>

						<div class="progress">
							<div class="progress-bar" style="width: 0%"></div>
						</div>
						<span class="progress-description">
							@{{ items.email }}
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-green">
				<span class="info-box-icon"><i class="fa fa-building-o"></i></span>
	
				<div class="info-box-content">
					<span class="info-box-text">Room Details</span>
					<span class="info-box-number">@{{ items.room_name }}</span>
	
					<div class="progress">
					<div class="progress-bar" style="width: 0%"></div>
					</div>
					<span class="progress-description">
						Room Code: <strong>@{{ items.room_code }}</strong>
					</span>
				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-yellow">
				<span class="info-box-icon"><i class="fa fa-calendar-check-o"></i></span>
	
				<div class="info-box-content">
					<span class="info-box-text">Checkin In</span>
					<span class="info-box-number">@{{ items.checkin }}</span>
	
					<div class="progress">
					<div class="progress-bar" style="width: 0%"></div>
					</div>
					<span class="progress-description">
						Time IN: <strong>@{{ items.timein }}</strong>
					</span>
				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-red">
				<span class="info-box-icon"><i class="fa fa-calendar-minus-o"></i></span>
	
				<div class="info-box-content">
					<span class="info-box-text">Check Out</span>
					<span class="info-box-number">@{{ items.checkout }}</span>
	
					<div class="progress">
					<div class="progress-bar" style="width: 0%"></div>
					</div>
					<span class="progress-description">
						Time OUT: <strong>@{{ items.timeout }}</strong>
					</span>
				</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h1 class="box-title"><i class="fa fa-building"></i> Room Cost</h1>
					</div>
					<div class="box-body">
						<table id="addon_table" class="table">
							<thead>
								<tr>
									<th class="text-center" width="40">#</th>
									<th class="text-center">ROOM TYPE</th>
									<th class="text-center">TOTAL HOURS</th>
									<th class="text-center">COST</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">@{{ 1 }}</td>
									<td class="text-center text-primary">@{{ items.room_name }}</td>
									<td class="text-center text-primary">24</td>
									<td class="text-center text-danger">@{{ items.total_cost | phCurrency }}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th class="text-success text-right">Total Cost:</th>
									<th class="text-danger text-center">@{{ items.total_cost | phCurrency }}</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class="box box-success">
					<div class="box-header with-border">
						<h1 class="box-title"><i class="fa fa-plus-circle"></i> Addons</h1>
					</div>
					<div class="box-body">
						<div v-if="booking_addons.length === 0" class="alert alert-danger">
							<i class="fa fa-warning"></i>
							No Addons
						</div>
						<table v-else id="addon_table" class="table">
							<thead>
								<tr>
									<th class="text-center" width="40">#</th>
									<th class="text-center">DESCRIPTION</th>
									<th class="text-center">QUANTITY</th>
									<th class="text-center">COST</th>
								</tr>
							</thead>
							<tbody>
								<tr 
								v-for="(item, index) in booking_addons"
								v-bind:key="index">
									<td class="text-center">@{{ index+1 }}</td>
									<td class="text-center text-primary">@{{ item.description }}</td>
									<td class="text-center text-primary">@{{ item.quantity }}</td>
									<td class="text-center text-danger">@{{ item.cost | phCurrency }}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th class="text-success text-right">Total Addons:</th>
									<th class="text-danger text-center">@{{ addon_costs | phCurrency }}</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class="box box-success">
					<div class="box-header with-border">
						<h1 class="box-title"><i class="fa fa-dollar"></i> Grand Total</h1>
					</div>
					<div class="box-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label style="margin-top: 7px;"><i class="fa fa-caret-right"></i> Reservation Type</label>
								</div>
								<div class="col-md-6">
									{{-- <input type="text" class="form-control" v-model="items.reservation_type == 'long_time' ? 'Daily Rate' : 'Hourly Rate'" readonly> --}}
									<input type="text" class="form-control" value="Daily Rate" readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label style="margin-top: 7px;"><i class="fa fa-caret-right"></i> Transaction Type</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control" v-model="items.transaction_type" readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label style="margin-top: 7px;"><i class="fa fa-caret-right"></i> Payment Status</label>
								</div>
								<div class="col-md-6 text-danger">
									{{-- <input type="text" class="form-control" v-model="items.payment_status == 'fully_paid' ? 'Fully paid' : 'Half Paid'" readonly> --}}
									
									<!-- checkbox -->
									<div class="form-group">
											<div class="input-group">
												<input type="text" 
												class="form-control" 
												v-model="items.payment_status == 'fully_paid' ? 'Fully paid' : 'Half Paid'"	
												readonly>

												<div class="input-group-addon" v-on:click="togglePayment(items.room_schedule_id)" style="cursor: pointer;">
													<i :class="`fa fa-toggle-${items.payment_status == 'fully_paid' ? 'on' : 'off'} fa-lg text-danger`"></i>
												</div>
											</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label><i class="fa fa-caret-right" style="margin-top: 7px;"></i> Total Paid</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control text-bold text-red" :value="totalPaid | phCurrency" readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label><i class="fa fa-caret-right" style="margin-top: 7px;"></i> Grand Total</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control text-bold text-green" :value="grandTotal | phCurrency" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer clearfix">
						<button 
						v-on:click="getBillingInformation"
						class="btn btn-flat btn-success pull-right">
							CHECKOUT GUEST &nbsp;
							<i class="fa fa-sign-out"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</section>

		@include('components.admin.modals.official_receipt')
@endsection

@push('scripts')
	<script src="{{ asset('libraries/admin/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/daterangepicker.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/bootstrap-colorpicker.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/bootstrap-timepicker.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/icheck.min.js') }}"></script>
	<script>
		document.querySelector('#billing_tab').setAttribute('class', 'active');
		var room_schedule_id = "{{ Request::segment('4') }}";
		new Vue({
			el: '#app', 
			mixins: [mixin],
			data() {
				return {
					items: {},
					addons: [],
					booking_addons: [],
					room_costs: 0,
					addon_costs: 0,

					header: {},
					list: []
				}
			},
			watch: {
				// payment_status() {
				// 	alert('haha');
				// }
			},
			computed: {
				payment_status() {
					return this.items.payment_status;
				},
				totalPaid() {
					var sum = this.room_costs + this.addon_costs;
					return this.items.payment_status == 'fully_paid' ? sum : (sum/2);
				},
				grandTotal() {
					return parseInt(this.room_costs + this.addon_costs);
				}
			},
			created() {
				this.getItems();
				this.getAddons(room_schedule_id);
			},
			methods: {
				getItems() {
					axios.get(`${this.base_url}/admin/billing_information/get/${room_schedule_id}`)
					.then(({data}) => {
						this.items = data.billing;
						this.addons = data.addons;
						this.room_costs = data.billing.total_cost;
					})
					.catch((err) => {
						console.log(err.response);
					});
				},
				getAddons(room_schedule_id) {
					var url = `${this.base_url}/admin/booking_addons/get/${room_schedule_id}`;
					axios.get(url)
					.then(({data}) => {
						this.booking_addons = data;

						var total = 0;
						this.booking_addons.forEach(element => {
							total += element.cost;
						});

						this.addon_costs = total;
						$('#booking_addons_modal').modal('show');
					})
					.catch((err) => {
						console.log(err.response);
					});
				},
				checkOut() {
					$('#official_receipt_modal').modal('show');
				},
				togglePayment(room_schedule_id) {
					swal({
						title: "Are you sure?",
						text: "Payment status will be updated.",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then(res => {
						if (res) {
							var status = this.items.payment_status == 'fully_paid' ? 'half_paid' : 'fully_paid';

							axios.put(`${this.base_url}/admin/update_payment_status/update/${room_schedule_id}`, {status: status})
							.then(({data}) => {
								this.getItems();
								swal("Success!", "Payment status has been updated", "success");
							})
							.catch((err) => {
								console.log(err.response);
							});
						}
					});
				},
				getBillingInformation() {
					axios.get(`${this.base_url}/admin/official_receipt/get/${room_schedule_id}`)
					.then(({data}) => {
						this.header = data.header;
						this.list = data.list;

						$('#official_receipt_modal').modal('show');
					})
					.catch((error) => {
						console.log(error.response);
					});
				}
			}
		});
	</script>
@endpush