@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-list-ul"></i>
            Page Content
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <button v-on:click="saveItem"
                class="btn btn-flat btn-success text-bold pull-right">
                    <i class="fa fa-check-circle"></i>&nbsp;
                    SAVE CHANGES
                </button>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>MISSION</label>
                            <textarea class="form-control" 
                            name="mission_vision" id="mission_vision" 
                            cols="30" rows="10" v-model="item.mission_vision"></textarea>
                        </div>
                        <div class="form-group">
                            <label>VISION</label>
                            <textarea class="form-control" 
                            name="history" id="history" 
                            cols="30" rows="10" v-model="item.history"></textarea>
                        </div>
                        <div class="form-group">
                            <label>About Our Hotel</label>
                            <textarea class="form-control" 
                            name="about_our_hotel" id="about_our_hotel" 
                            cols="30" rows="10" v-model="item.about_our_hotel"></textarea>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        document.querySelector('#page_content_tab').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    item: {},
                    form: {},
                    isEdit: 0,
                    formTitle: 'Add/Replace Information',
                    errors: {}
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                getItems() {
                    axios.get(`${this.base_url}/admin/page_content/get`)
                    .then(({data}) => {
                        this.item = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                getItem(contact_us_id) {
                    axios.get(`${this.base_url}/admin/contact_us/get/${contact_us_id}`)
                    .then(({data}) => {
                        $('#contact_us_modal').modal('show');
                        this.isEdit = 1;
                        this.form = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                saveItem() {
                    // if (this.isEdit) {
                    //     axios.put(`${this.base_url}/admin/contact_us/update/${this.form.contact_us_id}`, this.form)
                    //     .then(({data}) => {
                    //         this.openCreateModal();
                    //         this.resetModalForm();
                    //         this.getItems();
                    //         toastr.success('', 'Succcessfully Saved!');
                    //     })
                    //     .catch((error) => {
                    //         this.errors = error.response.data.errors;
                    //         console.log(error.response);
                    //     });
                    // }
                    // else {
                        axios.post(`${this.base_url}/admin/page_content/store`, this.item)
                        .then(({data}) => {
                            this.getItems();
                            toastr.success('', 'Succcessfully Saved!');
                        })
                        .catch((error) => {
                            this.errors = error.response.data.errors;
                            console.log(error.response);
                        });
                    // }
                },
                deleteItem(contact_us_id) {
                    swal({
						title: "Delete this item",
						text: "Note: There's no way to undo changes",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then(res => {
						if (res) {
							axios.delete(`${this.base_url}/admin/contact_us/destroy/${contact_us_id}`)
							.then(({data}) => {
								if (data) {
									this.getItems();
									toastr.success('Successfull Saved!');
								}
							})
							.catch((error) => {
								console.log(error.response);
							});
						}
					});
                },
                //
                resetModalForm() {
                    this.form = {};
                    this.isEdit = 0;
                    this.formTitle = 'Replace Information';
                    $('#contact_us_modal').modal('hide');
                },
                openCreateModal() {
                    $('#contact_us_modal').modal('show');
                    this.form = {};
                    this.isEdit = 0;
                    this.formTitle = 'Add/Replace Information';
                },
                openEditModal() {
                    $('#contact_us_modal').modal('show');
                    this.form = {};
                    this.isEdit = 1;
                    this.formTitle = 'Edit Information';
                },
            }
        });
    </script>
@endpush