@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i>
            Billing Information System
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center text-uppercase">RM Sched ID</th>
                            <th class="text-center text-uppercase">Guest</th>
                            <th class="text-center text-uppercase">Email</th>
                            <th class="text-center text-uppercase">Room Type</th>
                            <th class="text-center text-uppercase">Room Code</th>
                            <th class="text-center text-uppercase bg-green ">Check In</th>
                            <th class="text-center text-uppercase bg-yellow">Time In</th>
                            <th class="text-center text-uppercase bg-green ">Check Out</th>
                            <th class="text-center text-uppercase bg-yellow">Time Out</th>
                            <th class="text-center text-uppercase">In/Out Status</th>
                            <th class="text-center text-uppercase">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items">
                            <td class="text-center">@{{ item.room_schedule_id }}</td>
                            <td class="text-center text-success text-bold">@{{ item.name }}</td>
                            <td class="text-center">@{{ item.email }}</td>
                            <td class="text-center text-success">@{{ item.room_name }}</td>
                            <td class="text-center">@{{ item.room_code }}</td>
                            <td class="text-center bg-success">@{{ item.checkin }}</td>
                            <td class="text-center bg-warning">@{{ item.timein }}</td>
                            <td class="text-center bg-success">@{{ item.checkout }}</td>
                            <td class="text-center bg-warning">@{{ item.timeout }}</td>
                            <td class="text-center">
                                <span v-if="item.did_checkout == null" class="label label-warning" >
                                    <i class="fa fa-exclamation" style="margin-right: 4px;"></i>
                                    waiting
                                </span>
                                <span v-else-if="item.did_checkout == 0" class="label label-primary">
                                    <i class="fa fa-check"></i>
                                    Checked In
                                </span>
                                <span v-else class="label label-success">
                                    <i class="fa fa-check"></i>
                                    Checked Out
                                </span>
                            </td>
                            <td class="text-center">
                                <a 
                                :href="`{{ url('admin/guest_billing_information/room_schedule_id') }}/${item.room_schedule_id}`"
                                class="btn btn-sm btn-flat btn-default">
                                    OPEN FOLDER
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        document.querySelector('#billing_tab').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    items: []
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                getItems() {
                    axios.get(`${this.base_url}/admin/billing_information/get`)
                    .then(({data}) => {
                        this.items = data;

                        setTimeout(() => {
                            $('#table').DataTable();   
                        });
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },
                billingInfo(room_schedule_id, index) {
                    alert(room_schedule_id);
                }
            }
        });
    </script>
@endpush