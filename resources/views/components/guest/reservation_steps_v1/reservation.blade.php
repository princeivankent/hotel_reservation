<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="form-group mt-5">
            <label>Check In Date:</label>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="date" 
                class="form-control" 
                v-model="form.checkin"
                style="padding-top: 0px;" autofocus required>
            </div>
        </div>
        <div class="form-group">
            <label>Check Out Date</label>

            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="date" 
                class="form-control" 
                v-model="form.checkout"
                style="padding-top: 0px;" autofocus required>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <button class="btn btn-default text-bold shadow" v-on:click="openBookedRooms">
                SEE AVAILABLE ROOMS
            </button>
            <button v-on:click="nextTab()" class="btn btn-success text-bold shadow">
                NEXT&nbsp;
                <i class="fa fa-arrow-circle-right"></i>
            </button>
        </div>
    </div>
</div>