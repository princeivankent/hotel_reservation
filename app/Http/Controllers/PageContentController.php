<?php

namespace App\Http\Controllers;

use App\PageContent;
use Illuminate\Http\Request;

class PageContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(PageContent::latest()->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'mission_vision' => 'required|string',
            'history' => 'required|string',
            'about_our_hotel' => 'required|string'
        ]);

        $query = PageContent::updateOrCreate([
            'page_content_id' => $request->page_content_id,
            'mission_vision' => $request->mission_vision,
            'history' => $request->history,
            'about_our_hotel' => $request->about_our_hotel,
        ]);

        return 200;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($page_content_id)
    {
        return response()->json(PageContent::findOrFail($page_content_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'mission_vision' => 'required|string',
            'history' => 'required|string',
            'about_our_hotel' => 'required|string'
        ]);

        $query = PageContent::findOrFail($page_content_id);
        $query->mission_vision = $request->mission_vision;
        $query->history = $request->history;
        $query->about_our_hotel = $request->about_our_hotel;
        $query->save();

        return 200;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($page_content_id)
    {
        $query = PageContent::findOrFail($page_content_id);
        $query->delete();

        return 201;
    }

    public function frequently_asked_questions()
    {
        
    }
}
