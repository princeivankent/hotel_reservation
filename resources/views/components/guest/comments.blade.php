@extends('welcome')

@push('styles')
	<style>
		.card--flex-toolbar {
			margin-top: -64px;
		}

		.swal-title {
			font-family: sans-serif;
		}

		.swal-text {
			font-family: sans-serif;
			text-align: center;
		}

		.swal-button {
			font-family: sans-serif;
			background-color: #A5DC86;
		}
	</style>
@endpush

@section('content')
<section>
	<v-container grid-list-xl>
		<v-card flat class="transparent">
			<v-layout row wrap>
				<v-flex xs12 sm12 md8 offset-xs2>
					<v-card>
						<v-toolbar card prominent>
							<v-toolbar-title class="body grey--text">Comments and Suggestions</v-toolbar-title>
						</v-toolbar>
						
						<v-divider></v-divider>
						
						<v-card-text>
							<v-textarea
                            outline
                            name="input-7-4"
                            label="Outline textarea"
                            v-model="message"
                            ></v-textarea>
                            <v-btn color="primary" v-on:click="send">
                                <v-icon>fa fa-send</v-icon>&nbsp;
                                Send
                            </v-btn>
						</v-card-text>
					</v-card>
				</v-flex>
			</v-layout>
			<v-layout row wrap>
				<v-flex xs12 sm12 md8 offset-xs2>
					<v-card v-for="(comment, index) in user_comments" class="mb-3" light>
						<v-card-title>
							<h6 class="title mb-0">
								<i class="ion ion-person"></i>&nbsp;
								@{{ comment.name }} |
								<small class="grey--text">@{{ comment.created_at | dateTimeFormat }}</small>
							</h6>
						</v-card-title>
						<v-card-text style="margin-top: -20px;">
							- @{{ comment.comment }}
						</v-card-text>
					</v-card>
				</v-flex>
			</v-layout>
		</v-card>
	</v-container>
</section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        var base_url = "{{ url('/') }}";
        new Vue({
            el: '#app',
            data() {
                return {
					message: '',
					user_comments: []
                }
			},
			created() {
				this.comments();
			},
			methods: {
                send() {
					// axios.post(`${this.base_url}/guest/comments_suggestions/post`, {message: this.message})
					// .then(({data}) => {
					// 	console.log(data);
					// })
					// .catch((error) => {
					// 	console.log(error.response);
					// });
					var data = {
						user_id: user_id,
						comment: this.message
					}
					axios.post(`${this.base_url}/guest/comments/store`, data)
					.then(({data}) => {
						if (data) {
							swal('Alright!', 'Your comment and suggestion is successfully sent.', 'success', {button:false,timer:4000});
						}
					})
					.catch((error) => {
						console.log(error.response);
						swal('Ooops!', 'Something went wrong please contact the developer. Thanks', 'error', {button:false,timer:4000});
					});
				},
				comments() {
					axios.get(`${this.base_url}/guest/comments/get`)
					.then(({data}) => {
						this.user_comments = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
				}
			}
        })
    </script>
@endpush