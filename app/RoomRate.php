<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomRate extends Model
{
    protected $fillable = ['room_type_id','cost','hours'];
    protected $primaryKey = 'room_rate_id';
    public $timestamps = false;

    public function room_rate()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }
}
