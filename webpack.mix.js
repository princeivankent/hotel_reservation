let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'js')
//    .sass('resources/assets/sass/app.scss', 'css');


mix.js('resources/assets/js/app.js', 'js').sourceMaps();
mix.js('resources/assets/js/vue.js', 'js/vue.js').sourceMaps();
mix.js('resources/assets/js/vuetify.js', 'js/vuetify.js').sourceMaps();