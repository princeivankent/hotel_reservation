<div class="modal fade" tabindex="-1" role="dialog" id="receptionist_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@{{ formTitle }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Receptionist Name</label>
                    <input type="text" class="form-control" v-model="form.name">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" v-model="form.email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="email" class="form-control" v-model="form.password">
                </div>
            </div>
            <div class="modal-footer">
                <button v-on:click="save" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>