<div class="modal fade" tabindex="-1" role="dialog" id="contact_us_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i>
                    @{{ formTitle }}
                </h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" v-model="form.contact_us_id" name="contact_us_id" id="contact_us_id">
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" v-model="form.contact_number">

                        <span v-if="errors.contact_number" class="text-danger text-bold">
                            @{{ errors.contact_number[0] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" 
                        name="address" id="address" 
                        cols="30" rows="2" v-model="form.address"></textarea>

                        <span v-if="errors.address" class="text-danger text-bold">
                            @{{ errors.address[0] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" id="email" v-model="form.email">

                        <span v-if="errors.email" class="text-danger text-bold">
                            @{{ errors.email[0] }}
                        </span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>&nbsp;
                    CLOSE
                </button>
                <button v-on:click="saveItem" type="button" class="btn btn-sm btn-success">
                    <i class="fa fa-check-circle"></i>&nbsp;
                    Save Changes
                </button>
            </div>
        </div>
    </div>
</div>