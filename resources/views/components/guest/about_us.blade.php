@extends('welcome')

@section('content')
    <section>
        <v-container grid-list-xl>

            <v-layout row wrap justify-center class="my-5">
                <v-flex xs12 md6>
                    <v-card flat class="transparent text-md-center font-weight-light title">
                        <v-card-title primary-title class="layout justify-center">
                            <div class="headline" id="about_us">
                                <v-icon class="red--text mb-1">ion ion-flag</v-icon>&nbsp;
                                MISSION
                            </div>
                        </v-card-title>
                        <v-divider></v-divider>
                        <v-card-text class="font-weight-regular text-md-center white--text">
                            @{{ item.mission_vision }}
                        </v-card-text>
                    </v-card>
                </v-flex>
            </v-layout>

            <v-layout row wrap justify-center class="my-5">
                <v-flex xs12 md6>
                    <v-card flat class="transparent text-md-center font-weight-light title">
                        <v-card-title primary-title class="layout justify-center">
                            <div class="headline" id="about_us">
                                <v-icon class="red--text mb-1">ion ion-filing</v-icon>&nbsp;
                                VISION
                            </div>
                        </v-card-title>
                        <v-divider></v-divider>
                        <v-card-text class="font-weight-regular text-md-center white--text">
                            @{{ item.history }}
                        </v-card-text>
                    </v-card>
                </v-flex>
            </v-layout>

            <v-layout row wrap justify-center>
                <v-flex xs12 md6>
                    <v-card flat class="transparent text-md-center font-weight-light title">
                        <v-card-title primary-title class="layout justify-center">
                            <div class="headline" id="about_us">
                                <v-icon class="red--text mb-1">ion ion-android-compass</v-icon>&nbsp;
                                OUR LOCATION
                            </div>
                        </v-card-title>
                        <v-divider></v-divider>
                        <v-card-text>
                            <iframe src="https://www.google.com/maps/embed?pb=!4v1535352329412!6m8!1m7!1s6xMDwYvq1dHcOIBwi2lTvA!2m2!1d14.06517147533128!2d121.2850134952949!3f159.04366364124274!4f2.6435112283795803!5f0.4000000000000002" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </v-card-text>
                    </v-card>
                </v-flex>
            </v-layout>
            
        </v-container>
    </section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        var base_url = "{{ url('/') }}";
        new Vue({
            el: '#app',
            data() {
                return {
                    about_us: '',
                    mission_vision: '',
                    history: '',
                    item: {}
                }
            },
            created() {
                this.about_tab = 'white';
                this.getAboutUs();
            },
            methods: {
                getAboutUs() {
                    axios.get(`${base_url}/guest/page_content/get`)
                    .then(({data}) => {
                        this.item = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                }
            }
        })
    </script>
@endpush