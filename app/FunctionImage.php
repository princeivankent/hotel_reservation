<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionImage extends Model
{
    protected $fillable = ['function_room_id','image'];
    protected $primaryKey = 'function_image_id';
    public $timestamps = false;

    public function function_room()
    {
        return $this->belongsTo('App\FunctionRoom', 'function_room_id', 'function_room_id');
    }
}
