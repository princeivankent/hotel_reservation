<?php

namespace App\Http\Controllers;

use App\AddOnType;
use Illuminate\Http\Request;

class AddOnTypeController extends Controller
{
    public function index()
    {
        $query = AddOnType::where('addon_type', 'guest')->get();
        return response()->json($query);
    }

    public function addons()
    {
        $query = AddOnType::all();
        return response()->json($query);
    }

    public function show($add_on_type_id)
    {
        return response()->json(AddOnType::find($add_on_type_id));
    }

    public function store(Request $request)
    {
        $query = new AddOnType;
        $query->description = $request->description;
        $query->addon_type = $request->addon_type;
        $query->cost = $request->cost;
        $query->save();

        return response()->json($query);
    }

    public function update(Request $request, $add_on_type_id)
    {
        $query = AddOnType::find($add_on_type_id);
        $query->description = $request->description;
        $query->addon_type = $request->addon_type;
        $query->cost = $request->cost;
        $query->save();

        return response()->json($query);
    }

    public function destroy($add_on_type_id)
    {
        return response()->json(AddOnType::find($add_on_type_id)->delete());
    }
}
