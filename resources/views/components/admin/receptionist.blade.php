@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Receptionist
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <button 
                class="btn btn-primary pull-right" v-on:click="create">
                    <i class="fa fa-plus"></i>
                    Add New Receptionist
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" width="100">&nbsp;</th>
                            <th class="text-center">Receptionist</th>
                            <th class="text-center">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items" v-bind:key="index">
                            <td class="text-center">
                                <button v-on:click="destroy(item.user_id)" class="btn btn-sm btn-danger">
                                    Delete Account
                                </button>
                            </td>
                            <td class="text-center">@{{ item.name }}</td>
                            <td class="text-center">@{{ item.email }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@include('components.admin.modals.receptionist_modal')
@endsection

@push('scripts')
    <script>
        document.querySelector('#_room_rates').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    dialog: false,
                    items: [],
                    isEdit: false,
                    errors: {},
                    form: {
                        user_id: 0,
                        name: '',
                        email: '',
                        password: ''                        
                    },
                }
            },
            computed: {
                formTitle () {
                    return this.isEdit == false ? 'New Receptionist' : 'Edit Receptionist';
                }
            },
            created() {
                this.get();
            },
            methods: {
                get(user_id) {
                    if (!user_id) {
                        axios.get(`${this.base_url}/admin/receptionist/get`)
                        .then(({data}) => {
                            this.items = data;
                        })
                        .catch((error) => {
                            console.log(error.response);
                        });
                    }
                    else {
                        axios.get(`${this.base_url}/admin/receptionist/get/${user_id}`)
                        .then(({data}) => {
                            this.form = data;
                            $('#receptionist_modal').modal('show');
                        })
                        .catch((error) => {
                            console.log(error.response);
                        });
                    }
                },
                create() {
                    this.isEdit = 0;
                    $('#receptionist_modal').modal('show');
                },
                save() {
                    if (this.isEdit) {

                    }
                    else {
                        axios.post(`${this.base_url}/admin/receptionist/store`, this.form)
                        .then(({data}) => {
                            if (data) {
                                this.get();
                                swal({
                                    title: "Alright!",
                                    text: 'Successfully saved.',
                                    icon: "success",
                                    timer: 3000
                                });
                            }
                        })
                        .catch((error) => {
                            console.log(error.response);
                        });
                    }
                },
                destroy(user_id) {
                    swal({
						title: "Delete this account?",
						text: "Note: There's no way to undo changes",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then((res) => {
                        if (res) {
							axios.delete(`${this.base_url}/admin/receptionist/destroy/${user_id}`)
                            .then(({data}) => {
                                this.get();
                                swal({
                                    title: "Alright!",
                                    text: 'Successfully deleted.',
                                    icon: "success",
                                    timer: 3000
                                });
                            })
                            .catch((error) => {
                                console.log(error.response);
                            });
						}
					});
                }
            }
        });
    </script>
@endpush