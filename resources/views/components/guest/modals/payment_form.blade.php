<div class="modal fade" tabindex="-1" role="dialog" id="payment_form">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<i class="fa fa-book"></i>
					Payment Form
				</h4>
			</div>
			<div class="modal-body">
				<form 
				action="{{ route('pay') }}" 
				method="post" id="payment-form">

					{{ csrf_field() }}
					<div class="form-row">
						<label for="card-element">
							Credit or debit card
						</label>
						<div id="card-element">
							<!-- A Stripe Element will be inserted here. -->
						</div>
						
						<input type="hidden" name="stripeEmail" v-bind:value="email">
						<input type="hidden" name="amount" v-bind:value="grand_total">
						
						<!-- Used to display form errors. -->
						<div id="card-errors" role="alert"></div>
					</div>
					
					<div class="clearfix">
						<button class="btn btn-success pull-right" style="margin-top: 8px; margin-bottom: 8px;">PAY NOW</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>