@extends('welcome')

@push('styles')

@endpush

@section('content')
<section>
	<v-container grid-list-xl >
		<v-layout row wrap container justify-center>
			<v-flex xs12 sm12 md7>
				<v-card flat color="transparent">
					<v-carousel
						v-if="place.length > 0"
						delimiter-icon="stop"
						prev-icon="mdi-arrow-left"
						next-icon="mdi-arrow-right"
						style="height: 600px;"
					>
						<v-carousel-item
							v-for="(item,index) in place"
							:key="index"
							:src="item.src"
						></v-carousel-item>
					</v-carousel>
				</v-card>
			</v-flex>
			<v-flex xs12 sm12 md3>
				<v-card height="600px">
					<v-list two-line>
						<v-list-tile v-on:click="">
							<v-list-tile-action>
								<v-icon color="green">ion-android-checkmark-circle</v-icon>
							</v-list-tile-action>
							<v-list-tile-content>
								<v-list-tile-sub-title>Room Type</v-list-tile-sub-title>
								<v-list-tile-title>@{{ room_type.room_name }}</v-list-tile-title>
							</v-list-tile-content>

							
						</v-list-tile>
			
						<v-divider></v-divider>
						<v-list-tile v-on:click="">
							<v-list-tile-action>
								<v-icon color="green">ion-android-checkmark-circle</v-icon>
							</v-list-tile-action>
							<v-list-tile-content>
								<v-list-tile-sub-title>Room Amenities</v-list-tile-sub-title>
								<v-list-tile-title>@{{ room_type.room_description }}</v-list-tile-title>
							</v-list-tile-content>
						</v-list-tile>
			
						<v-divider></v-divider>
						<v-list-tile v-on:click="">
							<v-list-tile-action>
								<v-icon color="green">ion-android-checkmark-circle</v-icon>
							</v-list-tile-action>
							<v-list-tile-content>
								<v-list-tile-sub-title>Room Capacity</v-list-tile-sub-title>
								<v-list-tile-title>@{{ room_type.room_capacity }} person(s)</v-list-tile-title>
							</v-list-tile-content>
						</v-list-tile>
			
						<v-divider></v-divider>
						<v-list-tile v-on:click="">
							<v-list-tile-action>
								<v-icon color="green">ion-android-checkmark-circle</v-icon>
							</v-list-tile-action>
							<v-list-tile-content>
								<v-list-tile-sub-title>Available Rooms</v-list-tile-sub-title>
								<v-list-tile-title>@{{ available_rooms }} room(s)</v-list-tile-title>
							</v-list-tile-content>
						</v-list-tile>

						<v-divider></v-divider>

						<v-list-tile v-on:click="">
							<v-list-tile-action>
								<v-icon color="green">ion-android-checkmark-circle</v-icon>
							</v-list-tile-action>
							<v-list-tile-content>
								<v-list-tile-sub-title>Mobile</v-list-tile-sub-title>
								<v-list-tile-title v-if="cost == 'Select room rate below'">@{{ cost }}</v-list-tile-title>
								<v-list-tile-title v-else>@{{ cost | phCurrency }}</v-list-tile-title>
							</v-list-tile-content>
						</v-list-tile>
					</v-list>
					<v-btn color="green" 
					class="mr-2"
					href="{{ url('/reservation') . '/?room_type_id=' . $_GET['room_type_id'] }}"
					style="margin-top: 165px; width: 95%;"
					large 
					dark 
					>Reserve Now</v-btn>
				</v-card>
			</v-flex>
		</v-layout>
	</v-container>
</section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
		var room_type_id = "{{ $_GET['room_type_id'] ?? '' }}";
        new Vue({
            el: '#app',
            data() {
                return {
					e1: 0,
					room_types: [],
					room_type: {},
					available_rooms: 0,
					cost: '',
					place: [],
                }
            },
			created() {
				this.getImages(room_type_id);
				this.getRoomType(room_type_id);
			},
			methods: {
				getRoomType: function(room_type_id) {
					axios.get(`${this.base_url}/guest/room_types/get/${room_type_id}/1`)
					.then(({data}) => {
						this.room_type = data.result;
						this.available_rooms = data.available_rooms;
						this.cost = data.cost;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
				},
				getImages: function(room_type_id) {
					axios.get(`${this.base_url}/guest/images/${room_type_id}`)
					.then(({data}) => {
						// this.src = data.room_images;

						data.room_images.forEach(element => {
							this.place.push({
								src: "{{ url('uploaded-images')}}" + '/' + element.filename
							});
						});
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
			},
			}
        })
    </script>
@endpush