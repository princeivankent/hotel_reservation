<v-layout
row
wrap
class="my-5"
justify-center
>
    <v-flex xs12 sm4 class="my-3">
        <div class="text-xs-center">
            <h2 class="headline">Our Function Rooms</h2>
            <span class="subheading">
                All our rooms are fully air-conditioned, with hot & cold shower, free 2 bottles of water, TV and with FREE toiletrees.
            </span>
        </div>
    </v-flex>
    
    <v-container grid-list-xl>
        <v-layout row wrap>
            <v-flex md4 v-for="(item, index) in function_rooms" v-bind:key="item.function_room_id">
                <v-card class="elevation-8" color="#342010" v-if="item.function_images.length > 0">
                    {{-- <v-img :src="`{{ asset('uploaded-images') }}/${item.function_images[0].image}`" aspect-ratio="1.75" height="200px;"></v-img> --}}
                    <v-carousel
                        delimiter-icon="stop"
                        prev-icon="mdi-arrow-left"
                        next-icon="mdi-arrow-right"
                    >
                        <v-carousel-item
                            v-for="(image,index) in item.function_images"
                            :key="index"
                            :src="`{{ asset('uploaded-images') }}/${image.image}`"
                            aspect-ratio="1.75" height="100px;" width="100%"
                        ></v-carousel-item>
                    </v-carousel>

                    <v-card-title primary-title>
                        <div>
                            <h3 class="headline mb-0">@{{ item.name }}</h3>
                            <div class="grey--text text--darken-1">@{{ item.description }}</div>
                        </div>
                    </v-card-title>
        
                    {{-- <v-card-actions>
                        <v-spacer></v-spacer>
                        <v-btn v-on:click="" flat color="orange">Explore</v-btn>
                    </v-card-actions> --}}
                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
</v-layout>