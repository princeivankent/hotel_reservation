<?php

namespace App\Http\Controllers;

use App\BankAccount;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            BankAccount::all()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'bank_name' => 'required|string',
            'account_number' => 'required|string'
        ]);

        $query = new BankAccount;
        $query->bank_name = $request->bank_name;
        $query->account_number = $request->account_number;
        $query->save();

        return response()->json($query);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bank_account_id)
    {
        return response()->json(
            BankAccount::findOrFail($bank_account_id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $bank_account_id)
    {
        $this->validate($request, [
            'bank_name' => 'required|string',
            'account_number' => 'required|string'
        ]);

        $query = BankAccount::findOrFail($bank_account_id);
        $query->bank_name = $request->bank_name;
        $query->account_number = $request->account_number;
        $query->save();

        return response()->json($query);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bank_account_id)
    {
        $query = BankAccount::findOrFail($bank_account_id);
        $query->delete();

        return response()->json($query);
    }
}
