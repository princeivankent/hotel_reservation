<div class="modal fade" tabindex="-1" role="dialog" id="bank_account_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@{{ formTitle }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="bank_name">Bank Name</label>
                    <input type="text" class="form-control" v-model="form.bank_name" id="bank_name">
                </div>
                <div class="form-group">
                    <label for="account_number">Account Number</label>
                    <input type="text" class="form-control" v-model="form.account_number" id="account_number">
                </div>
            </div>
            <div class="modal-footer">
                <button v-on:click="saveForm" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>