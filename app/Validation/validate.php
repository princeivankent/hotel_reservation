<?php

namespace App\Validation;

class Validate
{
    public function validate($request, $fields)
    {
        $rules = [
            'long_time' => [
                'reservation_type_id' => 'required|numeric|exists:reservation_types,reservation_type_id',
                'checkin'             => 'required|date|before_or_equal:checkout',
                'checkout'            => 'required|date|after_or_equal:checkin',
               // 'timein'			  => 'required',
              //  'timeout'			  => 'required'
            ],
            'short_time' => [
               'reservation_type_id' => 'required|numeric|exists:reservation_types,reservation_type_id',
                'room_rate_id'        => 'required|numeric|exists:room_rates,room_rate_id',
			    'checkin'             => 'required|date'
            ]
        ];
        
        return $request->validate($rules[$fields]);
    }
}

?>