<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionRoom extends Model
{
    protected $fillable = ['name','description','capacity','cost'];
    protected $primaryKey = 'function_room_id';
    public $timestamps = false;

    public function function_reservations()
    {
        return $this->hasMany('App\FunctionReservation', 'function_room_id', 'function_room_id');
    }

    public function function_images()
    {
        return $this->hasMany('App\FunctionImage', 'function_room_id', 'function_room_id');
    }
}
