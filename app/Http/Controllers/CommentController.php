<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        return DB::table('comments as c')
            ->select(
                'c.*',
                'u.name',
                'u.email'
            )
            ->leftJoin('users as u', 'u.user_id', '=', 'c.user_id')
            ->latest()
            ->get();
    }

    public function show($comment_id)
    {
        return DB::table('comments as c')
            ->select(
                'c.*',
                'u.name',
                'u.email'
            )
            ->leftJoin('users as u', 'u.user_id', '=', 'c.user_id')
            ->where(['comment_id' => $comment_id])
            ->first();
    }

    public function store(Request $request)
    {
        $query = new Comment;
        $query->user_id = $request->user_id;
        $query->comment = $request->comment;
        $query->save();

        return response()->json($query);
    }

    public function update(Request $request, $comment_id)
    {
        $query = Comment::findOrFail($comment_id);
        $query->user_id = $request->user_id;
        $query->comment = $request->comment;
        $query->save();

        return response()->json($query);
    }

    public function destroy($comment_id)
    {
        $query = Comment::findOrFail($comment_id);
        $query->delete();

        return response()->json($query);
    }
    
    public function approve(Request $request, $comment_id)
    {
        $query = Comment::findOrFail($comment_id);
        $query->is_approved = 1;
        $query->save();

        return response()->json($query);
    }

    public function comments() 
    {
        return DB::table('comments as c')
            ->select(
                'c.*',
                'u.name',
                'u.email'
            )
            ->leftJoin('users as u', 'u.user_id', '=', 'c.user_id')
            ->where('is_approved', 1)
            ->latest()
            ->get();
    }
}
