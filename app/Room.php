<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['room_type_id','room_code','is_maintenance'];
    protected $primaryKey = 'room_id';
    public $timestamps = false;

    public function room_type()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation', 'room_id', 'room_id');
    }
}
