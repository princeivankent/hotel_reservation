<div class="modal fade" tabindex="-1" role="dialog" id="rooms">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Rooms</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Room #</th>
                            <th class="text-center">Availability</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr v-for="(room, index) in rooms">
                            <td>@{{ room.room_code }}</td>
                            <td>
                                <span v-if="room.reservation_room_id" class="label label-danger">
                                    unavailable
                                </span>
                                <span v-else class="label label-success">
                                    available
                                </span>    
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a type="button" 
                href="" 
                class="btn btn-default btn-sm">
                <i class="fa fa-times"></i>
                Close</a>
                <a type="button" 
                href="{{ url('/admin/rooms') }}" 
                class="btn btn-primary btn-sm">
                Edit Rooms</a>
            </div>
        </div>
    </div>
</div>