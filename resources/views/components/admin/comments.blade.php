@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Customer's Comments
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="row">
            <div v-for="(item, index) in items" class="col-xs-12 col-sm-6 col-md-4">
                <div class="box box-solid">
                    <div class="box-header with-border clearfix">
                        <div class="pull-left info">
                            <i class="fa fa-circle text-success"></i>&nbsp;
                            <label>@{{ item.name }}</label>
                            <p class="text-default">@{{ item.created_at | dateTimeFormat }}</p>
                        </div>
                        <div class="pull-right">
                            <button v-if="!item.is_approved" v-on:click="approveComment(item.comment_id)" type="button" class="btn btn-sm btn-default">
                                <i class="fa fa-thumbs-up"></i>&nbsp;
                                approve
                            </button>
                            <button v-on:click="deleteComment(item.comment_id)" class="btn btn-sm btn-danger">
                                <i class="fa fa-trash"></i>&nbsp;
                                delete
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <p>@{{ item.comment }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        document.querySelector('#function_room_tab').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    items: [],
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                getItems() {
                    axios.get(`${this.base_url}/admin/comments/get`)
                    .then(({data}) => {
                        this.items = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                approveComment(comment_id) {
                    swal({
                        title: "Before in action!",
                        text: "Are you sure to approve this comment?",
                        icon: "warning",
                        dangerMode: true,
                        buttons: {
                            cancel: true,
                            confirm: 'Proceed'
                        },
                        closeOnClickOutside: false
                    })
                    .then(res => {
                        if (res) {
                            return axios.put(`${this.base_url}/admin/comments/approve/${comment_id}`)
                            .then(({data}) => {
                                if (data.is_approved) {
                                    this.getItems();
                                    swal('Alright!', 'This will reflect to your frontend site.', 'success', {button:false,timer:4000});
                                }
                            })
                            .catch((error) => {
                                console.log(error.response);
                                swal('Ooops!', 'Something went wrong please contact the developer. Thanks', 'error', {button:false,timer:4000});
                            });
                        }
                        else {
                            return swal('Ooops!', 'Seems you cancel it?', 'error', {button:false,timer:4000});
                        }
                    });
                },
                deleteComment(comment_id) {
                    swal({
                        title: "Are you sure you want to delete this?",
                        text: "This cannot unchange after it.",
                        icon: "warning",
                        dangerMode: true,
                        buttons: {
                            cancel: true,
                            confirm: 'Proceed',
                        },
                        closeOnClickOutside: false
                    })
                    .then(res => {
                        if (res) {
                            axios.delete(`${this.base_url}/admin/comments/delete/${comment_id}`)
                            .then(({data}) => {
                                if (data) {
                                    this.getItems();
                                    swal('Success!', 'Comment has been deleted.', 'success', {button:false,timer:4000});
                                }
                            })
                            .catch((error) => {
                                console.log(error.response);
                                swal('Ooops!', 'Something went wrong please contact the developer. Thanks', 'error', {button:false,timer:4000});
                            });
                        }
                        else {
                            return swal('Ooops!', 'Seems you cancel it?', 'error', {button:false,timer:4000});
                        }
                    });
                }
            }
        });
    </script>
@endpush