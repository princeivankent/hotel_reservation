<div class="modal fade" id="official_receipt_modal">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <a href="{{ url('/admin/generate_official_receipt') .'/'. Request::segment('4') }}" type="button" class="btn btn-default pull-right text-bold">
                    <i class="fa fa-print"></i>&nbsp;
                    PRINT
                </a>
                <h4 class="modal-title">
                    OFFICIAL RECEIPT
                </h4>
            </div>
            <div class="modal-body">
                <section class="invoice">
                    <div class="row" style="margin-left: 10px; margin-right: 10px;">
                        <div class="col-md-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> Paradise Hotel
                                <small class="pull-right">{{ Carbon\Carbon::parse(now())->format('M d, Y') }}</small>
                            </h2>
                        </div>
                        <div class="row invoice-info">
                            <div class="col-sm-6 invoice-col">
                                <address>
                                    Guest: <strong>@{{ header.guest }}</strong><br>
                                    Email: @{{ header.email }}<br>
                                    Check In: @{{ header.checkin }}<br>
                                    Check Out: @{{ header.checkout }}<br>
                                </address>
                            </div>
                            <div class="col-sm-6 invoice-col">
                                <b>Official Receipt: #@{{ header.official_receipt_code }}</b><br>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Description</th>
                                            <th>Days</th>
                                            <th>Amount Per Day</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(item, index) in list">
                                            <td>@{{ item.date }}</td>
                                            <td>@{{ item.description }}</td>
                                            <td>@{{ item.days }}</td>
                                            <td>@{{ item.amount_per_day | phCurrency }}</td>
                                            <td>@{{ item.total | phCurrency }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="col-xs-6">
                                
                            </div>
                            <div class="col-xs-6">
                                
                            </div>
                        </div>

                        <div class="row" style="margin-top: 30px;">
                            <div class="col-xs-4 text-center">
                                <strong>{{ Auth::user()->name }}</strong>
                                <hr style="margin-bottom: 0px; margin-top: 5px; font-size: 24px;">
                                Cashier
                            </div>
                            <div class="col-xs-4">
                                
                            </div>
                            <div class="col-xs-4 text-center">
                                <strong>@{{ header.guest }}</strong>
                                <hr style="margin-bottom: 0px; margin-top: 5px; font-size: 24px;">
                                Guest
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>