<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ActivateUserController extends Controller
{
    public function activate_user($verification_code)
    {
        $query = DB::table('users')->where([
            'verification_code' => $verification_code
        ])
        ->update([
            'is_verified' => 1
        ]);

        return redirect()->route('home');
    }
}
