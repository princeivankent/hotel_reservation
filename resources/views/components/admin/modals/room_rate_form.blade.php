<!-- Room Rates -->
<div class="modal fade" tabindex="-1" role="dialog" id="room_rates">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Room Rates</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Hours</th>
                            <th class="text-center">Cost</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr v-for="(room, index) in room_rates">
                            <td>@{{ room.hours }}</td>
                            <td class="text-danger">P @{{ room.cost }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a type="button" 
                href="{{ url('/admin/room_rates') }}"
                class="btn btn-primary">
                Edit Rates</a>
            </div>
        </div>
    </div>
</div>