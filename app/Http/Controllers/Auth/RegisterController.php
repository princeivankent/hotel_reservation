<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Services\SendEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    protected $mail;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    public function redirectTo()
    {
        $user_type = Auth::user()->user_type;
        if ($user_type == 'guest') return $this->redirectTo = '/';
        
        return $this->redirectTo = '/admin/dashboard';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SendEmail $mail)
    {
        $this->middleware('guest');
        $this->mail = $mail;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $query = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => $data['user_type'],
            'verification_code' => date('y').date('m').date('s')
        ]);

        $verfication_code = $query->verification_code;

        $res = $this->mail->send([
            'subject'	        => 'Verification Code',
            'sender'	        => config('mail.from.address'),
            'recipient'	        => $query->email,
            'cc'	            => NULL,
            'attachment'	    => NULL,
            'content'	        => [
                'verification_code' => $verfication_code,
                'title' => 'You received <span style="color: #5caad2;">Verification Code</span>',
                'message' => 
                    'Hi Mam/Sir, <strong>Paradise Hotel Reservation</strong> is requiring you to use One Time Login(OTL) verfication code. <br>
                    This is your code <strong>'. $verfication_code .'</strong>.'
            ]
        ]);

        return $query;
    }
}
