@extends('welcome')

@push('styles')
    <style>
        .swal-title {
			font-family: sans-serif;
		}

		.swal-text {
			font-family: sans-serif;
			text-align: center;
		}

		.swal-button {
			font-family: sans-serif;
			background-color: #A5DC86;
		}
    </style>
@endpush

@section('content')
    <section id="top">
        @include('hotel_templates.content_image')
    </section>

    <section>
        @include('hotel_templates.second_section')
    </section>

    {{-- <section>
        @include('hotel_templates.hotel_specifications')
    </section> --}}

    <section>
        @include('hotel_templates.function_rooms')
    </section>

    <section>
        <v-container grid-list-xl>
            <v-layout row wrap justify-center class="my-5">
                <v-flex xs12 sm4>
                    <v-card flat class="transparent">
                        <v-card-title primary-title class="layout justify-center">
                        <div class="headline" id="about_us">
                            <v-icon class="red--text mb-1">favorite</v-icon>
                            About Our Hotel
                        </div>
                        </v-card-title>
                        <v-card-text>
                            @{{ about_our_hotel }}
                        </v-card-text>
                    </v-card>
                </v-flex>

                <v-flex xs12 sm4 offset-sm1>
                    <v-card flat class="transparent">
                        <v-card-title primary-title class="layout justify-center">
                        <div class="headline">
                            <v-icon class="primary--text" style="margin-bottom: 2px;">phone</v-icon>
                            Contact us
                        </div>
                        </v-card-title>
                        <v-card-text>
                        
                        </v-card-text>
                        <v-list class="transparent">
                        <v-list-tile>
                            <v-list-tile-action>
                            <v-icon class="blue--text text--lighten-2">phone</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                            <v-list-tile-title>@{{ item.contact_number }}</v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-action>
                            <v-icon class="blue--text text--lighten-2">place</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                            <v-list-tile-title>@{{ item.address }}</v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-action>
                            <v-icon class="blue--text text--lighten-2">email</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                            <v-list-tile-title>@{{ item.email }}</v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                        </v-list>
                    </v-card>
                </v-flex>

                <v-btn
                    v-on:click="$vuetify.goTo(target)"
                    fab
                    fixed
                    dark
                    bottom
                    right
                    color="success darken-1"
                >
                    <v-icon>arrow_upward</v-icon>
                </v-btn>

            </v-layout>
        </v-container>
    </section>
    @include('components.guest.modals.booking_status')
    @include('hotel_templates.footer')
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        var base_url = "{{ url('/') }}";
        new Vue({
            el: '#app',
            data() {
                return {
                    booking_status_dialog: false,
                    dialog: false,
                    notifications: false,
                    drawer: null,
                    target: '#top',
                    header: '#header',
                    about_us: '#about_us',
                    mission_vision: '#mission_vision',
                    history: '#history',
                    faq: '#faq',

                    main_images: [
                        { src: "{{ asset('images/designs/image_c1.JPG') }}" },
                        { src: "{{ asset('images/img/room/Updated Pics from Paradise Hotel/IMG_2911.JPG') }}" },
                        { src: "{{ asset('images/img/room/Updated Pics from Paradise Hotel/IMG_2960.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2910.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2926.JPG') }}" },
                    ],

                    place: [
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2910.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2935.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2937.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2948.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2957.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/convenient_place/IMG_2960.JPG') }}" }
                    ],
                    spacius: [
                        { src: "{{ asset('images/hotel_specifications/spacius/IMG_2911.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/spacius/IMG_2926.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/spacius/IMG_2927.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/spacius/IMG_2946.JPG') }}" }
                    ],
                    road: [
                        { src: "{{ asset('images/hotel_specifications/wide_road/IMG_2966.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/wide_road/IMG_2971.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/wide_road/IMG_2973.JPG') }}" },
                        { src: "{{ asset('images/hotel_specifications/wide_road/IMG_2974.JPG') }}" }
                    ],

                    items: [],
                    item: {},
                    headers: [
                        {
                            text: 'Room Type',
                            align: 'left',
                            sortable: false,
                            value: 'room_name'
                        },
                        { text: 'Room Code', value: 'room_code' },
                        { text: 'Check In', value: 'checkin' },
                        { text: 'Check Out', value: 'checkout' },
                        { text: 'Persons', value: 'persons' },
                        { text: 'Days', value: 'days' },
                        { text: 'Total Cost', value: 'total_cost' },
                        { text: 'Acknowledged', value: 'is_acknowledged' },
                        { text: 'Approved', value: 'is_approved' },
                        { text: 'Expiry', value: 'expiry' },
                        { text: 'Pay', value: 'pay' },
                        { text: 'Deposit Slip', value: 'deposit_slip' }
                    ],
                    active_bookings: [],
                    uploadedSlip: '',
                    reservation_id: 0,
                    function_rooms: [],
                    place: [],
                    about_our_hotel: ''
                }
            },
            computed: {
                
            },
            watch: {
                booking_status_dialog() {
                    if (this.booking_status_dialog) return this.bookingStatus(); 
                }
            },
            created() {
                this.home_tab = 'white';
                this.getFunctionRooms();

                if (localStorage.getItem('isLogged') == 1) {
                    swal('Alright!', 'You are successfully Logged In.', 'success', {button:false,timer:4000})
                    .then(() => {
                        localStorage.setItem('isLogged', 0);
                    });
                }
            },
            mounted() {
                setTimeout(() => {
                    this.getAboutUs();
                }, 4000);
                this.getRoomType();
                this.getContactUs();
                this.getAboutUs();
            },
            methods: {
                getFunctionRooms() {
                    axios.get(`${this.base_url}/guest/function_rooms/get`)
                    .then(({data}) => {
                        this.function_rooms = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                triggerToUpload(reservation_id) {
                    this.reservation_id = reservation_id;
                },
                onImageChange(e) {
					let files = e.target.files || e.dataTransfer.files;
					if (!files.length)
						return;
                        
					this.createImage(files[0]);
				},
				createImage(file) {
					let reader = new FileReader();
					let vm = this;
					reader.onload = (e) => {
						vm.uploadedSlip = e.target.result;
					};
					reader.readAsDataURL(file);
				},
                uploadSlip() {
					axios.put(`${this.base_url}/guest/booking_status/upload_deposit_slip/${this.reservation_id}`, {deposit_slip: this.uploadedSlip})
					.then(({data}) => {
						swal({
							title: "Alright!",
							text: 'Successfully Uploaded',
							icon: "success",
							closeOnClickOutside: false
						});
                        this.bookingStatus();
                        this.reservation_id = 0;
					})
					.catch((err) => {
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},
                bookingStatus() {
                    axios.get(`${this.base_url}/guest/booking_status`)
                    .then(({data}) => {
                        this.active_bookings = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                createBooking: function(room_type_id) {
                    this.room_type_id = room_type_id;
                    
                    // window.location.href = `{{ url('/guest/booking_form/?room_type_id=') }}${room_type_id}`; // old
                    window.location.href = `{{ url('/room_details?room_type_id=') }}${room_type_id}`;
                },
                getRoomType() {
                    axios.get(`${base_url}/guest/room_types/get`)
                    .then(({data}) => {
                        this.items = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                getContactUs() {
                    axios.get(`${base_url}/guest/contact_us/get`)
                    .then(({data}) => {
                        data.forEach(element => {
                            this.item = element;
                        });
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                getAboutUs() {
                    axios.get(`${base_url}/guest/page_content/get`)
                    .then(({data}) => {
                        this.about_our_hotel = data.about_our_hotel;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                }
            }
        })
    </script>
@endpush