<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomSchedule extends Model
{
    protected $fillable = ['room_type_id','checkin','checkout','timein','timeout','persons','days','total_cost','transaction_type_id'];
    protected $primaryKey = 'room_schedule_id';
    public $timestamps = false;
    protected $casts = [
        'checkin' => 'Y-m-d',
        'checkout' => 'Y-m-d',
        'timein' => 'H:00',
        'timeout' => 'H:00'
    ];
    
    public function reservation()
    {
        return $this->belongsTo('App\Reservation', 'room_schedule_id');
    }
    
    public function room_type()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id', 'room_type_id');
    }
}
