<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = ['bank_name','account_number'];
    protected $primaryKey = 'bank_account_id';
    public $timestamps = false;
}
