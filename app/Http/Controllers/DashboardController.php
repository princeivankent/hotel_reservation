<?php

namespace App\Http\Controllers;

use App\Reservation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $new_reservations = Reservation::where('is_acknowledged', 0)->count();
        $acknowledged_reservations = Reservation::where('is_acknowledged', 1)->count();
        $total_reservations = Reservation::count();
        $outdated_reservations = Reservation::leftJoin('room_schedules as rs', 'rs.room_schedule_id', '=', 'reservations.room_schedule_id')
            ->where([
                ['is_acknowledged', '=', 0],
                ['rs.checkin', '<', Carbon::now()->toDateString()]
            ])->count();
            
        return response()->json([
            'new_reservations' => $new_reservations,
            'acknowledged_reservations' => $acknowledged_reservations,
            'total_reservations' => $total_reservations,
            'outdated_reservations' => $outdated_reservations
        ]);
    }
}
