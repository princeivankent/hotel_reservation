<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomImage extends Model
{
    protected $fillable = ['room_type_id','filename'];
    protected $primaryKey = 'room_image_id';
    public $timestamps = false;

    public function room_type()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }
}
