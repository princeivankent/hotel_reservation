<?php

namespace App\Http\Controllers;

use Auth;
use App\RoomAddOn;
use App\Room;
use App\RoomRate;
use App\Reservation;
use App\RoomSchedule;
use App\RoomType;
use App\Validation\Validate;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BookingController extends Controller
{
	/**
	 * Parameters:
	 * - reservation_type_id
	 * - room_type_id
	 * - checkin
	 * - checkout
	 * - hours //--> optional
	 */
	public function reservation(Validate $validator, Request $request)
	{
		$input = $request->all();

		// Check if availability of rooms
		if ($input['reservation_type_id'] == config('constants.long_time')) {
			$isAvailable = DB::table('rooms')
				->where([
					'room_type_id'   => $input['room_type_id'],
					'reservation_id' => 0,
					'is_maintenance' => 0
				])->exists();
			$validator->validate($request, 'long_time');
		}
		else if ($input['reservation_type_id'] == config('constants.short_time')) {
			$isAvailable = DB::table('rooms')
				->where([
					'room_type_id'   => $input['room_type_id'],
					'reservation_id' => 0,
					'is_maintenance' => 0
				])->exists();
			$validator->validate($request, 'short_time');
		} 
		
		if (isset($isAvailable)) {
			if (!$isAvailable) return response()->json(['availability' => $isAvailable]);
		} 
		
		return $this->transact($input);
	}

	public function transact($params)
	{
		/**
		 * params: 
		 *      reservation_type_id, 	$input->reservation_type_id
		 *      room_type_id, 			$input->room_type_id
		 *      checkin, 				$input->checkin
		 *      checkout, 				$input->checkout
		 * 		hours //--> optional	$input->hours
		 * return [days, hours, cost]
		 */
		try {
			DB::beginTransaction();

			/**
			 * cost 
			 * days
			 * hours
			 */
			$computed_cost = $this->getTotalCost($params); 

			// return $params;
			if ($params['reservation_type_id'] == 1) {
				$hour = Carbon::parse($params['timein'])->format('h');
				$minutes = Carbon::parse($params['timein'])->format('i');

				$query = RoomRate::findOrFail($params['room_rate_id']);
				$picked_hours = $query->hours; // 12
				$total_hours = (int) $hour + $picked_hours;
				
				$params['timeout'] = $total_hours.':'.$minutes;
			}
			else {
				$params['timeout'] = Carbon::parse($params['timeout'])->format('H:i');
			}

			$query = new RoomSchedule;
			$query->checkin = $params['checkin'];
			$query->checkout = $params['checkout'];
			$query->timein = Carbon::parse($params['timein'])->format('H:i');
			$query->timeout = $params['timeout'];
			$query->persons = $params['persons'];
			$query->total_hours = $computed_cost['hours'];
			$query->total_cost = $computed_cost['cost'];
			$query->transaction_type_id = $params['transaction_type_id'];
			$query->payment_status = $params['transaction_type_id'] == 1 ? 'half_paid' : 'fully_paid';
			$query->save();

			// $start = \Carbon\Carbon::parse($params['checkin']); 
			// $end = \Carbon\Carbon::parse($params['checkout']);
			// $length = $end->diffInDays($start);

			$reservation_id;
			if ($query) {
				$room_schedule_id = $query->room_schedule_id;

				$query = new Reservation;
				$query->room_schedule_id = $room_schedule_id;
				$query->room_type_id = $params['room_type_id'];
				$query->reservation_type_id = $params['reservation_type_id'];
				$query->user_id = Auth::user()->user_id;
				$query->save();

				$reservation_id = $query->reservation_id;

				if ($params['addons'] > 0) {
					foreach ($params['addons'] as $value) {
						$query = new RoomAddOn;
						$query->add_on_type_id = $value['add_on_type_id'];
						$query->room_schedule_id = $room_schedule_id;
						$query->quantity = $value['quantity'];
						$query->cost = $value['cost'];
						$query->save();
					}
				}
			}

			// Update one room in random, then return to user!
			$query = DB::table('rooms')
				->where([
					'room_type_id'   => $params['room_type_id'],
					'reservation_id' => 0,
					'is_maintenance' => 0
				])
				->inRandomOrder()
				->first();

			$room_details = [
				'room_id' 	   => $query->room_id,
				'room_type_id' => $query->room_type_id,
				'room_code'    => $query->room_code
			];

			$query = Room::findOrFail($room_details['room_id']);
			$query->reservation_id = $reservation_id;
			$query->save();
			
			DB::commit();
			return response()->json($room_details);
		} catch (Exception $e) {
			DB::rollBack();
			report($e);

			return response()->json(false);
		}
	}

	//----- API Computation ------//

	/**
	 * params: 
	 *      reservation_type_id, 
	 *      room_type_id, 
	 *      checkin, 
	 *      checkout,
	 * 		hours //--> optional
	 * return [days, hours, cost]
	 */
	public function getTotalCost($params)
	{
		$condition_fields;
		if ($params['reservation_type_id'] == 1) {
			$condition_fields = [
				'room_rate_id' => $params['room_rate_id']
			];
			$params['checkout'] = $params['checkin'];
		}
		else {
			$condition_fields = [
				'room_type_id' => $params['room_type_id'],
				'hours' => 24
			];
		} 

		$query = RoomRate::where($condition_fields)->first();

		// Get HOurs & Cost (default)
		$rate = [
			'hours' => $query->hours,
			'cost'  => $query->cost
		];

		$start = \Carbon\Carbon::parse($params['checkin']); 
		$end = \Carbon\Carbon::parse($params['checkout']);
		$length = $end->diffInDays($start);

		// Computations
		if ($params['reservation_type_id'] == 1) {
			$days = (int) $length + 1;
			$hours = $rate['hours'];
			$cost = $rate['cost'];
		}
		else {
			$days = (int) $length; // 2 days
			$hours = (int) ($days*$rate['hours']); // 42 hours
			$cost = (int) ($rate['cost']*$days);
		}

		$result = [
			'days'  => $days,
			'hours' => $hours,
			'cost'  => $cost
		];

		return $result;
	}

	public function cancel($reservation_id)
	{
		$query = Reservation::findOrFail($reservation_id);
		$query->is_cancelled = 1;
		$query->save();

		return response()->json($query);
	}

	public function timeParser($time)
	{
		return Carbon::parse($time)->format('H:i A');
	}

	public function room_capacity($room_type_id)
	{
		$query = RoomType::findOrFail($room_type_id);
		return response()->json($query->room_capacity);
	}
}
