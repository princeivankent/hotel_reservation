<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ContactUs::latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contact_number' => 'required|numeric',
            'address' => 'required|string',
            'email' => 'required|email'
        ]);

        $query = new ContactUs;
        $query->contact_number = $request->contact_number;
        $query->address = $request->address;
        $query->email = $request->email;
        $query->save();

        return 200;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($contact_us_id)
    {
        return response()->json(ContactUs::findOrFail($contact_us_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contact_us_id)
    {
        $this->validate($request, [
            'contact_number' => 'required|numeric',
            'address' => 'required|string',
            'email' => 'required|email'
        ]);

        $query = ContactUs::findOrFail($contact_us_id);
        $query->contact_number = $request->contact_number;
        $query->address = $request->address;
        $query->email = $request->email;
        $query->save();

        return 200;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($contact_us_id)
    {
        $query = ContactUs::findOrFail($contact_us_id);
        $query->delete();

        return 201;
    }
}
