<v-dialog
    v-model="booking_status_dialog"
    width="1700"
    light
>
    <v-card height="60vh">
        <v-card-title
            {{-- class="headline grey lighten-2" --}}
            class="headline"
            primary-title
        >
            Booking Status
        </v-card-title>

        <v-card-text>
            <v-card v-if="reservation_id > 0">
                <v-card-text>
                    <v-btn v-on:click="uploadSlip" color="green" small dark>Save Changes</v-btn>
                    <input type="file" v-on:change="onImageChange">
                </v-card-text>
            </v-card>

            <v-data-table
            :headers="headers"
            :items="active_bookings"
            hide-actions
            class="elevation-1"
            >
                <template slot="items" slot-scope="props">
                    <td>@{{ props.item.room_name }}</td>
                    <td class="text-xs-right">@{{ props.item.room_code }}</td>
                    <td class="text-xs-right">@{{ props.item.checkin | dateFormat}} | @{{ props.item.timein | timeFormat }}</td>
                    <td class="text-xs-right">@{{ props.item.checkout | dateFormat }} | @{{ props.item.timeout | timeFormat }}</td>
                    <td class="text-xs-right">@{{ props.item.persons }}</td>
                    <td class="text-xs-right">@{{ props.item.days }}</td>
                    <td class="text-xs-right">@{{ props.item.total_cost | phCurrency }}</td>
                    <td class="text-xs-right">
                        <v-chip v-if="props.item.is_acknowledged" color="green" text-color="white">
                            <v-avatar>
                                <v-icon>check_circle</v-icon>
                            </v-avatar>
                            YES
                        </v-chip>
                        <v-chip v-else color="red" text-color="white">
                            <v-avatar>
                                <v-icon>cancel</v-icon>
                            </v-avatar>
                            NO
                        </v-chip>
                    </td>
                    <td class="text-xs-right">
                        <v-chip v-if="props.item.is_approved" color="green" text-color="white">
                            <v-avatar>
                                <v-icon>check_circle</v-icon>
                            </v-avatar>
                            YES
                        </v-chip>
                        <v-chip v-else color="red" text-color="white">
                            <v-avatar>
                                <v-icon>cancel</v-icon>
                            </v-avatar>
                            NO
                        </v-chip>
                    </td>
                    <td class="text-xs-right">@{{ props.item.expiry }}</td>
                    <td class="text-xs-right">@{{ props.item.pay | phCurrency }}</td>
                    <td class="text-xs-left">
                        <span v-if="props.item.deposit_slip">@{{ props.item.deposit_slip }}</span>
                        <v-btn v-else v-on:click="triggerToUpload(props.item.reservation_id)" color="green" small dark>Click to upload</v-btn>
                    </td>
                </template>
            </v-data-table>
        </v-card-text>
    </v-card>
</v-dialog>