<?php

namespace App\Http\Controllers;

use App\FrequentlyAskedQuestion;
use Illuminate\Http\Request;

class FrequentlyAskedQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(FrequentlyAskedQuestion::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required|string',
            'answer' => 'required|string'
        ]);

        $query = new FrequentlyAskedQuestion;
        $query->question = $request->question;
        $query->answer = $request->answer;
        $query->save();

        return 200;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($frequently_asked_question_id)
    {
        return response()->json(FrequentlyAskedQuestion::findOrFail($frequently_asked_question_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $frequently_asked_question_id)
    {
        $this->validate($request, [
            'question' => 'required|string',
            'answer' => 'required|string'
        ]);

        $query = FrequentlyAskedQuestion::findOrFail($frequently_asked_question_id);
        $query->question = $request->question;
        $query->answer = $request->answer;
        $query->save();

        return 200;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = FrequentlyAskedQuestion::findOrFail($frequently_asked_question_id);
        $query->delete();

        return 201;
    }
}
