<div class="modal fade" tabindex="-1" role="dialog" id="booked_rooms">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-search"></i>
                    Room Availability
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" width="40">#</th>
                            <th class="text-center">ROOM CODE</th>
                            <th class="text-center">AVAILABILITY</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in booked_rooms"
                        v-bind:key="index">
                            <td class="text-center">@{{ index+1 }}</td>
                            <td class="text-center text-primary">@{{ item.room_code }}</td>
                            <td class="text-center text-danger">
                                <div v-if="item.is_maintenance" class="label label-danger">
                                    unavailable
                                </div>
                                <div v-else-if="item.checkout === null && item.timeout === null" class="label label-success">
                                    available now
                                </div>
                                <div v-else>
                                    @{{ item.checkout }} | @{{ item.timeout }}
                                </div>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>