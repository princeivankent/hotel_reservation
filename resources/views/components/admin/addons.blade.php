@extends('layouts.app')

@section('content')
<v-app>
    <div>
    <section class="content-header">
        <h1>
            Rooms and Utilities
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box box-primary">
            <div class="box-body">
                <v-toolbar flat color="white">
                    <v-toolbar-title>AddOns</v-toolbar-title>
                    <v-divider
                        class="mx-2"
                        inset
                        vertical
                    ></v-divider>
                    <v-spacer></v-spacer>
                    <v-dialog v-model="dialog" persistent width="500px">
                        <v-btn slot="activator" color="success" dark class="mb-2">New Item</v-btn>
                        <v-card>
                            <v-card-title class="headline primary" primary-title>
                                <span class="headline white--text">@{{ formTitle }}</span>
                            </v-card-title>
                    
                            <v-card-text>
                                <v-container grid-list-md>
                                    <v-layout wrap>
                                        <v-flex xs12 sm6 md12>
                                            <v-textarea
                                            v-model="form.description" 
                                            label="Description"
                                            outline
                                            auto-grow
                                            ></v-textarea>
                                        </v-flex>
                                        <v-flex xs12 sm6 md6>
                                            <v-text-field 
                                            v-model="form.max_hours" 
                                            label="Max Hours"
                                            ></v-text-field>
                                        </v-flex>
                                        <v-flex xs12 sm6 md6>
                                            <v-text-field 
                                            v-model="form.cost" 
                                            label="Cost"
                                            ></v-text-field>
                                        </v-flex>
                                    </v-layout>
                                </v-container>
                            </v-card-text>
                    
                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="red darken-1" flat v-on:click="close">
                                    <v-icon>cancel</v-icon>
                                    Cancel
                                </v-btn>
                                <v-btn color="blue darken-1" flat v-on:click="saveItem">
                                    <v-icon>check_circle</v-icon>
                                    Save
                                </v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>
                </v-toolbar>

                <v-data-table
                :headers="headers"
                :items="items"
                :search="search"
                :loading="loading"
                :disable-initial-sort="true"
                class="elevation-1">
                    <template slot="items" slot-scope="props">
                        <td>@{{ props.item.description }}</td>
                        <td>@{{ props.item.max_hours }}</td>
                        <td>P @{{ props.item.cost }}</td>
                        <td class="text-xs-right">
                            <v-btn
                                v-on:click="editItem(props.item.add_on_type_id, props.index)"
                                color="primary"
                                icon
                                dark
                                flat
                            >
                                <v-icon style="font-size: 20px;">
                                    edit
                                </v-icon>
                            </v-btn>
                            
                            <v-btn
                                v-on:click="deleteItem(props.item.add_on_type_id, props.index)"
                                color="error darken-1"
                                icon
                                dark
                                flat
                            >
                                <v-icon style="font-size: 20px;">
                                    delete
                                </v-icon>
                            </v-btn>
                        </td>
                    </template>
                </v-data-table>
            </div>
        </div>
    </section>
    <div>
<v-app>
@endsection

@push('scripts')
    <script>
        document.querySelector('#_addons').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    dialog: false,
                    loading: true,
                    search: '',
                    headers: [
                        {
                            text: 'Description',
                            align: 'left',
                            sortable: false,
                            value: 'description'
                        },
                        { text: 'max_hours', value: 'max_hours' },
                        { text: 'Cost', value: 'cost' },
                        { text: '', value: '', sortable: false }
                    ],
                    items: [],
                    itemIndex: -1,

                    form: {
                        add_on_type_id: 0,
                        description: '',
                        max_hours: 0,
                        cost: 0
                    },
                    defaultItem : {
                        add_on_type_id: 0,
                        description: '',
                        max_hours: 0,
                        cost: 0
                    }
                }
            },
            computed: {
                formTitle () {
                    return this.itemIndex === -1 ? 'New Item' : 'Edit Item'
                }
            },
            created() {
                this.getItem();
            },
            methods: {
                getItem: function() {
                    axios.get(`${this.base_url}/admin/addons/get`)
                    .then(({data}) => {
                        this.items = data;

                        setTimeout(() => {
                            this.loading = false;
                        }, 1000);
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                editItem: function(add_on_type_id, index) {
                    axios.get(`${this.base_url}/admin/addons/get/${add_on_type_id}`)
                    .then(({data}) => {
                        this.form = Object.assign({}, data);
                        this.itemIndex = index;
                        this.dialog = true;
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                deleteItem: function(add_on_type_id, index) {
                    var msg = confirm('Are you sure you want to delete this item?');
                    if (msg) {
                        axios.delete(`${this.base_url}/admin/addons/delete/${add_on_type_id}`)
                        .then(({data}) => {
                            this.items.splice(index, 1);
                            this.notify('Successfully deleted!', true);
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    }
                },

                close () {
                    this.dialog = false
                    setTimeout(() => {
                        this.form = Object.assign({}, this.defaultItem)
                        this.itemIndex = -1
                    }, 300)
                },

                saveItem: function() {
                    if (this.itemIndex > -1) {
                        axios.put(`${this.base_url}/admin/addons/put/${this.form.add_on_type_id}`, this.form)
                        .then(({data}) => {
                            if (Object.assign(this.items[this.itemIndex], this.form)) {
                                this.notify('Successfully saved!', true);
                                return this.close();
                            } 
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    } else {
                        axios.post(`${this.base_url}/admin/addons/post`, this.form)
                        .then(({data}) => {
                            if (this.items.push(this.form)) {
                                this.notify('Successfully saved!', true);
                                return this.close();
                            }
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    }
                }
            }
        });
    </script>
@endpush
