<?php

namespace App\Http\Controllers;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Illuminate\Http\Request;

class StripeController extends Controller
{
	public function pay(Request $request)
	{
		try {
			Stripe::setApiKey(config('services.stripe.secret'));
		
			$customer = Customer::create(array(
				'email'  => $request->stripeEmail,
				'source' => $request->stripeToken
			));
			
			$half_pay = (int)($request->amount+0000)/2;
			$charge = Charge::create(array(
				'customer' => $customer->id,
				'amount' => $half_pay,
				'currency' => 'php'
			));
		
			return redirect()->route('home');
		} catch (Exception $ex) {
			return $ex->getMessage();
		}
	}
}
