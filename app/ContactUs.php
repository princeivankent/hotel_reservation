<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = ['contact_number','address','email'];
    protected $primaryKey = 'contact_us_id';
    protected $table = 'contact_us';
    public $timestamps = false;
}
