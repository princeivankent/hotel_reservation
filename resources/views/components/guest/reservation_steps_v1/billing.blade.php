<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="form-group mt-5">
            <label for="">Room Type</label>
            <input type="text" class="form-control" v-model="room_type.room_name" readonly>
        </div>
        <div class="form-group">
            <label for="" style="margin-right: 5px;">Room Cost</label>
            <input type="text" class="form-control" v-bind:value="cost | phCurrency" readonly>
        </div>
        <div class="form-group clearfix">
            <label for="">Days</label>
            <input type="text" class="form-control" v-bind:value="days" readonly>
        </div>
        <div class="form-group">
            <label for="">Total</label>
            <input type="text" class="form-control" v-bind:value="days*cost | phCurrency" readonly>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <button class="btn btn-success pull-right text-bold">
                NEXT&nbsp;
                <i class="fa fa-arrow-circle-right"></i>
            </button>
        </div>
    </div>
</div>