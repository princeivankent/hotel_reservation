<?php

namespace App\Http\Controllers;

use Auth;
use Image;
use App\Payment;
use App\Reservation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BookingStatusController extends Controller
{
    public function booking_status()
    {
        $query = DB::table('reservations as rsv')
            ->select(
                'rsv.reservation_id',
                'rsv.is_acknowledged',
                'rsv.did_checkout',
                'rsv.is_cancelled',

                'rt.room_name',
                'rt.room_description',

                'p.deposit_slip',
                'p.pay',
                'p.expiry',
                'p.is_approved',

                'rs.checkin',
                'rs.checkout',
                'rs.timein',
                'rs.timeout',
                'rs.persons',
                'rs.days',
                'rs.total_cost',

                'r.room_code'
            )
            ->leftJoin('room_schedules as rs', 'rs.room_schedule_id', '=', 'rsv.room_schedule_id')
            ->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rs.room_type_id')
            ->leftJoin('rooms as r', 'r.room_id', '=', 'rsv.room_id')
            ->leftJoin('users as u', 'u.user_id', '=', 'rsv.user_id')
            ->leftJoin('payments as p', 'p.payment_id', '=', 'rsv.payment_id')
            ->where([
                'u.user_id' => Auth::user()->user_id,
                'rsv.did_checkout' => NULL
            ])
            ->get();

        return response()->json($query);
    }

    public function upload_deposit_slip(Request $request, $reservation_id)
    {
        $query = Reservation::findOrFail($reservation_id);
        $payment = Payment::findOrFail($query->payment_id);
        if ($payment) {
            $image = $request->deposit_slip;
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($image)->save(public_path('uploaded-images/deposit_slips/').$name);
            $payment->deposit_slip = $name;
            $payment->expiry = NULL;
        }

        $payment->save();

        return response()->json($payment);
    }
}
