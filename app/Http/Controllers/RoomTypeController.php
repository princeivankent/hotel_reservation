<?php

namespace App\Http\Controllers;

use App\RoomRate;
use App\RoomImage;
use App\RoomType;
use Validator;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RoomTypeController extends Controller
{
    public function show($room_type_id, $transaction_type_id)
    {
        $query = RoomType::where('room_type_id', $room_type_id)
            // ->with(['rooms', 'add_on_type', 'room_rates'])
            ->with(['rooms', 'room_rates'])
            ->first();

        $available_rooms = DB::table('rooms as r')
            ->selectRaw('r.*, rsv.room_id as room')
            ->leftJoin('reservations as rsv', 'rsv.room_id', '=', 'r.room_id')
            ->where([
                ['r.room_type_id', '=', $room_type_id],
                ['r.is_maintenance', '=', 0]
            ])->get();

        $count = 0;
        foreach ($available_rooms as $key => $value) {
            if (!$value->room) {
                $count++;
            }
        }

        $cost = '';
        if (isset($transaction_type_id)) {
            if ($transaction_type_id == 1) {
                $cost = 'Select room rate below';
            }
            else {
                $cost = RoomRate::where([
                        ['room_type_id', '=', $room_type_id],
                        ['hours', '=', 24]
                    ])->first();

                $cost = $cost->cost;
            }
        }

        $result = [
            'result'          => $query,
            'available_rooms' => $count,
            'cost'            => $cost
        ];

        return response()->json($result);
    }
    
    public function upload(Request $request)
    {
        Validator::make($request->all(), [
            'room_type_id' => 'required|numeric',
            'filename'     => 'required|image'
        ]);

        if($request->get('filename')) {
            $image = $request->get('filename');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('filename'))->save(public_path('uploaded-images/').$name);
        }

        $query = DB::table('room_images')
            ->insert([
                'room_type_id' => $request->room_type_id,
                'filename'     => $name
            ]);

        return response()->json(true);
    }

    public function images($room_type_id)
    {   
        return response()->json(
            RoomType::where('room_type_id', $room_type_id)
                ->with('room_images')
                ->oldest('created_at')
                ->first()
        );
    }

    public function delete_image($room_image_id) 
    {
        $query = RoomImage::findOrFail($room_image_id);
        $query->delete();
        if ($query) $file = Storage::delete('uploaded-images/' . $query->filename);

        return response()->json($query);
    }

    public function getAll()
    {
        return response()->json(RoomType::with(['rooms', 'room_rates'])->get());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'room_name' => 'required|string',
            'room_description' => 'required|string',
            'room_capacity' => 'required|numeric',
            'image' => 'required'
        ]);

        $query = new RoomType;
        $query->room_name = $request->room_name;
        $query->room_description = $request->room_description;
        $query->room_capacity = $request->room_capacity;

        if($request->get('image')) {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('image'))->save(public_path('uploaded-images/').$name);
            $query->image = $name;
        }

        $query->save();

        return response()->json($query);
    }

    public function destroy($room_type_id)
    {
        $query = RoomType::findOrFail($room_type_id);
        $query->delete();

        return response()->json($query);
    }

    public function getRoomType($room_type_id)
    {
        $query = RoomType::findOrFail($room_type_id);
        return response()->json($query);
    }

    public function update(Request $request, $room_type_id)
    {
        $query = RoomType::findOrFail($room_type_id);
        $query->room_name = $request->room_name;
        $query->room_description = $request->room_description;
        $query->room_capacity = $request->room_capacity;
        
        if($request->get('image')) {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('image'))->save(public_path('uploaded-images/').$name);
            $query->image = $name;
        }

        $query->save();

        return response()->json($query);
    }
    
    public function get_room_capacity($room_type_id)
    {
        $query = RoomType::findOrFail($room_type_id);
        return response()->json($query->room_capacity);
    }
}
