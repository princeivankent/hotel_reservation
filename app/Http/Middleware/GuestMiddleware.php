<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_type = Auth::user()->user_type;
        if ($user_type == 'admin') return redirect('/admin/dashboard');
        
        return $next($request);
    }
}
