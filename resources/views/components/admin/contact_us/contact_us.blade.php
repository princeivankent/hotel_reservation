@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-phone"></i>
            Contact Us
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <button v-on:click="openCreateModal"
                class="btn btn-flat btn-default pull-right">
                    <i class="fa fa-plus-circle"></i>&nbsp;
                    ADD OR REPLACE
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center text-uppercase">#</th>
                            <th class="text-center text-uppercase">Contact Number</th>
                            <th class="text-center text-uppercase">Address</th>
                            <th class="text-center text-uppercase">Email</th>
                            <th class="text-center text-uppercase">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items">
                            <td class="text-center">@{{ index+1 }}</td>
                            <td class="text-center">@{{ item.contact_number }}</td>
                            <td class="text-center">@{{ item.address }}</td>
                            <td class="text-center">@{{ item.email }}</td>
                            <td class="text-center">
                                <button v-on:click="getItem(item.contact_us_id)"
                                class="btn btn-sm btn-flat btn-primary">
                                    <i class="fa fa-pencil"></i>&nbsp;
                                    UPDATE
                                </button>
                                <button v-on:click="deleteItem(item.contact_us_id)"
                                class="btn btn-sm btn-flat btn-danger">
                                    <i class="fa fa-trash"></i>&nbsp;
                                    DELETE
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('components.admin.contact_us.contact_us_modal')
@endsection

@push('scripts')
    <script>
        document.querySelector('#contact_us_tab').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    items: [],
                    form: {},
                    isEdit: 0,
                    formTitle: 'Add/Replace Information',
                    errors: {}
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                getItems() {
                    axios.get(`${this.base_url}/admin/contact_us/get`)
                    .then(({data}) => {
                        this.items = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                getItem(contact_us_id) {
                    axios.get(`${this.base_url}/admin/contact_us/get/${contact_us_id}`)
                    .then(({data}) => {
                        $('#contact_us_modal').modal('show');
                        this.isEdit = 1;
                        this.form = data;
                    })
                    .catch((error) => {
                        console.log(error.response);
                    });
                },
                saveItem() {
                    if (this.isEdit) {
                        axios.put(`${this.base_url}/admin/contact_us/update/${this.form.contact_us_id}`, this.form)
                        .then(({data}) => {
                            this.openCreateModal();
                            this.resetModalForm();
                            this.getItems();
                            toastr.success('', 'Succcessfully Saved!');
                        })
                        .catch((error) => {
                            this.errors = error.response.data.errors;
                            console.log(error.response);
                        });
                    }
                    else {
                        axios.post(`${this.base_url}/admin/contact_us/store`, this.form)
                        .then(({data}) => {
                            this.openEditModal();
                            this.resetModalForm();
                            this.getItems();
                            toastr.success('', 'Succcessfully Saved!');
                        })
                        .catch((error) => {
                            this.errors = error.response.data.errors;
                            console.log(error.response);
                        });
                    }
                },
                deleteItem(contact_us_id) {
                    swal({
						title: "Delete this item",
						text: "Note: There's no way to undo changes",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then(res => {
						if (res) {
							axios.delete(`${this.base_url}/admin/contact_us/destroy/${contact_us_id}`)
							.then(({data}) => {
								if (data) {
									this.getItems();
									toastr.success('Successfull Saved!');
								}
							})
							.catch((error) => {
								console.log(error.response);
							});
						}
					});
                },
                //
                resetModalForm() {
                    this.form = {};
                    this.isEdit = 0;
                    this.formTitle = 'Replace Information';
                    $('#contact_us_modal').modal('hide');
                },
                openCreateModal() {
                    $('#contact_us_modal').modal('show');
                    this.form = {};
                    this.isEdit = 0;
                    this.formTitle = 'Add/Replace Information';
                },
                openEditModal() {
                    $('#contact_us_modal').modal('show');
                    this.form = {};
                    this.isEdit = 1;
                    this.formTitle = 'Edit Information';
                },
            }
        });
    </script>
@endpush