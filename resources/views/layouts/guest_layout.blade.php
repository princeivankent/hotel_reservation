<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/adminlte.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="stylesheet" href="{{ url('css/styles.css') }}">
	<style>[v-cloak] > * {display: none;}</style>
	@stack('styles')
</head>
{{-- <body class="hold-transition layout-top-nav"> --}}
<body>
	<div class="wrapper">
		<header class="main-header" style="background-color: #190E08;">
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						@if (isset(Auth::user()->user_type))
							@if (Auth::user()->user_type == 'admin')
								<a href="{{ url('/admin/dashboard') }}" class="navbar-brand"><b>PARADISE</b>HOTEL</a>
							@endif
						@else
							<a href="{{ url('/') }}" class="navbar-brand"><b>PARADISE</b>HOTEL</a>
						@endif
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
						<!-- User Account Menu -->

						@auth
							<li class="dropdown user user-menu">
								<!-- Menu Toggle Button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!-- The user image in the navbar-->
								<img src="{{ asset('admin-images/avatar5.png') }}" class="user-image" alt="User Image">
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs">{{ Auth::user()->name }}</span>
								</a>
								<ul class="dropdown-menu">
								<!-- The user image in the menu -->
								<li class="user-header" style="background-color: #4E3018">
									<img src="{{ asset('admin-images/avatar5.png') }}" class="img-circle" alt="User Image">
									<p>
										{{ Auth::user()->name }}
										<small>{{ Auth::user()->user_type }}</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-right">
									<a href="{{ route('logout') }}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();" 
									class="btn btn-default btn-flat">
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
										<i class="fa fa-sign-out"></i>&nbsp;
										Sign out
									</a>
								</div>
								</li>
								</ul>
							</li>
						@endauth
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="content-wrapper">
			<div class="container">
				<div id="app" v-cloak>
					@yield('content')
				</div>
			</div>
		</div>
	
		<footer class="main-footer">
			<div class="container">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.4.0 | <span class="text-red">This page is secured by Laravel Artisans</span>
				</div>
				<strong>Copyright &copy; 2017-2018 <a href="{{ url('/') }}">Carefully crafted for PARADISE HOTEL</a>.</strong> All rights reserved.
			</div>
		</footer>			

		<div class="control-sidebar-bg"></div>
	</div>

	<!-- Vuejs/jQuery/Axios -->
	<script src="{{ asset('js/app.js') }}"></script>

	<!-- jQuery -->
	<script src="{{ asset('libraries/admin/js/jquery.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/fastclick.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/adminlte.min.js') }}"></script>

	<!-- Global Functions -->
	<script>
		var mixin = {
			data () {
				return {
					base_url: "{{ url('/') }}",
					fields: {},
					room_type_id: ''
				}
			},
			filters: {
				phCurrency(value) {
					if (!value) return ''
					value = parseInt(value);
					return '₱ ' + value.toLocaleString('en');
				},
				timeFormat(time) {
					return moment(time, "HH:mm A").format('hh:mm A');
				},
				dateFormat(value) {
					if (!value) return ''
					value = value.toString()
					return moment(value).format('MMMM D, YYYY')
				},
				dateTimeFormat(value) {
					if (!value) return ''
					value = value.toString()
					return moment(value).format('MMMM D, YYYY | h:mm:ss a')
				}
			}
		}
	</script>
	
	@stack('scripts')
</body>
</html>
