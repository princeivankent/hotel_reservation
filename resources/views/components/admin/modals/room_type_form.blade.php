<div class="modal fade" tabindex="-1" role="dialog" id="room_type_form">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i>
                    Room Type
                </h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" v-model="form.room_type_id" name="room_type_id" id="room_type_id">
                    <div class="form-group">
                        <label for="room_name">Room Name/Type</label>
                        <input type="text" class="form-control" id="room_name" v-model="form.room_name">

                        <span v-if="errors.room_name" class="text-danger text-bold">
                            @{{ errors.room_name[0] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="room_description">Room Description/Amenities</label>
                        <textarea class="form-control" 
                        name="room_description" id="room_description" 
                        cols="30" rows="2" v-model="form.room_description"></textarea>

                        <span v-if="errors.room_description" class="text-danger text-bold">
                            @{{ errors.room_description[0] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="room_capacity">Room Capacity</label>
                        <input type="number" class="form-control" id="room_capacity" v-model="form.room_capacity">

                        <span v-if="errors.room_capacity" class="text-danger text-bold">
                            @{{ errors.room_capacity[0] }}
                        </span>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="filename" class="pull-right">Filename</label>
                        </div>
                        <div class="col-md-4">
                            <input type="file" 
                            v-on:change="onImageChange"
                            class="form-control" 
                            id="filename"
                            name="filename"
                            style="padding-top: 5px; padding-bottom: 40px; width: 250px;">
                        </div>
                    </div>
                    <div v-if="isEdit" class="form-group row">
                        <img :src="`{{ url('uploaded-images') }}/${ form.image }`" alt="" width="100%" height="500px">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>&nbsp;
                    CLOSE
                </button>
                <button v-on:click="saveRoomType" type="button" class="btn btn-sm btn-success">
                    <i class="fa fa-check-circle"></i>&nbsp;
                    Save Changes
                </button>
            </div>
        </div>
    </div>
</div>