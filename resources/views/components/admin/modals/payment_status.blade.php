<!-- Images -->
<div class="modal fade" tabindex="-1" role="dialog" id="payment_staus">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Guest's Payment Status</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div v-if="reservation.deposit_slip">
                            <div id="images">
                                <img :src="`{{ url('uploaded-images/deposit_slips/') }}/${reservation.deposit_slip}`" width="100%" height="500" alt="">
                            </div>
                        </div>
                        <div v-else class="callout callout-danger">
                            <h4>
                                <i class="fa fa-hand-paper-o"></i>&nbsp;
                                Guest is on a Pay Later Mode
                            </h4>
                            <p>24 hours has been given to fulfill his/her payment.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button 
                v-if="!reservation.is_approved && reservation.deposit_slip && !reservation.expiry"
                v-on:click="approveDepositSlip(reservation.reservation_id)"
                class="btn btn-success">
                    <i class="ion ion-android-checkmark-circle"></i>&nbsp;
                    Approve
                </button>
                <a 
                v-else-if="reservation.is_approved"
                :href="`{{ url('/admin/guest_billing_information/room_schedule_id/') }}/${reservation.room_schedule_id}`"
                class="btn btn-success">
                    Navigate to Billing &nbsp;
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>

@push('plugin_scripts')
    <script>
        // setTimeout(() => {
        //     var viewer = new Viewer(document.getElementById('images'));
        //     viewer.update();
        // });
    </script>
@endpush