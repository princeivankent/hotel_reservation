<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class SendEmail
{
    public function send($params)
    {
        $data = [
            'subject'	        => $params['subject'],
            'sender'	        => $params['sender'],
            'recipient'	        => $params['recipient'],
            'cc'	            => $params['cc'],
            'attachment'	    => $params['attachment'],
            'content'	        => $params['content']
        ];

        return Mail::send('emails.email_template', ['content' => $data['content']], function ($mail) use ($data) {
            $mail->from($data['sender'], 'Paradise Hotel Reservation System');
            $mail->to($data['recipient'])->subject($data['subject']);
            
            if (isset($data['cc'])) $mail->cc($data['cc']);
            if (isset($data['attachment'])) $mail->attach($data['attachment']);
        });
    }
}