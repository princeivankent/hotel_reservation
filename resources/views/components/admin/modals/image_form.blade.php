<!-- Images -->
<div class="modal fade" tabindex="-1" role="dialog" id="images">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Images</h4>
            </div>
            <div class="modal-body">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label for="filename" class="pull-right">Filename</label>
                            </div>
                            <div class="col-md-4">
                                <input type="file" 
                                v-on:change="onImageChange"
                                class="form-control" 
                                id="filename"
                                name="filename"
                                style="padding-top: 5px; padding-bottom: 40px; width: 250px;">
                            </div>
                            <div class="col-md-4">
                                <a type="button" 
                                v-on:click="uploadImage"
                                class="btn btn-primary">
                                Upload Image</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"
                            v-for="(image, index) in room_types.room_images" 
                            v-bind:key="index" 
                            style="margin-right: 0px;">
                            <img :src="image_dir('{{ url('uploaded-images/') }}', image.filename)" 
                            style="width: 270px; height: 270px;"
                            alt="First slide">
                            <a class="btn btn-sm btn-danger" v-on:click="deleteImage(image.room_image_id)" style="margin-top: -52px;"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>