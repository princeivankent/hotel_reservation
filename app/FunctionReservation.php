<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionReservation extends Model
{
    protected $fillable = [
        'function_room_id',
        'checkin',
        'checkout',
        'timein',
        'timeout',
        'user_id',
        'total_cost'
    ];
    protected $primaryKey = 'function_reservation_id';
    public $timestamps = false;

    public function function_room()
    {
        return $this->belongsTo('App\FunctionRoom', 'function_room_id', 'function_room_id');
    }
}
