toastr.options = {
    showMethod: 'slideDown',
    hideMethod: 'slideUp',
    closeMethod: 'slideUp',
    timeOut: 8000,
    extendedTimeOut: 4000,
    progressBar: true,
    positionClass: 'toast-bottom-right'
}