@extends('layouts.guest_layout')

@push('styles')
	<link rel="stylesheet" href="{{ url('libraries/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-colorpicker.min.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-timepicker.min.css') }}">

	<style>
		.StripeElement {
			background-color: white;
			height: 40px;
			padding: 10px 12px;
			border-radius: 4px;
			border: 1px solid transparent;
			box-shadow: 0 1px 3px 0 #e6ebf1;
			-webkit-transition: box-shadow 150ms ease;
			transition: box-shadow 150ms ease;
		}
		
		.StripeElement--focus {
			box-shadow: 0 1px 3px 0 #cfd7df;
		}
		
		.StripeElement--invalid {
			border-color: #fa755a;
		}
		
		.StripeElement--webkit-autofill {
			background-color: #fefde5 !important;
		}
	</style>
@endpush

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1 style="color: white">Room Details</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ url('http://localhost/hotel_reservation') }}" class="text-gray">
				<i class="fa fa-home"></i>&nbsp;
				Home Page
			</a>
		</li>
		<li class="active">
			Booking Form
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div v-if="hasError" class="callout callout-danger">
		<h4>Warning!</h4>

		<ul v-for="error in errors">
			<li>@{{ error[0] }}</li>
		</ul>
	</div>
		
		<!-- Container -->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="nav-tabs-custom" style="background-color: #E8E4E0">
                <ul class="nav nav-tabs">
                    <li v-bind:class="`${first_tab}`">
                        <a href="#tab_1" data-toggle="tab">
                            <h4>
                                <span class="badge bg-green" style="padding: 5px 7px; margin-bottom: 2px;">1</span>&nbsp;
                                RESERVATION
                            </h4>
                        </a>
                    </li>
                    <li v-bind:class="`${second_tab}`">
                        <a href="#tab_2" data-toggle="tab">
                            <h4>
                                <span class="badge bg-green" style="padding: 5px 7px; margin-bottom: 2px;">2</span>&nbsp;
                                BILLING
                            </h4>
                        </a>
                    </li>
                    <li v-bind:class="`${third_tab}`">
                        <a href="#tab_3" data-toggle="tab">
                            <h4>
                                <span class="badge bg-green" style="padding: 5px 7px; margin-bottom: 2px;">3</span>&nbsp;
                                PAYMENT
                            </h4>
                        </a>
                    </li>
                    <li v-bind:class="`${fourth_tab}`">
                        <a href="#tab_4" data-toggle="tab">
                            <h4>
                                <span class="badge bg-green" style="padding: 5px 7px; margin-bottom: 2px;">4</span>&nbsp;
                                DONE
                            </h4>
                        </a>
                    </li>
                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div v-bind:class="`tab-pane ${first_tab}`" id="tab_1">
                        @include('components.guest.reservation_steps.reservation')
                    </div>
                    <div v-bind:class="`tab-pane ${second_tab}`" id="tab_2">
                        @include('components.guest.reservation_steps.billing')
                    </div>
                    <div v-bind:class="`tab-pane ${third_tab}`" id="tab_3">
                        @include('components.guest.reservation_steps.payment')
                    </div>
                    <div v-bind:class="`tab-pane ${fourth_tab}`" id="tab_4">
                        @include('components.guest.reservation_steps.done')
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-md-1"></div>    
    </div>

    @include('components.guest.modals.addons')
    @include('components.guest.modals.booked_rooms')
    @include('components.guest.modals.booking_details')
	</section>
@endsection

@push('scripts')
	<script src="{{ url('libraries/bootstrap-timepicker.min.js') }}"></script>
	<script>
        var room_type_id = "{{ $_GET['room_type_id'] ?? '' }}";
		var app = new Vue({
			el: '#app', 
			mixins: [mixin],
			data() {
				return {
					hasImage: false,
					isLongTime: true,
					form: {},
					room_types: [],
					room_rates: [],
					room_type: {},
					available_rooms: 0,
					addons: [],
					pickedAddons: [],
					booked_rooms: [],
					fields: {},
					days: 0,
					
					hasError: false,
					errors: {},
					
					cost: '', 		   // Room default price
					computed_cost: '', // total addons cost
					room_cost: 0,	   // total room cost
					grand_total: 0,	   // computed_cost + room_cost
					already_read: false,
					stripeToken: '',
                    transact_parameters: {},
                    
                    first_tab: 'active',
                    second_tab: '',
                    third_tab: '',
                    fourth_tab: ''
				}
			},
			watch: {
				computed_cost() {
					this.room_cost = parseInt(this.room_cost+this.computed_cost);
				},
				isLongTime() {
					if (!this.isLongTime) { // short time = 1
						this.showRoomRate(room_type_id);
						this.form.reservation_type_id = 1;
						
						// Initiate Time to display
						setTimeout(() => {
							$('.timepicker').timepicker({showInputs: false});
						});
					}
					else {
						this.form.reservation_type_id = 2;

						// Initiate Time to display
						setTimeout(() => {
							$('.timepicker').timepicker({showInputs: false});
						});
					}
					this.getRoomType(room_type_id, this.form.reservation_type_id);
                },
                
                first_tab() {
                    this.first_tab = 'active';
                    this.second_tab = '';
                    this.third_tab = '';
                    this.fourth_tab = '';
                },
                second_tab() {
                    this.second_tab = 'active';
                    this.first_tab = '';
                    this.third_tab = '';
                    this.fourth_tab = '';
                },
                third_tab() {
                    this.third_tab = 'active';
                    this.first_tab = '';
                    this.second_tab = '';
                    this.fourth_tab = '';
                },
                fourth_tab() {
                    this.fourth_tab = 'active';
                    this.first_tab = '';
                    this.second_tab = '';
                    this.third_tab = '';
                },
			},
			created() {
				this.getImages(room_type_id);
				this.getRoomType(room_type_id, this.form.reservation_type_id);
			},
			methods: {
				getRoomType: function(room_type_id, reservation_type_id) {
					axios.get(`${this.base_url}/guest/room_types/get/${room_type_id}/${reservation_type_id}`)
					.then(({data}) => {
						this.room_type = data.result;
						this.available_rooms = data.available_rooms;
						this.cost = data.cost;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
				},
				getImages: function(room_type_id) {
					axios.get(`${this.base_url}/guest/images/${room_type_id}`)
					.then(({data}) => {
						if (data.room_images.length > 0) {
							this.hasImage = true;
							this.room_types = data;
						}
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
				},
				grandTotal(params) {
					var fields = {};
					if (this.isLongTime) {
						var days = params.days;
						var computation = (days*params.cost) + params.addon_cost;
						fields = {
							total_cost: computation,
							total_paid: parseInt(computation/2)
						}
						
						this.fields = fields;
					} else {
						axios.get(`${this.base_url}/guest/room_rates/room_rate_id/${this.form.room_rate_id}`)
						.then(({data}) => {
							this.cost = data.cost;
							var days = 1;
							var computation = parseInt((days*data.cost) + this.computed_cost);
							fields = {
							    total_cost: computation,
							    total_paid: parseInt(computation/2)
							}
							
							this.fields = fields;
						})
						.catch((error) => {
							console.log(error.response);
						});
					}
				},
				book: function() {
					var addons = this.pickedAddons.length > 0 ? this.pickedAddons : null;
					var params = {};
					if (this.isLongTime) {
						params = {
							transaction_type_id: 1,
							reservation_type_id: 2, // 24 Hours Basis
							room_type_id: room_type_id,
							checkin: this.form.checkin,
							checkout: this.form.checkout,
							timein: document.getElementById('timein').value,
							timeout: document.getElementById('timein').value,
							persons: this.form.persons,
							addons: addons
						}
					}
					else {
						params = {
							transaction_type_id: 1,
							reservation_type_id: 1, // Hourly Rate Basis
							room_type_id: room_type_id,
							room_rate_id: this.form.room_rate_id,
							checkin: this.form.checkin,
							checkout: this.form.checkin,
							timein: document.getElementById('timein_hr').value,
							persons: this.form.persons,
							addons: addons
						}
					}

					if (this.form.checkin != null && this.form.checkout != null) {
						var date1 = new Date(this.form.checkin);
						var date2 = new Date(this.form.checkout);
						var timeDiff = Math.abs(date2.getTime() - date1.getTime());
						var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
						this.days = diffDays;
					}
					else {
						this.days = 1;
					}
					
					var params2 = {
						cost: this.cost,
						addon_cost: this.computed_cost,
						days: this.days,
					};

					this.grandTotal(params2);
					this.room_cost = parseInt(this.cost*this.days);
					this.room_cost = parseInt(this.room_cost+this.computed_cost);
					this.grand_total = this.room_cost;
					
					this.transact_parameters = params;
					
					setTimeout(() => {
						$('#booking_details').modal('show');
					}, 500);
				},
				showRoomRate: function(room_type_id) {
					axios.get(`${this.base_url}/guest/room_rates/room_type_id/${room_type_id}`)
					.then(({data}) => {
						this.room_rates = data;
					})
					.catch((err) => {
						console.log(err.response);
					});
				},
				showAddons: function() {
					this.getAddons();
					$('#addons-form').modal('show');
				},
				getAddons: function() {
					axios.get(`${this.base_url}/guest/addons/get`)
					.then(({data}) => {
						this.addons = data;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:', err.response.data.exception, false);
					});
				},
				chooseAddons: function(add_on_type_id, description, cost) {
					var hasDuplicate;
					this.pickedAddons.forEach(element => {
						if (element.add_on_type_id == add_on_type_id) {
							hasDuplicate = true;
						}
					});

					if (!hasDuplicate) {
						// Add item (1)
						this.pickedAddons.push({
							add_on_type_id: add_on_type_id,
							description: description,
							cost: cost,
							quantity: 1
						});
					}
					else {
						var i = 1;
						this.pickedAddons.forEach(element => {
							if (element.add_on_type_id == add_on_type_id) {
								element.cost += cost;
								element.quantity += parseInt(i);
							}
						});
					}

					this.computed_cost = parseInt(this.computed_cost + cost);
				},
				deductAddon: function(add_on_type_id, description, cost) {
					this.pickedAddons.forEach((element, index) => {
						if (element.add_on_type_id === add_on_type_id) {
							element.cost = element.cost - cost;
							element.quantity -= 1;
							this.computed_cost = this.computed_cost - cost;
							this.room_cost -= cost;

							if (element.cost === 0) {
								this.pickedAddons.splice(index, 1);
							}
						}
					});
				},
				removeAddon: function(cost, index) {
					this.computed_cost -= cost;
					this.pickedAddons.splice(index, 1);
				},
				getBookedRooms: function(room_type_id) {
					axios.get(`${this.base_url}/guest/booked_rooms/${room_type_id}`)
					.then(({data}) => {
						this.booked_rooms = data;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:', err.response.data.exception, false);
					});
				},
				openBookedRooms: function() {
					this.getBookedRooms(room_type_id);
					$('#booked_rooms').modal('show');
				},
				proceedToStripe() {
					$('#booking_details').modal('hide');
					$('#payment_form').modal('show');
				},
				finishTransaction() {
					return axios.post(`${this.base_url}/guest/reservation`, this.transact_parameters)
					.then(({data}) => {
						this.hasError = false;

						return true;
					})
					.catch((err) => {
						console.log(err.response);
						this.hasError = true;
						this.errors = err.response.data.errors;
					});
				},
				nextTab() {
					setTimeout(() => {
						this.second_tab = 'active';
						console.log('k');
					}, 4000);
				}
			}
		});

		//Timepicker
		$('.timepicker').timepicker({showInputs: false});
	</script>
@endpush
