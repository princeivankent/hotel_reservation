<!-- Addons -->
<div class="modal fade" tabindex="-1" role="dialog" id="addon_form">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Addons</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="description">Addon Description</label>
                    <textarea class="form-control" 
                    name="description" id="description" 
                    cols="30" rows="2" v-model="addon_form.description"></textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="addon_type">Addon Type</label>
                        <select v-model="addon_form.addon_type" 
                        class="form-control"
                        name="addon_type" id="addon_type">
                            <option value="receptionist">Receptionist</option>
                            <option value="guest">Guest</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="cost">Cost</label>
                        <input type="number"
                        class="form-control"
                        v-model="addon_form.cost"
                        v-on:keyup.enter="storeAddon">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" 
                v-on:click="storeAddon">
                    <i class="fa fa-check"></i>
                    SAVE CHANGES
                </button>
            </div>
        </div>
    </div>
</div>