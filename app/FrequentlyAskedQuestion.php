<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrequentlyAskedQuestion extends Model
{
    protected $fillable = ['question','answer'];
    protected $primaryKey = 'frequently_asked_question_id';
    public $timestamps = false;
}
