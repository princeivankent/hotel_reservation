@if (isset(Auth::user()->user_type))
    @if (Auth::user()->user_type == 'admin')
        <a href="{{ url('/admin/dashboard') }}">
            <v-toolbar-title class="hidden-sm-and-down orange--text darken-1">PARADISE HOTEL</v-toolbar-title>
        </a>
    @else 
        <a href="{{ url('/') }}">
            <v-toolbar-title class="hidden-sm-and-down orange--text darken-1">PARADISE HOTEL</v-toolbar-title>
        </a>
    @endif
@else
    <a href="{{ url('/') }}">
        <v-toolbar-title class="hidden-sm-and-down orange--text darken-1">PARADISE HOTEL</v-toolbar-title>
    </a>
@endif

<v-spacer></v-spacer>

<v-toolbar-items>
    <v-btn flat v-bind:color="home_tab" href="{{ url('/') }}">Home</v-btn>
    <v-btn flat v-bind:color="about_tab" href="{{ url('/about_us') }}">About Us</v-btn>
    <v-btn flat v-bind:color="faq_tab" href="{{ url('frequently_asked_questions') }}">FAQ</v-btn>
    <v-btn flat v-bind:color="faq_tab" href="{{ route('comments') }}">Comments & Suggestions</v-btn>
</v-toolbar-items>

<v-divider class="mr-3" inset vertical dark></v-divider>

@if (Route::has('login'))
    @auth
    <v-menu offset-y>
        <v-btn
            slot="activator"
            color="success"
            dark
            @click.stop="drawer = !drawer"
        >
            <v-icon class="mr-1">ion ion-ios-contact</v-icon>&nbsp;
            {{ Auth::user()->name }}
        </v-btn>
    </v-menu>
    
    @else
        <h4>
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a>
        </h4>
    @endauth
@endif