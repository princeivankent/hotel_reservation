<v-card
    class="mb-5"
>
    <v-card-text>
        <v-layout row wrap justify-center>
            <v-flex xs12 sm12 md5>
                <v-form v-model="valid" class="mt-3" lazy-validation>
                    <v-dialog
                        ref="dialog1"
                        v-model="modal1"
                        :return-value.sync="checkin_input"
                        persistent
                        lazy
                        full-width
                        width="290px"
                        light
                    >
                        <v-text-field
                            slot="activator"
                            v-model="form.checkin"
                            label="Check IN"
                            append-icon="fa fa-calendar"
                            outline
                            readonly
                        ></v-text-field>
                        <v-date-picker v-model="form.checkin" v-on:change="checkDateAvailability" scrollable>
                            <v-spacer></v-spacer>
                            <v-btn flat color="primary" v-on:click="modal1 = false">Cancel</v-btn>
                            <v-btn flat color="primary" v-on:click="$refs.dialog1.save(checkin_input)">OK</v-btn>
                        </v-date-picker>
                    </v-dialog>

                    <v-dialog
                        ref="dialog"
                        v-model="modal2"
                        :return-value.sync="checkout_input"
                        persistent
                        lazy
                        full-width
                        width="290px"
                        light
                    >
                        <v-text-field
                            slot="activator"
                            v-model="form.checkout"
                            label="Check OUT"
                            :rules="checkOutRule"
                            append-icon="fa fa-calendar"
                            outline
                            readonly
                        ></v-text-field>
                        <v-date-picker v-model="form.checkout" scrollable>
                            <v-spacer></v-spacer>
                            <v-btn flat color="primary" v-on:click="modal2 = false">Cancel</v-btn>
                            <v-btn flat color="primary" v-on:click="$refs.dialog.save(checkout_input)">OK</v-btn>
                        </v-date-picker>
                    </v-dialog>

                    <v-text-field 
                    v-model="form.persons"
                    label="No. of Person(s)"
                    :rules="numberRule"
                    outline
                    append-icon="fa fa-users" 
                    ></v-text-field>
                </v-form>
            </v-flex>
        </v-layout>
    </v-card-text>
</v-card>