<v-layout row justify-center>
    <v-dialog v-model="paylater_form" max-width="500" light>
        <v-card>
            <v-card-title class="headline">
                <i class="fa fa-user"></i>&nbsp;
                Administrator's Bank Account
            </v-card-title>

            <v-card-text>
                <v-data-table
                hide-headers
                :items="account_numbers"
                hide-actions
                class="elevation-2 mb-3"
                >
                    <template slot="items" slot-scope="props">
                        <td class="text-lg-center">@{{ props.item.bank_name }}</td>
                        <td class="text-lg-center">@{{ props.item.account_number }}</td>
                    </template>
                </v-data-table>

                <v-layout row wrap>
                    <v-flex md2>
                        <img src="{{ url('images/credit/visa.png') }}" alt="" width="60" height="40">
                    </v-flex>
                    <v-flex md2>
                        <img src="{{ url('images/credit/mastercard.png') }}" alt="" width="60" height="40">
                    </v-flex>
                    <v-flex md2>
                        <img src="{{ url('images/credit/cirrus.png') }}" alt="" width="60" height="40">
                    </v-flex>
                </v-layout>
            </v-card-text>

            <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="green darken-1" dark v-on:click="saveForm(0)">Got it!</v-btn>
            </v-card-actions>
        </v-card>
    </v-dialog>
</v-layout>