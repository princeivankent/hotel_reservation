@extends('welcome')

@push('styles')
	<style>
		.card--flex-toolbar {
			margin-top: -64px;
		}

		.swal-title {
			font-family: sans-serif;
		}

		.swal-text {
			font-family: sans-serif;
			text-align: center;
		}

		.swal-button {
			font-family: sans-serif;
			background-color: #A5DC86;
		}
	</style>
@endpush

@section('content')

<section>
	<v-container grid-list-xl>
		<v-card flat>
			<v-layout row pb-2 style="background-image: url('{{ asset('images/background2.JPG') }}'); height: 89vh;">
				<v-flex xs12 sm12 md8 offset-xs2>
					<v-card>
						<v-toolbar card prominent>
							<v-toolbar-title class="body grey--text">Reservation</v-toolbar-title>
						</v-toolbar>
						
						<v-divider></v-divider>
						
						<v-card-text>
							<v-stepper v-model="e1">
								<v-stepper-header>
									<v-stepper-step :editable="step1" :complete="e1 > 1" step="1">Reservation</v-stepper-step>
							
									<v-divider></v-divider>
							
									<v-stepper-step :editable="step2" :complete="e1 > 2" step="2">Booking Summary</v-stepper-step>
							
									<v-divider></v-divider>
							
									<v-stepper-step :editable="step3" :complete="e1 > 3" step="3">Payment</v-stepper-step>
								</v-stepper-header>
						
								<v-stepper-items>
									<v-stepper-content step="1">
										<v-alert
										v-if="dateNotAvailable"
										:value="true"
										type="error"
										>
											Check IN date you picked not available. Please select another, Thank you!
										</v-alert>
										@include('components.guest.reservation_steps.reservation_form')
							
										<v-btn
											v-bind:disabled="step1Invalid"
											color="primary"
											v-on:click="step(2)"
										>
											Continue
										</v-btn>
									</v-stepper-content>
							
									<v-stepper-content step="2">
										@include('components.guest.reservation_steps.booking_summary')
							
										<v-btn
											color="primary"
											v-on:click="terms = true"
										>
											Continue
										</v-btn>
									</v-stepper-content>
							
									<v-stepper-content step="3">
										@include('components.guest.reservation_steps.payment')
								
										<v-btn
											color="primary"
											v-on:click="step(4)"
										>
											Continue
										</v-btn>
									</v-stepper-content>
								</v-stepper-items>
							</v-stepper>
						</v-card-text>
					</v-card>
				</v-flex>
			</v-layout>
		</v-card>
	</v-container>
</section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        var base_url = "{{ url('/') }}";
		var room_type_id = "{{ $_GET['room_type_id'] }}";
        new Vue({
            el: '#app',
            data() {
                return {
					e1: 1,
					step1: true,
					step2: false,
					step3: false,

					step1Invalid: true,
					step2Invalid: true,
					step2Invalid: true,
					step3Invalid: true,

					valid: false,
					payment_form: false,
					paylater_form: false,
					terms: false,

					checkin_input: null,
					checkout_input: null,
					date2: null,
					modal1: false,
					modal2: false,
					form: {},
					deposit_slip: '',
					invalidUpload: true,
					dateNotAvailable: false,
					booking_summary: {},
					account_numbers: [],
					room_capacity: 0,

					// Rules
					numberRule: [
						v => !!v || 'No. of persons is required',
					],
					checkOutRule: [
						v => !!v || 'Checkout is required',
						v => new Date(v) >= new Date(this.form.checkin) || 'Check IN must not be ahead from Check OUT'
					],
					fileRule: [
						v => !!v || 'file is required'
					],
                }
			},
			computed: {
				checkin() {
					return this.form.checkin;
				},
				checkout() {
					return this.form.checkout;
				},
				persons() {
					return this.form.persons;
				}
			},
			watch: {
				e1() {
					if (this.e1 == 2) return this.bookingSummary();
				},
				checkin() {
					if (this.form.checkin > this.form.checkout) {
						this.step1Invalid = true;
					}
					else {
						this.step1Invalid = false;
					}
				},
				checkout() {
					if (this.form.checkin == 0 || this.form.checkin == null) {
						this.step1Invalid = true;
					}
					if (this.form.checkin > this.form.checkout) {
						this.step1Invalid = true;
					}
					else {
						this.step1Invalid = false;
					}
				},
				persons() {
					if (this.form.persons > this.room_capacity) {
						return this.step1Invalid = true;
					}
					else {
						return this.step1Invalid = false;
					}
				}
			},
			created() {
				this.showAdminBankAccount();
				this.getRoomCapacity(room_type_id);
			},
			methods: {
				getRoomCapacity: function(room_type_id) {
					axios.get(`${this.base_url}/guest/room_capacity/get/${room_type_id}`)
					.then(({data}) => {
						this.room_capacity = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
				},
				step(step) {
					this.e1 = step;
					if (step == 2) this.step2 = true;
					else if (step == 3) this.step3 = true;
				},
				termsAlreadyRead() {
					this.terms = false;
					this.step(3);
				},

				checkDateAvailability(event) {
					this.startDateChange();
					var data = {
						room_type_id: "{{ $_GET['room_type_id'] }}",
						checkin: this.form.checkin
					};

					axios.post(`${this.base_url}/guest/check_date_availability`, data)
					.then(({data}) => {
						if (data) {
							this.dateNotAvailable = true;
							this.step1Invalid = true;
						}
						else {
							this.step1Invalid = false;
							this.dateNotAvailable = false;
						}
					})
					.catch((error) => {
						console.log(error.response);
					});
				},

				onImageChange(e) {
					let files = e.target.files || e.dataTransfer.files;
					if (!files.length)
						return;

					this.invalidUpload = false;
					this.createImage(files[0]);
				},
				createImage(file) {
					let reader = new FileReader();
					let vm = this;
					reader.onload = (e) => {
						vm.deposit_slip = e.target.result;
					};
					reader.readAsDataURL(file);
				},
				saveForm(pay_method) {
					var room_type_id = "{{ $_GET['room_type_id'] }}";
					if (this.form.checkin == null) return false;

					axios.get(`${this.base_url}/guest/room_capacity/${room_type_id}`)
					.then(({data}) => {
						if (data < this.form.persons) {
							return swal({
								title: "Ooops!",
								text: data + " is the capacity of room.",
								icon: "error",
								button: false,
								timer: 4000,
							})
						} 
					})
					.catch((error) => {
						console.log(error.response);
					});

					var params = {
						room_type_id: room_type_id,
						deposit_slip: pay_method ? this.deposit_slip : null,
						checkin: this.form.checkin,
						checkout: this.form.checkout,
						persons: this.form.persons
					};
					
					axios.post(`${this.base_url}/guest/create_reservation`, params)
					.then(({data}) => {
						swal({
							title: "Alright!",
							text: 'You are successfully reserved. Please wait for the confirmation.',
							icon: "success",
							closeOnClickOutside: false
						})
						.then((res) => {
							window.location.href = "{{ route('home') }}";
						});

						this.form = {};
						this.paylater_form = false;
					})
					.catch((err) => {
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},
				bookingSummary() {
					var params = {
						room_type_id: "{{ $_GET['room_type_id'] }}",
						checkin: this.form.checkin,
						checkout: this.form.checkout,
						persons: this.form.persons
					};

					axios.post(`${this.base_url}/guest/booking_summary`, params)
					.then(({data}) => {
						this.booking_summary = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
				},
				showAdminBankAccount() {
					axios.get(`${this.base_url}/guest/bank_accounts/get`)
					.then(({data}) => {
						this.account_numbers = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
				}
			}
        })
    </script>
@endpush