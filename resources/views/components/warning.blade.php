@extends('verify_first')

@section('content')
    <section>
        <v-container grid-list-xl>
            <v-layout row wrap justify-center class="my-5">
                <v-flex xs12 sm12 md12>
                    <v-card flat class="transparent">
                        <v-card-title primary-title class="layout justify-center">
                        <div class="headline" id="about_us">
                            <v-alert
                            :value="true"
                            type="error"
                            >
                            You must verify your account first! We have sent you an email containing One Time Login(OTL) Code.
                            </v-alert>
                        </div>
                        </v-card-title>
                    </v-card>
                </v-flex>
            </v-layout>
        </v-container>
    </section>
@endsection

@push('scripts')
    <script>
        var user_id = "@auth{{Auth::user()->user_id}}@else{{0}}@endauth";
        var base_url = "{{ url('/') }}";
        new Vue({
            el: '#app',
            data() {
                return {
                    
                }
            },
            created() {
                
            },
            mounted() {
                
            },
            methods: {
                
            }
        })
    </script>
@endpush