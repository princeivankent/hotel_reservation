@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rooms and Utilities
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h1 class="box-title">
                    Rooms
                </h1>
                <button 
                class="btn btn-primary pull-right" v-on:click="showForm">
                    <i class="fa fa-plus"></i>
                    Add New Room
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Room Code</th>
                            <th class="text-center" width="800">Room Type</th>
                            <th class="text-center">Availability</th>
                            <th class="text-center" width="100">Maintenance Mode</th>
                            <th class="text-center" width="100">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items"
                        v-bind:key="index">
                            <td class="text-center">@{{ item.room_code }}</td>
                            <td class="text-center text-primary"><strong>@{{ item.room_type.room_name }}</strong></td>
                            <td class="text-center">
                                <span v-if="item.reservation_id > 0" class="label label-danger">
                                    <i class="fa fa-times"></i>
                                    Occupied
                                </span>
                                <span v-if="item.is_maintenance != 0" class="label label-danger">
                                    <i class="fa fa-times"></i>
                                    under maintenance
                                </span>
                                <span v-else class="label label-success">
                                    <i class="fa fa-check"></i>
                                    maintained
                                </span>
                            </td>
                            <td class="text-center">
                                <a v-on:click="toggle(item.room_id, item.is_maintenance)" style="cursor: pointer;">
                                    <i v-if="item.is_maintenance" class="fa fa-toggle-on fa-2x"></i>
                                    <i v-else class="fa fa-toggle-off fa-2x"></i>
                                </a>
                            </td>
                            <!-- Buttons -->
                            <td class="text-center">
                                <button class="btn btn-danger btn-sm" v-on:click="deleteItem(item.room_id, index)">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-default">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@{{ formTitle }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="room_type_id">Room Type</label>
                        <select v-model="form.room_type_id" class="form-control" id="room_type_id">
                            <option v-for="room_type in room_types" 
                            :value="room_type.room_type_id">@{{ room_type.room_name }}</option>    
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="room_code">Room Code</label>
                        <input type="text" class="form-control" v-model="form.room_code">
                    </div>
                </div>
                <div class="modal-footer">
                    <button v-on:click="saveForm" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        document.querySelector('#_rooms').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    dialog: false,
                    items: [],
                    itemIndex: -1,
                    room_types: [],
                    room_type_id: 0,

                    form: {
                        room_type_id: 0,
                        room_code: ''
                    },
                    defaultItem : {
                        room_type_id: 0,
                        room_code: ''
                    }
                }
            },
            computed: {
                formTitle () {
                    return this.itemIndex === -1 ? 'New Item' : 'Edit Item'
                }
            },
            created() {
                this.getItems();
            },
            mounted() {
                
            },
            methods: {
                getItems: function() {
                    axios.get(`${this.base_url}/admin/rooms/get`)
                    .then(({data}) => {
                        this.items = data;

                        setTimeout(() => {
                            $('#table').DataTable();   
                        });
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                editItem: function(room_id, index) {
                    axios.get(`${this.base_url}/admin/rooms/get/${room_id}`)
                    .then(({data}) => {
                        console.log(data);
                        this.form = Object.assign({}, data);
                        this.itemIndex = index;
                        $('#modal-default').modal('show');
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                deleteItem: function(room_id, index) {
                    var msg = confirm('Are you sure you want to delete this?');
                    if (msg) {
                        axios.delete(`${this.base_url}/admin/rooms/delete/${room_id}`)
                        .then(({data}) => {
                            this.items.splice(index, 1);
                            this.notify('Successfully deleted!', true);
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    }
                },

                showForm() {
                    $('#modal-default').modal('show');
                    this.fetchRoomTypes();
                    this.itemIndex = -1;
                },

                close() {
                    this.dialog = false;
                    $('#modal-default').modal('hide');
                    setTimeout(() => {
                        this.form = Object.assign({}, this.defaultItem)
                        this.itemIndex = -1
                    }, 300)
                },

                saveForm: function() {
                    if (this.itemIndex > -1) {
                        axios.put(`${this.base_url}/admin/rooms/update/${this.form.room_id}`, this.form)
                        .then(({data}) => {
                            this.close();
                            this.notify('Successfully saved!', true);
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    } else {
                        axios.post(`${this.base_url}/admin/rooms/store`, this.form)
                        .then(({data}) => {
                            this.close();
                            this.notify('Successfully saved!', true);
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    }
                    this.getItems();
                },

                fetchRoomTypes() {
                    axios.get(`${this.base_url}/admin/room_types/getAll`)
                    .then(({data}) => {
                        this.room_types = data;
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },
                toggle: function(room_id, status) {
                    if (status) status = 0;
                    else status = 1;
                    axios.put(`${this.base_url}/admin/toggle_maintenance/${room_id}/${status}`)
                    .then(({data}) => {
                        this.getItems();
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                }
            }
        });
    </script>
@endpush