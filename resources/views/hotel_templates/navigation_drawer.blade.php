<!-- Navigation Drawer -->
@if (Route::has('login'))
@auth
    <v-navigation-drawer
    v-model="drawer"
    absolute
    temporary
    right
    fixed
    >
        <v-list class="pa-1">
            <v-list-tile avatar>
                <v-list-tile-avatar>
                    <img src="{{ asset('admin-images/avatar5.png') }}">
                </v-list-tile-avatar>
                
                <v-list-tile-content>
                    <v-list-tile-title>{{ Auth::user()->name }}</v-list-tile-title>
                </v-list-tile-content>
            </v-list-tile>
        </v-list>
        
        <v-list class="pt-0" dense>
            <v-divider></v-divider>
            
            <v-list-tile v-on:click="booking_status_dialog = true">
                <v-list-tile-action>
                    <v-icon>book</v-icon>
                </v-list-tile-action>
                
                <v-list-tile-content>
                    <v-list-tile-title>Booking Status</v-list-tile-title>
                </v-list-tile-content>
            </v-list-tile>

            <v-list-tile 
            href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">

                <v-list-tile-action>
                    <v-icon>fa fa-sign-out</v-icon>
                </v-list-tile-action>

                <v-list-tile-content>
                    <v-list-tile-title>
                        Logout
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </v-list-tile-title>
                </v-list-tile-content>
                
            </v-list-tile>
        </v-list>
    </v-navigation-drawer>
@endauth
@endif