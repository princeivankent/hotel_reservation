<!DOCTYPE html>
<html>
	<head>
		<title>Paradise Hotel</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('libraries/admin/css/ionicons.min.css') }}">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Vuejs/jQuery/Axios -->
		<script src="{{ asset('js/vuetify.js') }}"></script>

		<style>
            body, html {
                overflow: hidden;
            }

			a {
				text-decoration: none;
				margin: 10px;
			}
		</style>
	</head>

	<body>
		<div id="app">
			<v-app style="background-image: url('{{ asset('images/background.jpg') }}');">
				<v-toolbar color="#21130A">
                    <a href="{{ url('/') }}" class="white--text">
                        <v-toolbar-title>PARADISE HOTEL</v-toolbar-title>
                    </a>
					<v-spacer></v-spacer>
					@if (Route::has('login'))
						@auth
							<v-toolbar-title>
								<a href="{{ url('/home') }}">Home</a>
							</v-toolbar-title>
						@else
							<h4>
								<a href="{{ route('login') }}">Login</a>
								<a href="{{ route('register') }}">Register</a>
							</h4>
						@endauth
					@endif
                </v-toolbar>
                
				<v-content>
                    <section>
                        <v-layout
                            column
                            wrap
                            class="my-5"
                            align-center>
                        
                        </v-layout>

                        <v-layout
                            row
                            wrap
                            align-center>

                            <v-flex md4></v-flex>
                            <v-flex sm12 md4>
                                <v-container grid-list-xl>
                                    <v-layout row wrap align-center>
                                        <v-flex xs12 md12>
                                            <v-card class="elevation-4">
                                                <v-card-text class="text-xs-center">
                                                    <v-icon x-large class="blue--text">ios ion-ios-person</v-icon>
                                                </v-card-text>
                                                <v-card-text>
                                                    <v-layout>
                                                        <v-flex md2></v-flex>
                                                        <v-flex md8>
                                                            <form method="POST" action="{{ route('login') }}" ref="form" lazy-validation>
                                                                @csrf

                                                                <v-text-field
                                                                
                                                                id="email" 
                                                                type="email" 
                                                                name="email" 
                                                                value="{{ old('email') }}"
                                                                
                                                                :rules="emailRules"
                                                                label="E-mail"
                                                                solo
                                                                autofocus
                                                                required
                                                                ></v-text-field>

                                                                <v-text-field

                                                                id="password" 
                                                                type="password" 
                                                                name="password"

                                                                :rules="passwordRules"
                                                                label="Password"
                                                                solo
                                                                required
                                                                ></v-text-field>
                                                            
                                                                {{-- <v-checkbox
                                                                class="mt-0"
                                                                v-model="checkbox"
                                                                label="Remember Me?"
                                                                ></v-checkbox> --}}
                                
                                                                {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                                                    {{ __('Forgot Your Password?') }}
                                                                </a> --}}

                                                                <div class="text-xs-center">
                                                                    <v-btn color="green" small dark v-on:click="clear">
                                                                        <v-icon class="mr-1">clear</v-icon>
                                                                        Clear
                                                                    </v-btn>
    
                                                                    <v-btn
                                                                    type="submit"
                                                                    :disabled="!valid"
                                                                    color="primary"
                                                                    small>
                                                                        <v-icon class="mr-1" x-small>check</v-icon>
                                                                        Login
                                                                    </v-btn>
                                                                </div>
                                                            </form>
                                                            
                                                        </v-flex>
                                                        <v-flex md2></v-flex>
                                                    </v-layout>

                                                    @if ($errors->has('email'))
                                                        <v-alert
                                                        :value="true"
                                                        type="error"
                                                        >
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </v-alert>
                                                    @endif
                                                
                                                </v-card-text>
                                            </v-card>
                                        </v-flex>
                                    </v-layout>
                                </v-container>
                            </v-flex>
                            <v-flex md4></v-flex>
                        </v-layout>
                        
                    </section>

                    {{-- <v-footer color="blue darken-2" bottom fixed>
                        <v-layout row wrap align-center>
                        <v-flex xs12>
                            <div class="white--text ml-3 text-xs-center">
                                <strong>Copyright &copy; 2016<a class="white--text text--darken-2" href="http://paradise.com">Paradise Hotel</a>.</strong> All rights reserved.
                            </div>
                        </v-flex>
                        </v-layout>
                    </v-footer> --}}
				</v-content>
			</v-app>
		</div>
	
		<script>
			new Vue({
				el: '#app',
				data () {
					return {
                        alert: true,
						title: 'Company Title',
                        valid: true,
                        password: '',
                        passwordRules: [
                            v => !!v || 'Password is required'
                        ],
                        email: '',
                        emailRules: [
                            v => !!v || 'E-mail is required',
                            v => /.+@.+/.test(v) || 'E-mail must be valid'
                        ], 
                        checkbox: false
					}
                },
                created() {
                    localStorage.setItem('isLogged', 1);
                },
                methods: {
                    clear () {
                        this.$refs.form.reset()
                    }
                }
			})
		</script>
	</body>
</html>
