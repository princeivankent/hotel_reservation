<div class="modal fade" tabindex="-1" role="dialog" id="guest_addons">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i>
                    Addons
                </h4>
            </div>
            <div class="modal-body">
                {{-- <table id="addon_table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" width="40">#</th>
                            <th class="text-center">DESCRIPTION</th>
                            <th class="text-center">COST</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items.reservations."
                        v-bind:key="index">
                            <td class="text-center">@{{ index+1 }}</td>
                            <td class="text-center text-primary">@{{ item.description }}</td>
                            <td class="text-center text-danger">P @{{ item.cost }}</td>
                        </tr>
                    </tbody>
                </table> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                    CLOSE
                </button>
            </div>
        </div>
    </div>
</div>