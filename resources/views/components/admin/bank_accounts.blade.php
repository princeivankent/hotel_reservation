@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Administrator's Bank Account
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">Bank Accounts</h1>
                <button class="btn btn-default btn-sm pull-right" v-on:click="createBankAccount">
                    <i class="fa fa-plus-circle"></i>&nbsp;
                    Add Bank Account
                </button>
            </div>
            <div class="box-body">
                <table id="data-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>#</th>
                            <th>Bank Name</th>
                            <th>Account Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items"
                        v-bind:key="index">
                            <td class="text-primary" width="100">
                                <div class="input-group">
                                    <div class="input-group-btn" style="padding: -2px 20px;">
                                        <button type="button" class="btn btn-sm btn-defualt dropdown-toggle" data-toggle="dropdown">
                                            ACTION &nbsp;
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu box-shadow table-menu">
                                            <li>
                                                <a v-on:click="">
                                                    <i class="ion ion-android-checkmark-circle text-primary text-bold"></i>&nbsp;
                                                    Edit
                                                </a>
                                            </li>
                                            <li>
                                                <a v-on:click="deleteBankAccount(item.bank_account_id)">
                                                    <i class="ion ion-android-checkmark-circle text-primary text-bold"></i>&nbsp;
                                                    Delete
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                            <td>@{{ index+1}}</td>
                            <td>@{{ item.bank_name }}</td>
                            <td>@{{ item.account_number }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('components.admin.modals.bank_account_modal')
@endsection

@push('scripts')
    <script>
        document.querySelector('#bank_account_tab').setAttribute('class', 'active');
        new Vue({
        el: '#app', 
        mixins: [mixin],
        data() {
            return {
                items: [],
                isEdit: false,
                form: {}
            }
        },
        computed: {
            formTitle () {
                return this.isEdit ? 'Edit Bank Account' : 'Add Bank Account'
            }
        },
        created() {
            this.getItems();
        },
        methods: {
            getItems() {
                axios.get(`${this.base_url}/admin/bank_accounts/get`)
                .then(({data}) => {
                    this.items = data;

                    setTimeout(() => {
                        $('#data-table').dataTable();
                    });
                })
                .catch((error) => {
                    console.log(error.response);
                });
            },
            createBankAccount() {
                this.showCreateForm();
            },
            saveForm() {
                axios.post(`${this.base_url}/admin/bank_accounts/store`, this.form)
                .then(({data}) => {
                    if (data) {
                        this.closeForm();
                        this.getItems();
                        return swal('Success!', 'New Bank Account has been added.', 'success');
                    }
                    else return swal('Oops!', 'Something went wrong.', 'error');

                })
                .catch((error) => {
                    console.log(error.response);
                });
            },
            deleteBankAccount(bank_account_id) {
                swal({
					title: "Are you sure to delete this account number?",
					text: "Note: There's no way to undo changes",
					icon: "warning",
					dangerMode: true,
					buttons: {
						cancel: true,
						confirm: 'Proceed'
					}
				})
				.then(res => {
					if (res) {
						axios.delete(`${this.base_url}/admin/bank_accounts/destroy/${bank_account_id}`)
                        .then(({data}) => {
                            if (data) {
                                this.getItems();
                                return swal('Success!', 'Account Number has been deleted!', 'success');
                            }
                            else return swal('Oops!', 'Something went wrong.', 'error');
                        })
                        .catch((error) => {
                            console.log(error.response);
                        });
					}
				});
            },

            showCreateForm() {
                $('#bank_account_modal').modal('show');
                this.isEdit = false;
            },
            closeForm() {
                $('#bank_account_modal').modal('hide');
                this.isEdit = false;
                this.form = {};
            }
        }
    });
    </script>
@endpush
