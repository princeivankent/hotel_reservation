<div class="modal fade" tabindex="-1" role="dialog" id="addons-form">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-green">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i>
                    Available Addons
                </h4>
            </div>
            <div class="modal-body">
                <table id="addon_table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" width="40">#</th>
                            <th class="text-center">DESCRIPTION</th>
                            <th class="text-center">COST</th>
                            <th class="text-center" width="100">INCLUDE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in addons"
                        v-bind:key="index">
                            <td class="text-center">@{{ index+1 }}</td>
                            <td class="text-center text-primary">@{{ item.description }}</td>
                            <td class="text-center text-danger">@{{ item.cost | phCurrency }}</td>
                            <!-- Buttons -->
                            <td class="text-center">
                                <button class="btn btn-primary btn-sm" v-on:click="chooseAddons(item.add_on_type_id, item.description, item.cost)">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-danger btn-sm" v-on:click="deductAddon(item.add_on_type_id, item.description, item.cost)">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                    CLOSE
                </button>
            </div>
        </div>
    </div>
</div>