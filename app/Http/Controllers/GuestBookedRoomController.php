<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GuestBookedRoomController extends Controller
{
    public function booked_rooms($room_type_id)
    {
        $query = DB::table('rooms as rm') 
            ->select(
                'rm.*',
                'rs.checkout',
                'rs.timeout'
            )
            ->leftJoin('reservations as rv', 'rv.reservation_id', '=', 'rm.reservation_id')
            ->leftJoin('room_schedules as rs', 'rs.room_schedule_id', '=', 'rv.room_schedule_id')
            ->where([
                ['rm.room_type_id', '=', $room_type_id]
            ])
            ->get();

        foreach ($query as $key => $value) {
            if ($value->timeout) {
                // $value->timeout = 'tangina';
                $minutes = Carbon::parse($value->timeout)->format('i');
                $value->timeout = Carbon::parse($value->timeout)->format('h') + 4 .':'. $minutes; // Military Time
                $value->timeout = Carbon::parse($value->timeout)->format('h:i A');  // Standard Time
            }
        }

        return response()->json($query);
    }
}
