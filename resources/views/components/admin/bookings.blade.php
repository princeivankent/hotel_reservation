@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Customer's Transactions
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">Bookings Track</h1>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Guest Name</th>
                            <th>Email Address</th>
                            <th>Room Type</th>
                            <th>Designated Room</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Total Hours</th>
                            <th>Total Cost</th>
                            <th class="text-center" width="60">Acknowledged</th>
                            <th class="text-center" width="60">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items"
                        v-bind:key="index">
                            <td class="text-primary">
                                <i class="fa fa-user"></i> 
                                <strong>
                                    @{{ item.name }}
                                </strong>
                            </td>
                            <td>@{{ item.email }}</td>
                            <td>@{{ item.room_name }}</td>
                            <td>@{{ item.room_code }}</td>
                            <td>@{{ item.checkin }}</td>
                            <td>@{{ item.checkout }}</td>
                            <td>@{{ item.total_hours }}</td>
                            <td class="text-danger">P @{{ item.total_cost }}</td>
                            <td>
                                <span v-if="item.is_acknowledged" class="label label-success">
                                    <i class="fa fa-check"></i>
                                    acknowleged
                                </span>
                                <button v-else class="btn btn-danger btn-sm"
                                v-on:click="acknowledge(item.reservation_id)">
                                    <i class="fa fa-exclamation-triangle" style="margin-right: 4px;"></i>
                                    Acknowlege me!
                                </button>
                            </td>
                            <!-- Buttons -->
                            <td class="text-center">
                                <button class="btn btn-danger btn-sm" v-on:click="cancelReservation(item.room_id, index)">
                                    <i class="fa fa-user-times"></i>
                                    Cancel
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    {{-- <div class="modal fade" tabindex="-1" role="dialog" id="modal-default">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@{{ formTitle }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="room_type_id">Room Type</label>
                        <select v-model="form.room_type_id" class="form-control" id="room_type_id">
                            <option v-for="room_type in room_types" 
                            :value="room_type.room_type_id">@{{ room_type.room_name }}</option>    
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="room_code">Room Code</label>
                        <input type="text" class="form-control" v-model="form.room_code">
                    </div>
                </div>
                <div class="modal-footer">
                    <button v-on:click="saveForm" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@push('scripts')
    <script>
        document.querySelector('#_active_bookings').setAttribute('class', 'active');
        new Vue({
        el: '#app', 
        mixins: [mixin],
        data() {
            return {
                dialog: false,
                items: [],
                itemIndex: -1,
                room_types: [],
                room_type_id: 0,

                form: {
                    room_type_id: 0,
                    room_code: ''
                },
                defaultItem : {
                    room_type_id: 0,
                    room_code: ''
                }
            }
        },
        computed: {
            formTitle () {
                return this.itemIndex === -1 ? 'New Item' : 'Edit Item'
            }
        },
        created() {
            this.getItems();
        },
        mounted() {
            
        },
        methods: {
            getItems: function() {
                axios.get(`${this.base_url}/admin/active_bookings/get`)
                .then(({data}) => {
                    this.items = data;

                    setTimeout(() => {
                        $('#table').DataTable();   
                    });
                })
                .catch((err) => {
                    console.log(err.response);
                });
            },

            editItem: function(room_id, index) {
                axios.get(`${this.base_url}/admin/rooms/get/${room_id}`)
                .then(({data}) => {
                    console.log(data);
                    this.form = Object.assign({}, data);
                    this.itemIndex = index;
                    $('#modal-default').modal('show');
                })
                .catch((err) => {
                    console.log(err.response);
                });
            },

            deleteItem: function(room_id, index) {
                var msg = confirm('Are you sure you want to delete this?');
                if (msg) {
                    axios.delete(`${this.base_url}/admin/rooms/delete/${room_id}`)
                    .then(({data}) => {
                        this.items.splice(index, 1);
                        this.notify('Successfully deleted!', true);
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                }
            },

            showForm() {
                $('#modal-default').modal('show');
                this.fetchRoomTypes();
                this.itemIndex = -1;
            },

            close() {
                this.dialog = false;
                $('#modal-default').modal('hide');
                setTimeout(() => {
                    this.form = Object.assign({}, this.defaultItem)
                    this.itemIndex = -1
                }, 300)
            },

            saveForm: function() {
                if (this.itemIndex > -1) {
                    axios.put(`${this.base_url}/admin/rooms/update/${this.form.room_id}`, this.form)
                    .then(({data}) => {
                        this.close();
                        this.notify('Successfully saved!', true);
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                } else {
                    axios.post(`${this.base_url}/admin/rooms/store`, this.form)
                    .then(({data}) => {
                        this.close();
                        this.notify('Successfully saved!', true);
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                }
                this.getItems();
            },

            fetchRoomTypes() {
                axios.get(`${this.base_url}/admin/room_types/getAll`)
                .then(({data}) => {
                    this.room_types = data;
                })
                .catch((err) => {
                    console.log(err.response);
                });
            },

            acknowledge: function(reservation_id) {
                axios.put(`${this.base_url}/admin/acknowledge/update/${reservation_id}`)
                .then(({data}) => {
                    this.notify('Successfully acknowledged!', true);
                    this.getItems();
                })
                .catch((err) => {
                    console.log(err.response);
                });
            }
        }
    });
    </script>
@endpush
