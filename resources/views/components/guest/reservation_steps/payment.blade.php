<v-card
    class="mb-5"
>
    <v-card-text>
        <v-layout row wrap justify-center>
            <v-flex xs12 sm12 md6>
                <div>
                    <v-alert
                        color="info"
                        value="true"
                        outline
                    >
                        <h3 class="mb-2">
                            <i class="fa fa-question-circle"></i>
                            Instructions:
                        </h3>
                        <v-divider class="mb-2"></v-divider>
                        <p>
                            <ol>
                                <li>Click Pay Later to view Hotel's Bank Account</li>
                                <li>Click Pay Later Button to Reserve the Chosen Room</li>
                                <li>If you are an old customer you can click browse and attach the Deposit Slip and Click Send after<li>
                                <li>Upload Deposit Slip after Sign-In within 24 hrs or else the Reservation will be cancelled</li>
                            </ol>
                        </p>
                    </v-alert>
                </div>
            </v-flex>
            <v-flex xs12 sm12 md6>
                <v-form v-model="payment_form" class="mt-3">
                    <input type="file" 
                    v-on:change="onImageChange"
                    id="deposit_slip"
                    name="deposit_slip"><br>

                    <v-btn 
                    v-on:click="paylater_form = true"
                    color="orange darken-1">
                        <i class="ion ion-android-hand" style="font-size: 1.2rem"></i>&nbsp;
                        Pay Later
                    </v-btn>
                    
                    <v-btn 
                    v-on:click="saveForm(1)"
                    color="primary" 
                    :disabled="invalidUpload">
                        <i class="fa fa-send"></i>&nbsp;
                        Send
                    </v-btn>
                </v-form>
            </v-flex>
        </v-layout>
    </v-card-text>
</v-card>

@include('components.guest.reservation_steps.paylater_dialog')