@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rooms and Utilities
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h1 class="box-title">Room Rates</h1>
                <button 
                class="btn btn-primary pull-right" v-on:click="showForm">
                    <i class="fa fa-plus"></i>
                    Add New Rates
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Hours</th>
                            <th class="text-center">Cost</th>
                            <th class="text-center">Room Types</th>
                            <th class="text-center" width="100">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr 
                        v-for="(item, index) in items"
                        v-bind:key="index">
                        <td class="text-center">@{{ item.hours }}</td>
                        <td class="text-center text-red">@{{ item.cost | phCurrency }}</td>
                        <td class="text-center text-primary"><strong>@{{ item.room_name }}</strong></td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-sm" v-on:click="editItem(item.room_rate_id, index)">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a class="btn btn-danger btn-sm" v-on:click="deleteItem(item.room_rate_id, index)">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-default">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@{{ formTitle }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="room_type_id">Room Types</label>
                        <select v-model="form.room_type_id" class="form-control" id="room_type_id" required>
                            <option v-for="room_type in room_types" 
                            :value="room_type.room_type_id">@{{ room_type.room_name }}</option>    
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hours">Hours</label>
                                <input type="number" class="form-control" v-model="form.hours">

                                <span v-if="errors.hours" class="text-danger">
                                    <strong>
                                        @{{ errors.hours[0] }}
                                    </strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cost">Cost</label>
                                <input type="number" class="form-control" v-model="form.cost">

                                <span v-if="errors.cost" class="text-danger">
                                    <strong>
                                        @{{ errors.cost[0] }}
                                    </strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button v-on:click="saveForm" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        document.querySelector('#_room_rates').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    dialog: false,
                    items: [],
                    isEdit: false,
                    room_types: [],
                    room_type_id: 0,

                    form: {
                        room_type_id: 0,
                        cost: '',
                        hours: ''
                    },
                    defaultItem : {
                        room_type_id: 0,
                        cost: '',
                        hours: ''
                    },

                    errors: {}
                }
            },
            computed: {
                formTitle () {
                    return this.isEdit == false ? 'New Item' : 'Edit Item';
                }
            },
            watch: {
                isEdit() {
                    if (this.isEdit == false) {
                        this.form = Object.assign({}, this.defaultItem);
                    }
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                getItems: function() {
                    axios.get(`${this.base_url}/admin/room_rates/get`)
                    .then(({data}) => {
                        this.items = data;

                        setTimeout(() => {
                            $('#table').DataTable();
                        });
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                editItem: function(room_rate_id, index) {
                    this.isEdit = true;
                    this.fetchRoomTypes();

                    axios.get(`${this.base_url}/admin/room_rates/get/${room_rate_id}`)
                    .then(({data}) => {
                        this.form = Object.assign({}, data);
                        $('#modal-default').modal('show');
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                deleteItem: function(room_rate_id, index) {
                    var msg = confirm('Are you sure you want to delete this?');
                    if (msg) {
                        axios.delete(`${this.base_url}/admin/room_rates/delete/${room_rate_id}`)
                        .then(({data}) => {
                            this.items.splice(index, 1);
                            toastr.success('Successfully deleted!');
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                    }
                },

                showForm() {
                    $('#modal-default').modal('show');
                    this.fetchRoomTypes();
                    this.isEdit = false;
                },

                close() {
                    this.dialog = false;
                    $('#modal-default').modal('hide');
                    setTimeout(() => {
                        this.errors = {};
                        this.form = Object.assign({}, this.defaultItem);
                        this.isEdit = false;
                    }, 300)
                },

                saveForm: function() {
                    if (this.isEdit == true) {
                        axios.put(`${this.base_url}/admin/room_rates/update/${this.form.room_rate_id}`, this.form)
                        .then(({data}) => {
                            this.getItems();
                            this.close();
                            toastr.success('Successfully saved!');
                        })
                        .catch((err) => {
                            console.log(err.response);
                            this.errors = err.response.data.errors;
                        });
                    } else {
                        axios.post(`${this.base_url}/admin/room_rates/store`, this.form)
                        .then(({data}) => {
                            this.getItems();
                            this.close();
                            toastr.success('Successfully saved!');
                        })
                        .catch((err) => {
                            console.log(err.response);
                            this.errors = err.response.data.errors;
                            toastr.error(err.response);
                        });
                    }
                },

                fetchRoomTypes() {
                    axios.get(`${this.base_url}/admin/room_types/getAll`)
                    .then(({data}) => {
                        this.room_types = data;
                    })
                    .catch((err) => {
                        toastr.error(err.response);
                    });
                }
            }
        });
    </script>
@endpush
