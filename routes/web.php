<?php

Route::get('/', function () {
	return view('components.guest.home');
})->name('home');

Auth::routes();

Route::group(['middleware' => ['admin', 'auth']], function() {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::post('/admin/rooms/post', 'RoomController@add_rooms');

	// Comments
	Route::get('admin/comments/get', 'CommentController@index');
	Route::get('admin/comments/get/{comment_id}', 'CommentController@show');
	Route::post('admin/comments/store', 'CommentController@store');
	Route::put('admin/comments/update/{comment_id}', 'CommentController@update');
	Route::delete('admin/comments/delete/{comment_id}', 'CommentController@destroy');
	Route::put('admin/comments/approve/{comment_id}', 'CommentController@approve');

	// Reports
	Route::get('/admin/customers/get', 'ReportController@customers');
	Route::get('/admin/all_customers/get', 'ReportController@all_customers');

	// Receptionist
	Route::get('/admin/receptionist/get', 'ReceptionistController@index');
	Route::get('/admin/receptionist/get/{user_id}', 'ReceptionistController@show');
	Route::post('/admin/receptionist/store', 'ReceptionistController@store');
	Route::put('/admin/receptionist/update/{user_id}', 'ReceptionistController@update');
	Route::delete('/admin/receptionist/destroy/{user_id}', 'ReceptionistController@destroy');

	// Bank Account
	Route::get('/admin/bank_accounts/get', 'BankAccountController@index');
	Route::get('/admin/bank_accounts/get/{bank_account_id}', 'BankAccountController@show');
	Route::post('/admin/bank_accounts/store', 'BankAccountController@store');
	Route::put('/admin/bank_accounts/update/{bank_account_id}', 'BankAccountController@update');
	Route::delete('/admin/bank_accounts/destroy/{bank_account_id}', 'BankAccountController@destroy');

	// Function Room
	Route::get('/admin/function_rooms/get', 'FunctionRoomController@index');
	Route::get('/admin/function_rooms/get/{function_room_id}', 'FunctionRoomController@show');
	Route::post('/admin/function_rooms/store', 'FunctionRoomController@store');
	Route::post('/admin/function_rooms/store_image', 'FunctionRoomController@store_image');
	Route::put('/admin/function_rooms/update/{function_room_id}', 'FunctionRoomController@update');
	Route::delete('/admin/function_rooms/destroy/{function_room_id}', 'FunctionRoomController@destroy');
	Route::delete('/admin/function_images/delete_image/{function_image_id}', 'FunctionRoomController@delete_image');

	// Extend Time
	Route::put('/admin/extend_time/{schedule_id}', 'ActiveBookingController@extend_time');

	// Official Receipt
	Route::get('/admin/official_receipt/get/{room_schedule_id}', 'BillingInformationController@official_receipt');
	Route::get('/admin/generate_official_receipt/{room_schedule_id}', 'BillingInformationController@generate_official_receipt');

	// Page Content
	Route::get('/admin/page_content/get', 'PageContentController@index');
	Route::get('/admin/page_content/get/{page_content_id}', 'PageContentController@show');
	Route::post('/admin/page_content/store', 'PageContentController@store');
	Route::put('/admin/page_content/update/{page_content_id}', 'PageContentController@update');
	Route::delete('/admin/page_content/destroy/{page_content_id}', 'PageContentController@destroy');

	// Contact Us
	Route::get('/admin/contact_us/get', 'ContactUsController@index');
	Route::get('/admin/contact_us/get/{contact_us_id}', 'ContactUsController@show');
	Route::post('/admin/contact_us/store', 'ContactUsController@store');
	Route::put('/admin/contact_us/update/{contact_us_id}', 'ContactUsController@update');
	Route::delete('/admin/contact_us/destroy/{contact_us_id}', 'ContactUsController@destroy');

	// Frequently Asked Question
	Route::get('/admin/frequently_asked_questions/get', 'FrequentlyAskedQuestionController@index');
	Route::get('/admin/frequently_asked_questions/get/{frequently_asked_question_id}', 'FrequentlyAskedQuestionController@show');
	Route::post('/admin/frequently_asked_questions/store', 'FrequentlyAskedQuestionController@store');
	Route::put('/admin/frequently_asked_questions/update/{frequently_asked_question_id}', 'FrequentlyAskedQuestionController@update');
	Route::delete('/admin/frequently_asked_questions/delete/{frequently_asked_question_id}', 'FrequentlyAskedQuestionController@delete');

	// Dashboard
	Route::get('/admin/dashboard/get', 'DashboardController@dashboard');

	// Billing Information
	Route::get('/admin/billing_information/get/{room_schedule_id}', 'BillingInformationController@show');
	Route::get('/admin/billing_information/get', 'BillingInformationController@billing_information');
	Route::put('/admin/update_payment_status/update/{room_schedule_id}', 'BillingInformationController@update_payment_status');

	// Maintenance
	Route::put('admin/toggle_maintenance/{room_id}/{status}', 'RoomController@toggle_maintenance');
	
	// Addons
	Route::get('/admin/addons/get', 'AddOnTypeController@addons');
	Route::get('/admin/addons/get/{add_on_type_id}', 'AddOnTypeController@show');
	Route::post('/admin/addons/post', 'AddOnTypeController@store');
	Route::put('/admin/addons/update/{add_on_type_id}', 'AddOnTypeController@update');
	Route::delete('/admin/addons/delete/{add_on_type_id}', 'AddOnTypeController@destroy');

	// Active Bookings
	Route::get('/admin/booking_addons/get/{room_schedule_id}', 'ActiveBookingController@booking_addons');
	Route::get('/admin/active_bookings/get', 'ActiveBookingController@active_bookings')->name('active_bookings');
	Route::get('/admin/active_bookings/get/{reservation_id}', 'ActiveBookingController@active_booking');
	Route::put('/admin/acknowledge/update/{reservation_id}', 'ActiveBookingController@acknowledge');
	Route::delete('/admin/cancel/{reservation_id}', 'BookingController@cancel');
	Route::put('/admin/checkin_guest/{reservation_id}', 'ActiveBookingController@checkin_guest');
	Route::put('/admin/approve_reservation/{reservation_id}', 'ActiveBookingController@approve_reservation');

	// Room Types
	Route::get('/admin/room_types/getAll', 'RoomTypeController@getAll')->name('room_types');
	Route::get('/admin/room_types/get/{room_type_id}', 'RoomTypeController@getRoomType');
	Route::post('/admin/room_types/post', 'RoomTypeController@store');
	Route::put('/admin/room_types/update/{room_type_id}', 'RoomTypeController@update');
	Route::delete('/admin/room_types/delete/{room_type_id}', 'RoomTypeController@destroy');

	// Rooms
	Route::get('/admin/rooms/get', 'RoomController@rooms');
	Route::get('/admin/rooms/get/{room_id}', 'RoomController@room');
	Route::post('/admin/rooms/store', 'RoomController@store');
	Route::put('/admin/rooms/update', 'RoomController@update');
	Route::delete('/admin/rooms/delete/{room_id}', 'RoomController@delete');

	Route::get('/admin/rooms/room_type_id/{room_type_id}', 'RoomController@show_rooms');
	Route::get('/admin/room_rates/room_type_id/{room_type_id}', 'RoomController@show_room_rates');

	// Room Rate
	Route::get('/admin/room_rates/get', 'RoomRateController@index');
	Route::get('/admin/room_rates/get/{room_rate_id}', 'RoomRateController@show');
	Route::post('/admin/room_rates/store', 'RoomRateController@store');
	Route::put('/admin/room_rates/update/{room_rate_id}', 'RoomRateController@update');
	Route::delete('/admin/room_rates/delete/{room_rate_id}', 'RoomRateController@delete');

	// Uploads
	Route::get('admin/images/{room_type_id}', 'RoomTypeController@images');
	Route::post('admin/upload', 'RoomTypeController@upload');
	Route::delete('admin/images/delete/{room_image_id}', 'RoomTypeController@delete_image');
	
	// Route Views
	Route::view('admin/receptionist', 'components.admin.receptionist');
	Route::view('admin/bank_accounts', 'components.admin.bank_accounts');
	Route::view('admin/function_rooms', 'components.admin.function_rooms');
	Route::view('admin/room_types_v2', 'components.admin.room_types_v2')->name('room_types_v2');

	Route::view('admin/guest_billing_information/room_schedule_id/{room_schedule_id}', 'components.admin.guest_billing_information')->name('guest_billing_information', ['room_schedule_id']);
	Route::view('admin/billing_information', 'components.admin.billing_information')->name('billing_information');
	Route::view('admin/room_rates', 'components.admin.room_rates')->name('room_rates');
	Route::view('admin/rooms', 'components.admin.rooms')->name('rooms');
	Route::view('admin/addons', 'components.admin.addons')->name('addons');
	Route::view('admin/dashboard', 'components.admin.dashboard')->name('dashboard');
	Route::view('admin', 'components.admin.dashboard')->name('dashboard');
	Route::view('admin/bookings', 'components.admin.bookings')->name('active_bookings');

	Route::view('admin/contact_us', 'components.admin.contact_us.contact_us');
	Route::view('admin/page_content', 'components.admin.page_content.page_content');
	Route::view('admin/comments', 'components.admin.comments');
});

// Guest
Route::group(['middleware' => ['hotel_guest', 'auth']], function() {
	// Added
	Route::get('guest/comments/get', 'CommentController@comments');
	Route::post('guest/comments_suggestions/post', 'CommentSuggestionController@send');

	// Bank Account
	Route::get('guest/bank_accounts/get', 'BankAccountController@index');

	// Summary
	Route::post('guest/booking_summary', 'ReservationController@booking_summary');

	Route::put('/guest/booking_status/upload_deposit_slip/{reservation_id}', 'BookingStatusController@upload_deposit_slip');
	Route::get('/guest/booking_status', 'BookingStatusController@booking_status');
	Route::post('/guest/create_reservation', 'ReservationController@reservation');
	//

	Route::post('/guest/pay', 'StripeController@pay')->name('pay');
	Route::get('/guest/room_rates/room_rate_id/{room_rate_id}', 'RoomRateController@show');
	Route::get('/guest/booked_rooms/{room_type_id}', 'GuestBookedRoomController@booked_rooms');
	Route::get('/guest/addons/get', 'AddOnTypeController@index');
	
	
	Route::get('/guest/room_rates/room_type_id/{room_type_id}', 'RoomController@show_room_rates');
	Route::post('/guest/reservation', 'BookingController@reservation');

	Route::get('/guest/room_capacity/{room_type_id}', 'BookingController@room_capacity');
	Route::get('/guest/room_types/get/{room_type_id}/{transaction_type_id}', 'RoomTypeController@show');
	Route::post('/guest/check_date_availability', 'ReservationController@check_room_availablity');
	Route::post('guest/comments/store', 'CommentController@store');

	Route::get('guest/room_capacity/get/{room_type_id}', 'RoomTypeController@get_room_capacity');

	Route::view('/guest/comments', 'components.guest.comments')->name('comments');
	Route::view('/room_details', 'components.guest.room_details');
	Route::view('/reservation', 'components.guest.reservation');
	Route::view('guest/payment_form', 'components.guest.payment_form');
	Route::view('/guest/reservation_form', 'components.guest.reservation_form');
});

// Public
// Route::group(['middleware' => ['verify']], function() {
	Route::get('/guest/function_rooms/get', 'FunctionRoomController@index');
	Route::get('/guest/frequently_asked_questions/get', 'PageContentController@frequently_asked_questions');
	Route::get('/guest/page_content/get', 'PageContentController@index');
	Route::get('/guest/contact_us/get', 'ContactUsController@index');
	Route::get('/guest/room_types/get', 'RoomTypeController@getAll');
	Route::get('/guest/images/{room_type_id}', 'RoomTypeController@images');
	
	Route::view('/about_us', 'components.guest.about_us');
	Route::view('/frequently_asked_questions', 'components.guest.frequently_asked');
	Route::view('/guest/booking_form', 'components.guest.booking_form');
// });

Route::view('/check_your_verification_first', 'components.warning')->name('verify_first');
Route::get('verify_account/{verfication_code}', 'ActivateUserController@activate_user');