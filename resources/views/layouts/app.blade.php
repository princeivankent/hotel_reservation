<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	
	<!-- Styles -->
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/skin-blue.min.css') }}">
	<link rel="stylesheet" href="{{ asset('libraries/admin/css/toastr.css') }}">

	<!-- Google Font -->
	{{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> --}}
	
	<style>
		[v-cloak] > * {display: none;}
		html, body {
			/* font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; */
			/* font-family:-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif; */
			font-family:Arial, Helvetica, sans-serif;
		}
	</style>
	
	@stack('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<header class="main-header">
			@include('templates.header')
		</header>

		<aside class="main-sidebar">
			@include('templates.sidebar')
		</aside>

		<div class="content-wrapper">
			<div id="app" v-cloak>
				@yield('content')

				<!-- Toast Notification -->
				{{-- <v-snackbar
				v-model="toast_notif.isTriggered"
				:color="toast_notif.color"
				:timeout="toast_notif.timeout"
				:multi-line="toast_notif.multi_line"
				right
				bottom
				>
					<v-icon class="mr-1" dark>@{{ toast_notif.icon }}</v-icon>
					@{{ toast_notif.text }}
					<v-btn
						dark
						flat
						v-on:click="toast_notif.isTriggered = false"
					>
						Close
					</v-btn>
				</v-snackbar> --}}
			</div>
		</div>
		
		@include('templates.footer')
			
		<aside class="control-sidebar control-sidebar-dark">
			@include('templates.control-sidebar')				
		</aside>

		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>

	<!-- Vuejs/jQuery/Axios -->
	<script src="{{ asset('js/app.js') }}"></script>

	<!-- jQuery -->
	<script src="{{ asset('libraries/admin/js/jquery.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/fastclick.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/adminlte.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/moment.min.js') }}"></script>
	<script src="{{ asset('libraries/admin/js/toastr.min.js') }}"></script>
	<script src="{{ asset('js/toastr.config.js') }}"></script>

	<!-- Global Functions -->
	<script>
		var mixin = {
			data () {
				return {
					base_url: "{{ url('/') }}",
				}
			},
			filters: {
				phCurrency(value) {
					if (!value) return ''
					value = parseInt(value);
					return '₱ ' + value.toLocaleString('en');
				},
				timeFormat(time) {
					return moment(time, "HH:mm A").format('hh:mm A');
				},
				dateFormat(value) {
					if (!value) return ''
					value = value.toString()
					return moment(value).format('MMMM D, YYYY')
				},
				dateTimeFormat(value) {
					if (!value) return ''
					value = value.toString()
					return moment(value).format('MMMM D, YYYY | h:mm:ss a')
				}
			}
		}
	</script>
	
	@stack('scripts')
	@stack('plugin_scripts')
</body>
</html>
