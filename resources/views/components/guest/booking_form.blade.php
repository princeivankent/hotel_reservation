@extends('layouts.guest_layout')

@push('styles')
	<link rel="stylesheet" href="{{ url('libraries/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-colorpicker.min.css') }}">
	<link rel="stylesheet" href="{{ url('libraries/bootstrap-timepicker.min.css') }}">

	<style>
		.StripeElement {
			background-color: white;
			height: 40px;
			padding: 10px 12px;
			border-radius: 4px;
			border: 1px solid transparent;
			box-shadow: 0 1px 3px 0 #e6ebf1;
			-webkit-transition: box-shadow 150ms ease;
			transition: box-shadow 150ms ease;
		}
		
		.StripeElement--focus {
			box-shadow: 0 1px 3px 0 #cfd7df;
		}
		
		.StripeElement--invalid {
			border-color: #fa755a;
		}
		
		.StripeElement--webkit-autofill {
			background-color: #fefde5 !important;
		}
	</style>
@endpush

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1 style="color: white">Room Details</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ url('http://localhost/hotel_reservation') }}" class="text-gray">
				<i class="fa fa-home"></i>&nbsp;
				Home Page
			</a>
		</li>
		<li class="active">
			Booking Form
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div v-if="hasError" class="callout callout-danger">
		<h4>Warning!</h4>

		<ul v-for="error in errors">
			<li>@{{ error[0] }}</li>
		</ul>
	</div>
		
		<!-- Container -->
		<div class="row">
			<div class="col-md-8">
				<div class="box box-default shadow-lg">
					<div class="box-header with-border">
						<h1 class="box-title">
							<i class="fa fa-camera"></i>
							Room Photos
						</h1>
					</div>
					<div class="box-body">
						<div v-if="hasImage === false">
							<div class="callout callout-danger">
								<h4 class="text-center">
									<i class="fa fa-thumbs-o-down"></i>
									There's no image to show
								</h4>
							</div>
						</div>
						<div v-else id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" 
								v-for="(image, index) in room_types.room_images" 
								:data-slide-to="index" 
								:class="`${index == 0 ? 'active' : ''}`"></li>
							</ol>
							<div class="carousel-inner">
								<div v-for="(image, index) in room_types.room_images" :class="`${index == 0 ? 'item active' : 'item'}`">
									<img :src="`{{ url('uploaded-images')}}/${image.filename}`" :alt="image.room_image_id"
									style="width: 100%; height: 480px; margin-bottom: 10px;">
								</div>
							</div>
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								<span class="fa fa-angle-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								<span class="fa fa-angle-right"></span>
							</a>
						</div>
					</div>
				</div>
				<div v-if="pickedAddons.length > 0" class="box box-default">
					<div class="box-header with-border">
						<h1 class="box-title">
							<i class="fa fa-plus"></i>
							Addons
						</h1>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center" width="40">#</th>
									<th class="text-center">DESCRIPTION</th>
									<th class="text-center">QUANTITY</th>
									<th class="text-center">COST</th>
									<th class="text-center">EXCLUDE</th>
								</tr>
							</thead>
							<tbody>
								<tr 
								v-for="(item, index) in pickedAddons"
								v-bind:key="index">
									<td class="text-center">@{{ index+1 }}</td>
									<td class="text-center text-primary">@{{ item.description }}</td>
									<td class="text-center text-primary">@{{ item.quantity }}</td>
									<td class="text-center text-danger">@{{ item.cost | phCurrency }}</td>
									<!-- Buttons -->
									<td class="text-center" width="50">
										<button class="btn btn-danger btn-sm" v-on:click="removeAddon(item.cost, index)">
											<i class="fa fa-times"></i>
										</button>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th class="text-success text-right">Total Addons:</th>
									<th class="text-danger text-center">@{{ computed_cost | phCurrency }}</th>
									<th>&nbsp;</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>   
			<div class="col-md-4">
				<div class="box box-default shadow-lg" style="height: 555px;">
					<div class="box-header with-border">
						<h1 class="box-title">
							<i class="ion ion-android-list"></i>
							Room Details
						</h1>
					</div>
					<div class="box-body">
						<ul class="list-group">
							<li class="list-group-item">
								<label for=""><i class="fa fa-check-circle"></i>&nbsp; Room Type</label>
								<p class="text-primary text-center text-bold">@{{ room_type.room_name }}</p>
							</li>
							<li class="list-group-item">
								<label for=""><i class="fa fa-check-circle"></i>&nbsp; Amenities</label>
								<p class="text-primary text-center text-bold">@{{ room_type.room_description }}</p>
							</li>
							<li class="list-group-item">
								<label for="" style="margin-right: 5px;"><i class="fa fa-check-circle"></i>&nbsp; Capacity</label><br/>
								<p class="text-primary text-center text-bold">@{{ room_type.room_capacity }} person(s)</p>
							</li>
							<li class="list-group-item">
								<label for="" style="margin-right: 5px;"><i class="fa fa-check-circle"></i>&nbsp; Available Rooms</label><br/>
								<p class="text-primary text-center text-bold">@{{ available_rooms }} room(s)</p>
							</li>
							<li class="list-group-item">
								<label for="" style="margin-right: 5px;"><i class="fa fa-check-circle"></i>&nbsp; Cost</label><br/>
								<p v-if="cost == 'Select room rate below'" class="text-primary text-center text-bold">@{{ cost }}</p>
								<p v-else class="text-danger text-center text-bold">@{{ cost | phCurrency }}</p>
							</li>
						</ul>

						<a href="{{ url('/guest/reservation_form?room_type_id=').$_GET['room_type_id'] }}" class="btn btn-flat btn-block btn-warning pull-right" style="margin-top: 52px;">
							<i class="fa fa-check-square-o"></i>&nbsp;
							RESERVE NOW
						</a>
					</div>
				</div>
			<div class="box box-primary shadow-lg">
				<div class="box-header with-border">
					<h1 class="box-title">
						<i class="fa fa-list-alt"></i>
						Booking Fill-up Form
					</h1>
					<button v-on:click="showAddons"
					class="btn btn-sm btn-warning pull-right">
						<i class="fa fa-plus"></i>
						Addons
					</button>
				</div>
				<div class="box-body">
					<!-- Booking Form -->
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div v-if="isLongTime">
									<label for="checkbox" class="text-primary" style="cursor: pointer;">
										<i class="fa fa-toggle-on"></i>
										Change booking type to Hourly Rate
									</label>
									<input type="checkbox" v-model="isLongTime" id="checkbox" hidden> 
									<h3 v-if="isLongTime" class="text-danger">Daily Rate</h3>
								</div>
								<div v-else>
									<label for="checkbox" class="text-primary" style="cursor: pointer;">
										<i class="fa fa-toggle-off"></i>
										Change booking type to Daily Rate
									</label>
									<input type="checkbox" v-model="isLongTime" id="checkbox" hidden>
									<h3 class="text-danger">Hourly Rate</h3>
								</div>
							</div>
							<!-- 24 Hours Rate -->
							<div v-if="isLongTime">
								<div class="row">
									<div class="form-group col-md-12">
										<label>Check In Date:</label>
										<button class="btn btn-sm btn-info" style="margin-bottom: 5px;"
										v-on:click="openBookedRooms">
											<i class="fa fa-sign-in"></i>
											SEE AVAILABLE ROOMS
										</button>
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="date" 
											class="form-control" 
											v-model="form.checkin"
											style="padding-top: 0px;" autofocus required>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label>Check Out Date</label>
					
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="date" 
											class="form-control" 
											v-model="form.checkout"
											style="padding-top: 0px;" autofocus required>
										</div>
									</div>
								</div>
								
								<!-- Time Picker -->
								<div class="form-group row">
									<div class="form-group col-md-6">
										<label>Arrival Time:</label>
										<div class="input-group">
											<input 
											id="timein"
											name="timein"
											type="text" 
											class="form-control timepicker">

											<div class="input-group-addon">
												<i class="fa fa-clock-o"></i>
											</div>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label>No. of Person(s)</label>
										<div class="input-group">
											<input 
											v-model="form.persons"
											type="number" 
											class="form-control">

											<div class="input-group-addon">
												<i class="fa fa-users"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Hourly Rate -->
							<div v-else>
								<div class="form-group">
									<label>Check In Date:</label>
									<button class="btn btn-sm btn-info" style="margin-bottom: 5px;"
									v-on:click="openBookedRooms">
										<i class="fa fa-sign-in"></i>
										SEE AVAILABLE ROOMS
									</button>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" 
										class="form-control" 
										v-model="form.checkin"
										style="padding-top: 0px;" autofocus required>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label>Arrival Time:</label>
										<div class="input-group">
											<input 
											id="timein_hr"
											name="timein_hr"
											type="text" 
											class="form-control timepicker">

											<div class="input-group-addon">
												<i class="fa fa-clock-o"></i>
											</div>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label>No. of Person(s)</label>
										<div class="input-group">
											<input 
											v-model="form.persons"
											type="number" 
											class="form-control">

											<div class="input-group-addon">
												<i class="fa fa-users"></i>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Choose Room Rates</label>
									<select v-model="form.room_rate_id" class="form-control" id="select2" style="width: 100%;">
										<option v-for="(room, index) in room_rates" :value="room.room_rate_id">
											@{{ room.hours }}hrs 
											<strong class="text-danger">for</strong>
											@{{ room.cost | phCurrency }}
										</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button
						v-on:click="book"
						class="btn btn-sm btn-success">
						<i class="fa fa-check"></i>
						BOOK NOW</button>
					</div>
				</div>
			</div>
			</div>     
		</div>

		@include('components.guest.modals.addons')
		@include('components.guest.modals.booked_rooms')
		@include('components.guest.modals.booking_details')
		{{-- @include('components.guest.modals.payment_form') --}}
	</section>
@endsection

@push('scripts')
	<script src="{{ url('libraries/bootstrap-timepicker.min.js') }}"></script>
	<script>
		var room_type_id = "{{ $_GET['room_type_id'] ?? '' }}";

		var app = new Vue({
			el: '#app', 
			mixins: [mixin],
			data() {
				return {
					hasImage: false,
					isLongTime: true,
					form: {},
					room_types: [],
					room_rates: [],
					room_type: {},
					available_rooms: 0,
					addons: [],
					pickedAddons: [],
					booked_rooms: [],
					fields: {},
					days: 0,
					
					hasError: false,
					errors: {},
					
					cost: '', 		   // Room default price
					computed_cost: '', // total addons cost
					room_cost: 0,	   // total room cost
					grand_total: 0,	   // computed_cost + room_cost
					already_read: false,
					stripeToken: '',
					transact_parameters: {}
				}
			},
			watch: {
				computed_cost() {
					this.room_cost = parseInt(this.room_cost+this.computed_cost);
				},
				isLongTime: function() {
					if (!this.isLongTime) { // short time = 1
						this.showRoomRate(room_type_id);
						this.form.reservation_type_id = 1;
						
						// Initiate Time to display
						setTimeout(() => {
							$('.timepicker').timepicker({showInputs: false});
						});
					}
					else {
						this.form.reservation_type_id = 2;

						// Initiate Time to display
						setTimeout(() => {
							$('.timepicker').timepicker({showInputs: false});
						});
					}
					this.getRoomType(room_type_id, this.form.reservation_type_id);
				}
			},
			created() {
				this.getImages(room_type_id);
				this.getRoomType(room_type_id, this.form.reservation_type_id);
			},
			mounted() {
				
			},
			methods: {
				getRoomType: function(room_type_id, reservation_type_id) {
					axios.get(`${this.base_url}/guest/room_types/get/${room_type_id}/${reservation_type_id}`)
					.then(({data}) => {
						this.room_type = data.result;
						this.available_rooms = data.available_rooms;
						this.cost = data.cost;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
				},
				getImages: function(room_type_id) {
					axios.get(`${this.base_url}/guest/images/${room_type_id}`)
					.then(({data}) => {
						if (data.room_images.length > 0) {
							this.hasImage = true;
							this.room_types = data;
						}
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:' + err.response.data.exception, false, true);
					});
				},
				grandTotal(params) {
					var fields = {};
					if (this.isLongTime) {
						var days = params.days;
						var computation = (days*params.cost) + params.addon_cost;
						fields = {
							total_cost: computation,
							total_paid: parseInt(computation/2)
						}
						
						this.fields = fields;
					} else {
						axios.get(`${this.base_url}/guest/room_rates/room_rate_id/${this.form.room_rate_id}`)
						.then(({data}) => {
							this.cost = data.cost;
							var days = 1;
							var computation = parseInt((days*data.cost) + this.computed_cost);
							fields = {
							    total_cost: computation,
							    total_paid: parseInt(computation/2)
							}
							
							this.fields = fields;
						})
						.catch((error) => {
							console.log(error.response);
						});
					}
				},
				book: function() {
					var addons = this.pickedAddons.length > 0 ? this.pickedAddons : null;
					var params = {};
					if (this.isLongTime) {
						params = {
							transaction_type_id: 1,
							reservation_type_id: 2, // 24 Hours Basis
							room_type_id: room_type_id,
							checkin: this.form.checkin,
							checkout: this.form.checkout,
							timein: document.getElementById('timein').value,
							timeout: document.getElementById('timein').value,
							persons: this.form.persons,
							addons: addons
						}
					}
					else {
						params = {
							transaction_type_id: 1,
							reservation_type_id: 1, // Hourly Rate Basis
							room_type_id: room_type_id,
							room_rate_id: this.form.room_rate_id,
							checkin: this.form.checkin,
							checkout: this.form.checkin,
							timein: document.getElementById('timein_hr').value,
							persons: this.form.persons,
							addons: addons
						}
					}

					if (this.form.checkin != null && this.form.checkout != null) {
						var date1 = new Date(this.form.checkin);
						var date2 = new Date(this.form.checkout);
						var timeDiff = Math.abs(date2.getTime() - date1.getTime());
						var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
						this.days = diffDays;
					}
					else {
						this.days = 1;
					}
					
					var params2 = {
						cost: this.cost,
						addon_cost: this.computed_cost,
						days: this.days,
					};

					this.grandTotal(params2);
					this.room_cost = parseInt(this.cost*this.days);
					this.room_cost = parseInt(this.room_cost+this.computed_cost);
					this.grand_total = this.room_cost;
					
					this.transact_parameters = params;
					
					setTimeout(() => {
						$('#booking_details').modal('show');
					}, 500);
				},
				showRoomRate: function(room_type_id) {
					axios.get(`${this.base_url}/guest/room_rates/room_type_id/${room_type_id}`)
					.then(({data}) => {
						this.room_rates = data;
					})
					.catch((err) => {
						console.log(err.response);
					});
				},
				showAddons: function() {
					this.getAddons();
					$('#addons-form').modal('show');
				},
				getAddons: function() {
					axios.get(`${this.base_url}/guest/addons/get`)
					.then(({data}) => {
						this.addons = data;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:', err.response.data.exception, false);
					});
				},
				chooseAddons: function(add_on_type_id, description, cost) {
					var hasDuplicate;
					this.pickedAddons.forEach(element => {
						if (element.add_on_type_id == add_on_type_id) {
							hasDuplicate = true;
						}
					});

					if (!hasDuplicate) {
						// Add item (1)
						this.pickedAddons.push({
							add_on_type_id: add_on_type_id,
							description: description,
							cost: cost,
							quantity: 1
						});
					}
					else {
						var i = 1;
						this.pickedAddons.forEach(element => {
							if (element.add_on_type_id == add_on_type_id) {
								element.cost += cost;
								element.quantity += parseInt(i);
							}
						});
					}

					this.computed_cost = parseInt(this.computed_cost + cost);
				},
				deductAddon: function(add_on_type_id, description, cost) {
					this.pickedAddons.forEach((element, index) => {
						if (element.add_on_type_id === add_on_type_id) {
							element.cost = element.cost - cost;
							element.quantity -= 1;
							this.computed_cost = this.computed_cost - cost;
							this.room_cost -= cost;

							if (element.cost === 0) {
								this.pickedAddons.splice(index, 1);
							}
						}
					});
				},
				removeAddon: function(cost, index) {
					this.computed_cost -= cost;
					this.pickedAddons.splice(index, 1);
				},
				getBookedRooms: function(room_type_id) {
					axios.get(`${this.base_url}/guest/booked_rooms/${room_type_id}`)
					.then(({data}) => {
						this.booked_rooms = data;
					})
					.catch((err) => {
						console.log(err.response);
						this.notify('Something went wrong:', err.response.data.exception, false);
					});
				},
				openBookedRooms: function() {
					this.getBookedRooms(room_type_id);
					$('#booked_rooms').modal('show');
				},
				proceedToStripe() {
					$('#booking_details').modal('hide');
					$('#payment_form').modal('show');
				},
				finishTransaction() {
					return axios.post(`${this.base_url}/guest/reservation`, this.transact_parameters)
					.then(({data}) => {
						this.hasError = false;

						return true;
					})
					.catch((err) => {
						console.log(err.response);
						this.hasError = true;
						this.errors = err.response.data.errors;
					});
				}
			}
		});

		//Timepicker
		$('.timepicker').timepicker({showInputs: false});
	</script>
@endpush
