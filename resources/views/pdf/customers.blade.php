<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1->0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Customers Report</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
        .page-break {
            page-break-after: always;
        }
        </style>
    </head>
    <body>
        {{-- <div class="page-break"></div> --}}
        <section class="invoice">
            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                <div class="col-md-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> Paradise Hotel
                        <small class="pull-right">{{ Carbon\Carbon::parse(now())->format('M d, Y') }}</small>
                    </h2>
                </div>
        
                <div class="row" style="margin-top: 30px;">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Guest</th>
                                    <th>Room Code</th>
                                    <th>Check IN</th>
                                    <th>Check Out</th>
                                    <th>Persons</th>
                                    <th>Total Cost</th>
                                    <th>DownPayment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($current_customers as $item)
                                    <tr>
                                        <td>{{ $item['user']['name'] }}</td>
                                        <td>{{ $item['room']['room_code'] }}</td>
                                        <td>{{ $item['room_schedule']['checkin'] }} {{ $item['room_schedule']['timein'] }}</td>
                                        <td>{{ $item['room_schedule']['checkout'] }} {{ $item['room_schedule']['timeout'] }}</td>
                                        <td>{{ $item['room_schedule']['persons']}}</td>
                                        <td>P {{ $item['room_schedule']['total_cost']}}</td>
                                        <td>P {{ $item['payment']['pay']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>