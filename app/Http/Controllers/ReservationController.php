<?php

namespace App\Http\Controllers;

use App\Room;
use App\Reservation;
use App\Payment;
use App\RoomType;
use App\RoomSchedule;

use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    protected $reservation = 1; // By default for reservation
    protected $isPayNow;

    public function reservation(Request $request)
    {
        try {
            DB::beginTransaction();
            // Check deposit_slip existence
            if ($request->get('deposit_slip')) $this->isPayNow = 1;

            // Build parameters
            $data = [
                'room_type_id' => $request->room_type_id,
                'checkin' => $request->checkin,
                'checkout' => $request->checkout,
                'timein' => '14:00',
                'timeout' => '12:00',
                'persons' => $request->persons,
                'days' => $this->dateDiff($request->checkin, $request->checkout),
                'total_cost' => $this->totalCost($request->room_type_id, $this->dateDiff($request->checkin, $request->checkout)),
                'transaction_type_id' => $this->reservation == 1 ? 1 : 2,
                'payment_status' => $this->isPayNow ? 'half_paid' : 'no_pay',

                'deposit_slip' => $this->file($request->deposit_slip),
                'pay' => $this->pay($this->totalCost($request->room_type_id, $this->dateDiff($request->checkin, $request->checkout))),
                'expiry' => $this->isPayNow ? NULL : Carbon::now()->toDateString()
            ];

            $room_schedule = RoomSchedule::create($data);

            if ($room_schedule) {
                $payment = Payment::create($data);

                if ($payment) {
                    $reservation = new Reservation;
                    $reservation->room_schedule_id = $room_schedule->room_schedule_id;
                    $reservation->room_id = $this->availableRooms($data['room_type_id']) ? $this->availableRooms($data['room_type_id']) : NULL;
                    $reservation->user_id = Auth::user()->user_id;
                    $reservation->payment_id = $payment->payment_id;
                    $reservation->save();

                    DB::commit();
                    return response()->json(['message' => 'reservation succeeded']);
                }
            }
        } catch (Exception $e) {
            DB::rollBack();
            report($e);

            return response()->json([
                'message' => 'something went wrong',
                'error' => $e
            ]);
        }
    }

    public function check_room_availablity(Request $request)
    {
        $data = [
            'room_type_id' => $request->room_type_id,
            'checkin' => $request->checkin
        ];

        if ($this->availableRooms($data['room_type_id']) == NULL) {
            // This will queued reservation
            $query = DB::table('room_schedules as rs')
                ->leftJoin('reservations as rsv', 'rsv.room_schedule_id', '=', 'rs.room_schedule_id')
                ->where([
                    ['rsv.did_checkout', '=', NULL],
                ])
                ->whereRaw("( ? BETWEEN rs.checkin and rs.checkout )", [$data['checkin']])
                ->exists();

            return response()->json($query);
        }
    }

    public function booking_summary(Request $request)
    {
        if ($request->get('deposit_slip')) $this->isPayNow = 1;

        // Build parameters
        return [
            'room_type' => RoomType::findOrFail($request->room_type_id),
            'checkin' => $request->checkin,
            'checkout' => $request->checkout,
            'timein' => '14:00',
            'timeout' => '12:00',
            'persons' => $request->persons,
            'days' => $this->dateDiff($request->checkin, $request->checkout),
            'total_cost' => $this->totalCost($request->room_type_id, $this->dateDiff($request->checkin, $request->checkout)),
            'transaction_type_id' => $this->reservation == 1 ? 1 : 2,
            'payment_status' => $this->isPayNow ? 'half_paid' : 'no_pay',
            'pay' => $this->pay($this->totalCost($request->room_type_id, $this->dateDiff($request->checkin, $request->checkout)))
        ];
    }

    // API Helpers
    public function availableRooms($room_type_id)
    {
        $rooms = Reservation::select('room_id')
            ->where([
                ['did_checkout', '=', NULL]
            ])
            ->get();
            
        $room_ids = [];
        foreach ($rooms as $value) {
            array_push($room_ids, $value['room_id']);
        }

        $query = Room::select('room_id')
            ->where('room_type_id', $room_type_id)
            ->whereNotIn('room_id', $room_ids)
            ->first();

        if (!$query) return NULL;
        else return $query->room_id;
    }

    public function pay($total_cost)
    {
        return (int) $total_cost/2;
    }

    public function file($file)
    {
        if ($this->isPayNow) {
            if($file) {
                $image = $file;
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                Image::make($file)->save(public_path('uploaded-images/deposit_slips/').$name);

                return $name;
            }
        }
    }

    public function dateDiff($start, $end) 
    {
        $start = \Carbon\Carbon::parse($start); 
		$end = \Carbon\Carbon::parse($end);
		return $end->diffInDays($start);
    }
    
    public function totalCost($room_type_id, $days)
    {
        $room_rate = DB::table('room_types as rt')
            ->select('rr.cost')
            ->leftJoin('room_rates as rr', 'rr.room_type_id', '=', 'rt.room_type_id')
            ->where([
                ['rr.room_type_id', $room_type_id],
                ['rr.hours', 24]
            ])
            ->first();

        return (int) $room_rate->cost * $days;
    }
}