<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Reservation;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function customers()
    {
        $current_customers = Reservation::with([
            'room_schedule',
            'room', 
            'user',
            'payment'
        ])
        ->where(['did_checkout' => 0, 'is_cancelled' => 0])
        ->get();

        $pdf = \PDF::loadView('pdf.customers', ['current_customers' => $current_customers])->setPaper('a4', 'portrait')->setWarnings(false);
		return $pdf->stream();
    }

    public function all_customers()
    {
        $current_customers = Reservation::with([
            'room_schedule',
            'room', 
            'user',
            'payment'
        ])
        ->where(['is_cancelled' => 0])
        ->get();

        $pdf = \PDF::loadView('pdf.customers', ['current_customers' => $current_customers])->setPaper('a4', 'portrait')->setWarnings(false);
		return $pdf->stream();
    }
}
