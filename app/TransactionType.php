<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationType extends Model
{
    protected $fillable = ['reservation_type'];
    protected $primaryKey = 'reservation_type_id';
    public $timestamps = false;
    
    public function reservations()
    {
        return $this->hasMany('App\Reservation', 'reservation_type_id');
    }
}
