<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['deposit_slip','pay','expiry','is_approved'];
    protected $primaryKey = 'payment_id';
    public $timestamps = false;

    public function reservation()
    {
        return $this->belongsTo('App\Reservation', 'payment_id', 'payment_id');
    }
}