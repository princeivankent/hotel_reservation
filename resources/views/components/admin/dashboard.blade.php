@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="{{ url('libraries/admin/css/viewer.css') }}">
	<style>
		.box-shadow {
			-webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
					box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
		}
		.table-menu li a:hover {
			color: #3C8DBC;
			cursor: pointer;
		}
		.table-menu li .danger:hover {
			color: red;
			cursor: pointer;
		}
		.label-status {
			padding: 6px 18px;
		}
	</style>
@endpush

@section('content')
<section class="content-header">
	<h1>
		Administrator's Dashboard
	</h1>
</section>

<section class="content container-fluid">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>@{{ dashboard.new_reservations }}</h3>

					<p>New Reservations</p>
				</div>
				<div class="icon">
					<i class="ion ion-speakerphone"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>@{{ dashboard.acknowledged_reservations }}</h3>

					<p>Acknowledged Reservations</p>
				</div>
				<div class="icon">
					<i class="ion ion-pricetags"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>@{{ dashboard.total_reservations }}</h3>

					<p>Total Reservations</p>
				</div>
				<div class="icon">
					<i class="ion ion-ios-paper"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-red">
				<div class="inner">
					<h3>@{{ dashboard.outdated_reservations }}</h3>

					<p>Outdated Reservations</p>
				</div>
				<div class="icon">
					<i class="ion ion-thumbsdown"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>

	<div class="box box-default">
		<div class="box-header with-border">
			<h1 class="box-title">Bookings Track</h1>

			<div class="pull-right">
				<label style="margin-right: 10px;">Statuses:</label>
				<span class="text-green" style="margin-right: 8px;">
					<i class="ion ion-checkmark-circled" style="font-size: 18px;"></i>
					approved
				</span>
				<span class="text-info" style="margin-right: 8px;">
					<i class="ion ion-checkmark-circled" style="font-size: 18px;"></i>
					waiting for approval
				</span>
				<span class="text-yellow" style="margin-right: 8px;">
					<i class="ion ion-checkmark-circled" style="font-size: 18px;"></i>
					pending
				</span>
			</div>
		</div>
		<div class="box-body">
			<table id="table" class="table table-bordered table-striped" style="overflow-x: visible;">
				<thead>
					<tr>
						<th class="text-center text-uppercase">&nbsp;</th>
						<th class="text-center text-uppercase">Guest Name</th>
						<th class="text-center text-uppercase">Email Address</th>
						<th class="text-center text-uppercase">Room Type</th>
						<th class="text-center text-uppercase">Room Code</th>
						<th class="text-center text-uppercase bg-green ">Check In</th>
						<th class="text-center text-uppercase bg-yellow">Time In</th>
						<th class="text-center text-uppercase bg-green ">Check Out</th>
						<th class="text-center text-uppercase bg-yellow">Time Out</th>
						<th class="text-center text-uppercase">Reservation Status</th>
						<th class="text-center text-uppercase">Guest</th>
					</tr>
				</thead>
				<tbody>
					<tr 
					v-for="(item, index) in items"
					v-bind:key="index">
						<td>
							<div class="input-group">
								<div class="input-group-btn" style="padding: -2px 20px;">
									<button type="button" class="btn btn-sm btn-defualt dropdown-toggle" data-toggle="dropdown">
										ACTION &nbsp;
										<i class="fa fa-caret-down"></i>
									</button>
									<ul class="dropdown-menu box-shadow table-menu">
										<li v-if="!item.is_approved && item.deposit_slip && !item.expiry">
											<a v-on:click="getPaymentStatus(item.reservation_id)">
												<i class="ion ion-android-checkmark-circle text-primary text-bold"></i>&nbsp;
												Approve Reservation
											</a>
										</li>
										<li v-else-if="item.expiry && !item.deposit_slip && !item.is_approved">
											<a v-on:click="getPaymentStatus(item.reservation_id)">
												<i class="ion ion-search text-primary text-bold"></i>&nbsp;
												No deposit slip
											</a>
										</li>
										<li v-else-if="item.is_approved && item.did_checkout == null">
											<a v-on:click="checkedMeIn(item.reservation_id)">
												<i class="ion ion-android-checkmark-circle text-primary text-bold"></i>&nbsp;
												CHECK IN GUEST
											</a>
										</li>
										<li v-if="!item.did_checkout && !item.is_approved">
											<a v-on:click="cancel(item.reservation_id, index)" class="danger">
												<i class="ion ion-android-close text-red text-bold"></i>&nbsp;
												Cancel Reservation
											</a>
										</li>
										<li v-if="item.is_approved && item.did_checkout == 0">
											<a v-on:click="extendTime(item.room_schedule_id)">
												<i class="ion ion-plus-circled text-primary text-bold"></i>&nbsp;
												EXTEND TIME
											</a>
										</li>
										<li>
											<a :href="`{{ url('/admin/guest_billing_information/room_schedule_id/') }}/${item.room_schedule_id}`">
												<i class="ion ion-android-folder"></i>
												SEE DETAILS
											</a>
										</li>
									</ul>
								</div>
							</div>
						</td>
						<td class="text-primary text-center">
							<i class="fa fa-user"></i> 
							<strong>
								@{{ item.name }}
							</strong>
						</td>
						<td class="text-center">@{{ item.email }}</td>
						<td class="text-center">@{{ item.room_name }}</td>
						<td class="text-center text-primary">@{{ item.room_code }}</td>
						<td class="text-center bg-success">@{{ item.checkin }}</td>
						<td class="text-center bg-warning">@{{ item.timein }}</td>
						<td class="text-center bg-success">@{{ item.checkout }}</td>
						<td class="text-center bg-warning">@{{ item.timeout }}</td>
						<td class="text-center text-uppercase text-bold">
							<label 
							v-if="item.expiry && !item.deposit_slip && !item.is_approved"
							class="label label-warning label-status" :disabled="item.is_outdated == 1 ? true : false">
								PENDING
							</label>
							<label 
							v-else-if="item.is_approved"
							class="label label-success label-status">
								APPROVED
							</label>
							<label 
							v-else-if="!item.expiry && item.deposit_slip"
							class="label label-info label-status">
								WAITING FOR APPROVAL
							</label>
						</td>
						<td class="text-center text-uppercase text-bold">
							<label 
							v-if="item.is_approved && item.did_checkout == 0"
							class="label label-success label-status">
								CHECKED IN
							</label>
							<label 
							v-else
							class="label label-warning label-status">
								WAITING
							</label>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">
			<a href="{{ url('admin/customers/get') }}" class="btn btn-default pull-right">Print Current Customers</a>
			<a href="{{ url('admin/all_customers/get') }}" class="btn btn-default pull-right`~">Print All Customers</a>
		</div>
	</div>
</section>

{{-- @include('components.admin.modals.booking_addons_modal') --}}
@include('components.admin.modals.payment_status')
@endsection

@push('scripts')
<script src="{{ url('libraries/admin/js/viewer.js') }}"></script>
<script>
	document.querySelector('#_dashboard').setAttribute('class', 'active');

	new Vue({
		el: '#app', 
		mixins: [mixin],
		data() {
			return {
				dashboard: {},
				dialog: false,
				items: [],
				itemIndex: -1,
				room_types: [],
				room_type_id: 0,
				form: {
					room_type_id: 0,
					room_code: ''
				},
				defaultItem : {
					room_type_id: 0,
					room_code: ''
				},
				booking_addons: [],
				computed_cost: 0,
				reservation: {}				
			}
		},
		computed: {
			formTitle () {
				return this.itemIndex === -1 ? 'New Item' : 'Edit Item'
			}
		},
		created() {
			this.getDash();
			this.getItems();
		},
		mounted() {
			
		},
		methods: {
			// ------ Added -------------//
			getPaymentStatus(reservation_id) {
				axios.get(`${this.base_url}/admin/active_bookings/get/${reservation_id}`)
				.then(({data}) => {
					this.reservation = data;
					this.acknowledge(reservation_id);
					$('#payment_staus').modal('show');

					if (this.reservation.deposit_slip) {
						setTimeout(() => {
							var viewer = new Viewer(document.getElementById('images'));
							viewer.update();
						});
					}
				})
				.catch((error) => {
					console.log(error.response);
				});
			},
			approveDepositSlip(reservation_id) {
				axios.put(`${this.base_url}/admin/approve_reservation/${reservation_id}`)
				.then(({data}) => {
					console.log(data);
					swal({
						title: "Alright!",
						text: data.message,
						icon: "success",
						button: false,
						timer: 4000
					});
					
					this.getItems();
					axios.get(`${this.base_url}/admin/active_bookings/get/${reservation_id}`)
					.then(({data}) => {
						this.reservation = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
				})
				.catch((error) => {
					console.log(error.response);
				});
			},
			extendTime(room_schedule_id) {
				var input = prompt('Hour(s)');
				if (input > 2) return swal('Ooops!', 'Time limit extension is only 2 hours.', 'error');
				else if (!input) return false;
				return axios.put(`${this.base_url}/admin/extend_time/${room_schedule_id}`, {hours: input})
				.then(({data}) => {
					if (data == false) return swal('Ooops!', 'Time limit extension is only 2 hours.', 'error');
					this.getItems();
					return swal('Success!', `Guest checkout time is extended about ${input} hour(s)`, 'success');
				})
				.catch((error) => {
					console.log(error.response);
				});
			},
			// -----------------
			getDash() {
				axios.get(`${this.base_url}/admin/dashboard/get`)
				.then(({data}) => {
					this.dashboard = data;
				})
				.catch((error) => {
					console.log(error.response);
					toastr.error(error.response);
				});
			},
			getItems() {
				axios.get(`${this.base_url}/admin/active_bookings/get`)
				.then(({data}) => {
					this.items = data;

					setTimeout(() => {
						$('#table').DataTable();   
					});
				})
				.catch((err) => {
					console.log(err.response);
				});
			},
			editItem(room_id, index) {
				axios.get(`${this.base_url}/admin/rooms/get/${room_id}`)
				.then(({data}) => {
					console.log(data);
					this.form = Object.assign({}, data);
					this.itemIndex = index;
					$('#modal-default').modal('show');
				})
				.catch((err) => {
					console.log(err.response);
				});
			},
			deleteItem(room_id, index) {
				var msg = confirm('Are you sure you want to delete this?');
				if (msg) {
					axios.delete(`${this.base_url}/admin/rooms/delete/${room_id}`)
					.then(({data}) => {
						this.items.splice(index, 1);
						toastr.success('Successfully deleted.');
					})
					.catch((err) => {
						console.log(err.response);
					});
				}
			},
			showForm() {
				$('#modal-default').modal('show');
				this.fetchRoomTypes();
				this.itemIndex = -1;
			},
			close() {
				this.dialog = false;
				$('#modal-default').modal('hide');
				setTimeout(() => {
					this.form = Object.assign({}, this.defaultItem)
					this.itemIndex = -1
				}, 300)
			},
			saveForm() {
				if (this.itemIndex > -1) {
					axios.put(`${this.base_url}/admin/rooms/update/${this.form.room_id}`, this.form)
					.then(({data}) => {
						this.close();
						toastr.success('Successfully saved!');
					})
					.catch((err) => {
						console.log(err.response);
					});
				} else {
					axios.post(`${this.base_url}/admin/rooms/store`, this.form)
					.then(({data}) => {
						this.close();
						toastr.success('Successfully saved!');
					})
					.catch((err) => {
						console.log(err.response);
					});
				}
				this.getItems();
			},
			fetchRoomTypes() {
				axios.get(`${this.base_url}/admin/room_types/getAll`)
				.then(({data}) => {
					this.room_types = data;
				})
				.catch((err) => {
					console.log(err.response);
				});
			},
			acknowledge: function(reservation_id) {
				axios.put(`${this.base_url}/admin/acknowledge/update/${reservation_id}`)
				.then(({data}) => {
					this.getItems();
					this.getDash();
				})
				.catch((err) => {
					console.log(err.response);
				});
			},
			cancel(reservation_id) {
				swal({
					title: "Cancel Reservation?",
					text: "Note: There's no way to undo changes",
					icon: "warning",
					dangerMode: true,
					buttons: {
						cancel: true,
						confirm: 'Proceed'
					}
				})
				.then(res => {
					if (res) {
						axios.delete(`${this.base_url}/admin/cancel/${reservation_id}`)
						.then(({data}) => {
							console.log(data);
							this.getItems();
							this.getDash();
							this.notify('Successfully cancelled', true);
						})
						.catch((err) => {
							console.log(err.response);
						});
					}
				});
			},
			openAddons(room_schedule_id) {
				var url = `${this.base_url}/admin/booking_addons/get/${room_schedule_id}`;
				axios.get(url)
				.then(({data}) => {
					this.booking_addons = data;

					var total = 0;
					this.booking_addons.forEach(element => {
						total += element.cost;
					});

					this.computed_cost = total;
					$('#booking_addons_modal').modal('show');
				})
				.catch((err) => {
					console.log(err.response);
				});
			},
			checkedMeIn(reservation_id) {
				swal({
					title: "Check In Guest?",
					text: "Note: There's no way to undo changes",
					icon: "warning",
					dangerMode: true,
					buttons: {
						cancel: true,
						confirm: 'Proceed'
					}
				})
				.then(res => {
					if (res) {
						axios.put(`${this.base_url}/admin/checkin_guest/${reservation_id}`, {status: 0})
						.then(({data}) => {
							this.getItems();
							this.getDash();
							toastr.success('New guest has been Checked In', 'Success!');
						})
						.catch((error) => {
							console.log(error.response);
						});
					}
				});
			}
		}
	});
</script>
@endpush
