<v-layout row wrap justify-center>
    <v-flex xs12 sm12 md7 lg5>
        <v-card>
            <v-list two-line>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content v-if="booking_summary.room_type">
                        <v-list-tile-sub-title>Room Type</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.room_type.room_name }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>
    
                <v-divider></v-divider>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>Check In</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.checkin | dateFormat }} @{{ booking_summary.timein | timeFormat }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>

                <v-divider></v-divider>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>Check Out</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.checkout | dateFormat }} @{{ booking_summary.timeout | timeFormat }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>

                <v-divider></v-divider>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>Number of day(s)</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.days }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>
    
                <v-divider></v-divider>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>No. of Person(s)</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.persons }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>
    
                <v-divider></v-divider>
                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>Total Cost</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.total_cost | phCurrency }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>

                <v-divider></v-divider>

                <v-list-tile>
                    <v-list-tile-action>
                        <v-icon color="blue">ion-android-checkmark-circle</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-content>
                        <v-list-tile-sub-title>DownPayment (50%)</v-list-tile-sub-title>
                        <v-list-tile-title class="blue--text">@{{ booking_summary.pay | phCurrency }}</v-list-tile-title>
                    </v-list-tile-content>
                </v-list-tile>
            </v-list>
        </v-card>
    </v-flex>
</v-layout>

<v-layout row justify-center>
    <v-dialog v-model="terms" persistent max-width="800" light>
        <v-card>
            <v-card-title class="headline">
                <i class="fa fa-exclamation-circle"></i>&nbsp;
                Terms and Conditions
            </v-card-title>
            <v-card-text>
                <ul>
                    <li>The Guest need to pay to Bank 50% Room Fee and insert the Picture to be approved by Administrator</li>
                </ul>
            </v-card-text>
            <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="red darken-1" flat @click.native="terms = false">Disagree</v-btn>
                <v-btn color="blue darken-1" flat v-on:click="termsAlreadyRead">Agree</v-btn>
            </v-card-actions>
        </v-card>
    </v-dialog>
</v-layout>