<?php

namespace App\Http\Controllers;

use App\RoomRate;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomRateController extends Controller
{
    public function index()
    {
        return response()->json(
            DB::table('room_rates as rr')
            ->select('rr.*', 'rt.room_name')
            ->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rr.room_type_id')
            ->oldest('rr.created_at')
            ->get()
        );
    }

    public function show($room_rate_id)
    {
        return response()->json(
            RoomRate::findOrFail($room_rate_id)
        );
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'room_type_id' => 'required|numeric',
            'hours' => 'required|numeric',
            'cost' => 'required'
        ])->validate();

        $query = new RoomRate;
        $query->room_type_id = $request->room_type_id;
        $query->cost = $request->cost;
        $query->hours = $request->hours;
        $query->save();

        return 200;
    }

    public function update(Request $request, $room_rate_id)
    {
        Validator::make($request->all(), [
            'room_type_id' => 'required|numeric',
            'hours' => 'required|numeric',
            'cost' => 'required|numeric'
        ])->validate();

        $query = RoomRate::findOrFail($room_rate_id);
        $query->room_type_id = $request->room_type_id;
        $query->cost = $request->cost;
        $query->hours = $request->hours;
        $query->save();

        return 200;
    }

    public function delete($room_rate_id)
    {
        return response()->json(RoomRate::findOrFail($room_rate_id)->delete());
    }
}
