@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>
			Rooms and Utilities
		</h1>
	</section>
		
	<section class="content container-fluid">
		<div class="box">
			<div class="box-header with-border">
				<h1 class="box-title">Room Types</h1>
				<button 
				v-on:click="addRoomType"
				class="btn btn-default pull-right">
					<i class="fa fa-plus"></i>
					Add New Room
				</button>
			</div>
			<div class="box-body">
				<table id="table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="text-uppercase text-center">Room Types</th>
							<th class="text-uppercase text-center">Amenities</th>
							<th class="text-uppercase text-center">Capacity</th>
							<th class="text-uppercase text-center">Rooms</th>
							<th class="text-uppercase text-center">Room Rates</th>
							<th class="text-uppercase text-center">Images</th>
							<th class="text-uppercase text-center" width="100">Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr 
						v-for="(item, index) in items"
						v-bind:key="index">
							<td class="text-center"><strong class="text-primary">@{{ item.room_name }}</strong></td>
							<td class="text-center">@{{ item.room_description }}</td>
							<td class="text-center">@{{ item.room_capacity }}</td>
							<td class="text-center">
								<a class="btn btn-default btn-sm"
								v-on:click="showRooms(item.room_type_id)"
								>
								<i class="fa fa-folder-open"></i>
								Open Folder
								</a>
							</td>
							<td class="text-center">
								<a class="btn btn-default btn-sm"
								v-on:click="showRoomRate(item.room_type_id)"
								>
								<i class="fa fa-folder-open"></i>
								Open Folder
								</a>
							</td>
							<td class="text-center">
								<a class="btn btn-default btn-sm"
								v-on:click="showUpload(item.room_type_id)"
								>
								<i class="fa fa-folder-open"></i>
								Open Folder
								</a>
							</td>
							<td class="text-center">
								<a class="btn btn-primary btn-sm" v-on:click="editRoomType(item.room_type_id)">
									<i class="fa fa-pencil"></i>
								</a>
								<a class="btn btn-danger btn-sm" 
								v-on:click="deleteRoomType(item.room_type_id)">
									<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<!-- Addons -->
		<div class="box box-default">
			<div class="box-header with-border">
				<h1 class="box-title">
					Addons
				</h1>
				<button 
				class="btn btn-default pull-right" v-on:click="createAddon">
					<i class="fa fa-plus"></i>
					Add New Addons
				</button>
			</div>
			<div class="box-body">
				<table id="addon_table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="text-center" width="40">#</th>
							<th class="text-center">DESCRIPTION</th>
							<th class="text-center">COST</th>
							<th class="text-center">ADDONS FOR</th>
							<th class="text-center" width="100">ACTIONS</th>
						</tr>
					</thead>
					<tbody>
						<tr 
						v-for="(item, index) in addons"
						v-bind:key="index">
							<td class="text-center">@{{ index+1 }}</td>
							<td class="text-center text-primary">@{{ item.description }}</td>
							<td class="text-center text-danger"><strong>P @{{ item.cost }}</strong></td>
							<td class="text-center text-primary">
								<div v-if="item.addon_type === 'receptionist'" class="label label-primary">
									<i class="fa fa-user"></i>
									@{{ item.addon_type }}
								</div>
								<div v-else class="label label-success">
									<i class="fa fa-user"></i>
									@{{ item.addon_type }}
								</div>
							</td>
							<!-- Buttons -->
							<td class="text-center">
								<button class="btn btn-primary btn-sm" v-on:click="editAddon(item.add_on_type_id)">
									<i class="fa fa-pencil"></i>
								</button>
								<button class="btn btn-danger btn-sm" v-on:click="deleteAddon(item.add_on_type_id, index)">
									<i class="fa fa-trash"></i>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	@include('components.admin.modals.room_form')
	@include('components.admin.modals.room_rate_form')
	@include('components.admin.modals.image_form')
	@include('components.admin.modals.addon_form')
	@include('components.admin.modals.room_type_form')

@endsection

@push('scripts')
	<script>
		document.querySelector('#_room_types_v2').setAttribute('class', 'active');
		new Vue({
			el: '#app', 
			mixins: [mixin],
			data: () => {
				return {
					add_rooms_dialog: false,
					add_addons_dialog: false,
					add_rates_dialog: false,

					room_type_id: 0,
					room_code: null,
					room_codes: [],

					items: [],
					rooms: [],
					room_rates: [],
					editedIndex: -1,

					editedItem: {
					  name: '',
					  calories: 0,
					  fat: 0,
					  carbs: 0,
					  protein: 0
					},
					defaultItem: {
					  name: '',
					  calories: 0,
					  fat: 0,
					  carbs: 0,
					  protein: 0
					},

					image: '',
					room_types: [],
					addons: [],
					add_on_type_id: 0,
					addon_edit: false,
					addon_form: {},

					isEdit: 0,
					errors: {},
					form: {
						room_type_id: 0,
						room_name: '',
						room_description: '',
						room_capacity: 0,
						room_image: ''
					}
				}
			},
			computed: {
				formTitle () {
					return this.editedIndex === -1 ? 'New Item' : 'Edit Item';
				},
			},
			created () {
				this.initialize();
				this.getAddons();
			},
			
			methods: {
				editRoomType(room_type_id) {
					this.isEdit = 1;

					axios.get(`${this.base_url}/admin/room_types/get/${room_type_id}`)
					.then(({data}) => {
						$('#room_type_form').modal('show');
						this.form = data;
					})
					.catch((error) => {
						console.log(error.response);
					});
					
				},
				image_dir (dir, file) {
					var locator = dir + '/' + file;
					return locator;
				},
				showUpload: function(room_type_id) {
					this.room_type_id = room_type_id;
					this.getImages(room_type_id);
					$('#images').modal('show');
				},

				showRooms: function(room_type_id) {
					axios.get(`${this.base_url}/admin/rooms/room_type_id/${room_type_id}`)
					.then(({data}) => {
						this.rooms = data;

						$('#rooms').modal('show');
					})
					.catch((err) => {
						console.log(err.response);
					});
				},

				showRoomRate: function(room_type_id) {
					axios.get(`${this.base_url}/admin/room_rates/room_type_id/${room_type_id}`)
					.then(({data}) => {
						this.room_rates = data;

						$('#room_rates').modal('show');
					})
					.catch((err) => {
						console.log(err.response);
					});
				},

				initialize () {
					axios.get(`${this.base_url}/admin/room_types/getAll`)
					.then(({data}) => {
						this.items = data;
						
						setTimeout(() => {
							this.loading = false;
						}, 1000);
					})
					.catch((err) => {
						console.log(err.response);
					});
				},

				editItem (item) {
					this.editedIndex = this.desserts.indexOf(item)
					this.editedItem = Object.assign({}, item)
					this.dialog = true
				},

				deleteItem (item) {
					const index = this.desserts.indexOf(item)
					confirm('Are you sure you want to delete this item?') && this.desserts.splice(index, 1)
				},

				close () {
					this.dialog = false
					setTimeout(() => {
					this.editedItem = Object.assign({}, this.defaultItem)
					this.editedIndex = -1
					}, 300)
				},

				save () {
					if (this.editedIndex > -1) {
						Object.assign(this.desserts[this.editedIndex], this.editedItem)
					} else {
						this.desserts.push(this.editedItem)
					}
					this.close()
				},

				// -----------
				showAddRooms: function(room_type_id) {
					this.add_rooms_dialog = true;
					this.room_type_id = room_type_id;
				},
				showRoomAddons: function(room_type_id) {
					this.add_addons_dialog = true;
					this.room_type_id = room_type_id;
				},
				showAddRates: function(room_type_id) {
					this.add_rates_dialog = true;
					this.room_type_id = room_type_id;
				},
				closeAddRooms: function() {
					this.add_rooms_dialog = false; 
					this.room_codes = [];
				},
				closeRoomAddons: function() {
					this.add_addons_dialog = false; 
				},
				closeAddRoomRates: function() {
					this.add_rates_dialog = false; 
				},

				getRoomCodes: function() {
					if (this.room_code != '' && this.room_code != null) {
						this.room_codes.push({
							room_type_id: this.room_type_id,
							room_code: this.room_code
						});
					}

					this.room_code = '';
				},

				saveRooms: function() {
					if (this.room_codes.length > 0) {
						axios.post(`${this.base_url}/admin/rooms/post`, this.room_codes)
						.then(({data}) => {
							if (data) {
								toastr.success('New item has been added', 'Success!');
								this.add_rooms_dialog = false;
								this.room_codes = [];
								this.initialize();
							}
						})
						.catch((err) => {
							console.log(err.response);
						});
					}
				},

				onImageChange(e) {
					let files = e.target.files || e.dataTransfer.files;
					if (!files.length)
						return;
					this.createImage(files[0]);
				},
				createImage(file) {
					let reader = new FileReader();
					let vm = this;
					reader.onload = (e) => {
						vm.image = e.target.result;
					};
					reader.readAsDataURL(file);
				},
				uploadImage(){
					var params = {
						filename: this.image,
						room_type_id: this.room_type_id
					}
					axios.post(`${this.base_url}/admin/upload`, params)
					.then(({data}) => {
						this.getImages(this.room_type_id);
						toastr.success('You have successfully uploaded an image', 'Success!');
					})
					.catch((err) => {
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},

				getImages: function(room_type_id) {
					axios.get(`${this.base_url}/admin/images/${room_type_id}`)
					.then(({data}) => {
						this.room_types = data;
					})
					.catch((err) => {
						console.log(err.response);
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},

				deleteImage: function(room_image_id) {
					confirm('Are you sure you want to delete this item?') && 
					axios.delete(`${this.base_url}/admin/images/delete/${room_image_id}`)
					.then(({data}) => {
						toastr.success('Successfully deleted');
						this.getImages(this.room_type_id);
					})
					.catch((err) => {
						console.log(err.response);
						toastr.error(err.response.data, 'Something went wrong:');
					});
				}, 
				// Addons
				dataTable: function() {
					$('#addon_table').DataTable({
						destroy: true,
						info: false,
						lengthChange: true
					});
				},
				getAddons: function() {
					var request = axios.get(`${this.base_url}/admin/addons/get`)
					.then(({data}) => {
						this.addons = data;

						return true;
					})
					.catch((err) => {
						console.log(err.response);
						toastr.error(err.response.data, 'Something went wrong:');
					});
					
					request.then((data) => {
						setTimeout(() => {
							this.dataTable();
						});
					});
				},
				createAddon: function() {
					this.addon_form = {};
					this.addon_edit = false;
					$('#addon_form').modal('show');
				},
				editAddon: function(add_on_type_id) {
					this.addon_edit = true;
					this.add_on_type_id = add_on_type_id;
					axios.get(`${this.base_url}/admin/addons/get/${add_on_type_id}`)
					.then(({data}) => {
						this.addon_form = data;
						$('#addon_form').modal('show');
					})
					.catch((err) => {
						console.log(err.response);
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},
				closeAddon: function() {
					this.addon_edit = false;
					$('#addon_form').modal('hide');
				},
				storeAddon: function() {
					if (this.addon_edit) {
						axios.put(`${this.base_url}/admin/addons/update/${this.add_on_type_id}`, this.addon_form)
						.then(({data}) => {
							toastr.success('An item has been updated', 'Success!');
							this.getAddons();
						})
						.catch((err) => {
							console.log(err.response);
							toastr.error(err.response.data, 'Something went wrong:');
						});
					}
					else {
						var request = axios.post(`${this.base_url}/admin/addons/post`, this.addon_form)
						.then(({data}) => {
							toastr.success('New item has been added', 'Success!');
							this.addons.push(this.addon_form);
							this.addon_form = {};
						})
						.catch((err) => {
							console.log(err.response);
							toastr.error(err.response.data, 'Something went wrong:');
						});
					}
				},
				deleteAddon: function(add_on_type_id, index) {
					confirm('Are you sure you want to delete this item?') && 
					axios.delete(`${this.base_url}/admin/addons/delete/${add_on_type_id}`)
					.then(({data}) => {
						toastr.success('An item has been deleted', 'Success!');
						this.addons.splice(index, 1);
					})
					.catch((err) => {
						console.log(err.response);
						toastr.error(err.response.data, 'Something went wrong:');
					});
				},
				addRoomType() {
					$('#room_type_form').modal('show');
				},
				saveRoomType() {
					this.form.image = this.image;
					if (this.isEdit) {
						axios.put(`${this.base_url}/admin/room_types/update/${this.form.room_type_id}`, this.form)
						.then(({data}) => {
							this.initialize();
							toastr.success('Successfull Saved!');
						})
						.catch((error) => {
							console.log(error.response);
						});
					}
					else {
						axios.post(`${this.base_url}/admin/room_types/post`, this.form)
						.then(({data}) => {
							if (data) {
								this.initialize();
								toastr.success('Successfull Saved!');
							}
						})
						.catch((error) => {
							this.errors = error.response.data.errors;
							console.log(error.response);
						});
					}
				},
				deleteRoomType(room_type_id) {
					swal({
						title: "Delete this room type",
						text: "Note: There's no way to undo changes",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then(res => {
						if (res) {
							axios.delete(`${this.base_url}/admin/room_types/delete/${room_type_id}`)
							.then(({data}) => {
								if (data) {
									this.initialize();
									toastr.success('Successfull Saved!');
								}
							})
							.catch((error) => {
								console.log(error.response);
							});
						}
					});
				}
			}
		});
	</script>
@endpush
