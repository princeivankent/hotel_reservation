<?php

namespace App\Http\Controllers;

use Auth;
use App\Reservation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BillingInformationController extends Controller
{
	public function billing_information()
	{
		$query = DB::table('room_schedules as rs')
			->select(
				'rs.room_schedule_id',
				'rs.checkin',
				'rs.timein',
				'rs.checkout',
				'rs.timeout',
				'rs.persons',
				'rs.total_cost',
				'rs.payment_status',
				'tt.transaction_type',
				'u.name',
				'u.email',
				'r.room_code',
				'rt.room_name',
				'rt.room_description',
				'rt.room_capacity',
				'rsv.did_checkout'
			)
			->leftJoin('transaction_types as tt', 'tt.transaction_type_id', '=', 'rs.transaction_type_id')
			->leftJoin('reservations as rsv', 'rsv.room_schedule_id', '=', 'rs.room_schedule_id')
			->leftJoin('users as u', 'u.user_id', '=', 'rsv.user_id')
			->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rs.room_type_id')
			->leftJoin('rooms as r', 'r.room_id', '=', 'rsv.room_id')
			->latest('rs.created_at')
			->get();

		foreach ($query as $key => $value) {
			$value->timein = Carbon::parse($value->timein)->format('h:i A');
			$value->timeout = Carbon::parse($value->timeout)->format('h:i A');
			$value->checkin = Carbon::parse($value->checkin)->format('M. d Y | D');
			$value->checkout = Carbon::parse($value->checkout)->format('M. d Y | D');
		}

		return response()->json($query);
	}

	public function show($room_schedule_id)
	{
		$billing = DB::table('room_schedules as rs')
			->select(
				'rs.room_schedule_id',
				'rs.checkin',
				'rs.timein',
				'rs.checkout',
				'rs.timeout',
				'rs.persons',
				'rs.total_cost',
				'rs.payment_status',
				'tt.transaction_type',
				'u.name',
				'u.email',
				'r.room_code',
				'rt.room_name',
				'rt.room_description',
				'rt.room_capacity'
			)
			->leftJoin('transaction_types as tt', 'tt.transaction_type_id', '=', 'rs.transaction_type_id')
			->leftJoin('reservations as rsv', 'rsv.room_schedule_id', '=', 'rs.room_schedule_id')
			->leftJoin('users as u', 'u.user_id', '=', 'rsv.user_id')

			->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rs.room_type_id')
			->leftJoin('rooms as r', 'r.room_id', '=', 'rsv.room_id')
			
			->where([
				'rsv.room_schedule_id' => $room_schedule_id
			])
			->first();

			$billing->timein = Carbon::parse($billing->timein)->format('h:i A');
			$billing->timeout = Carbon::parse($billing->timeout)->format('h:i A');
			$billing->checkin = Carbon::parse($billing->checkin)->format('M. d Y | D');
			$billing->checkout = Carbon::parse($billing->checkout)->format('M. d Y | D');

		$addons = DB::table('room_add_ons as rao')
			->select(
				'rao.room_add_on_id',
				'aot.description',
				'rao.quantity',
				'rao.cost'
			)
			->leftJoin('add_on_types as aot', 'aot.add_on_type_id', '=', 'rao.add_on_type_id')
			->where([
				'room_schedule_id' => $room_schedule_id
			])
			->oldest('rao.created_at')
			->get();

		return response()->json([
			'billing' => $billing,
			'addons' => $addons
		]);
	}

	public function update_payment_status(Request $request, $room_schedule_id)
	{
		$query = DB::table('room_schedules')
			->where('room_schedule_id', $room_schedule_id)
			->update(['payment_status' => $request->status]);

		return response()->json($query);
	}

	public function official_receipt($room_schedule_id)
	{
		$list = DB::table('room_schedules as rs')
			->select(
				'rs.room_type_id',
				'rs.checkin',
				'rs.checkout',
				'rs.timein',
				'rs.timeout',
				'rs.persons',
				'rs.days',
				'rs.total_cost',
				'rsv.reservation_id'
			)
			->leftJoin('reservations as rsv', 'rsv.room_schedule_id', '=', 'rs.room_schedule_id')
			->where([
				['rs.room_schedule_id', '=', $room_schedule_id]
			])
			->get();

		$header = [
			'guest' => $this->guest($room_schedule_id)->name,
			'email' => $this->guest($room_schedule_id)->email,
		];

		$new_list = [];
		foreach ($list as $key => $value) {
			$header['checkin'] = $value->checkin;
			$header['checkout'] = $value->checkout;
			$header['official_receipt_code'] = $value->reservation_id;
			
			$new_list[$key]['date'] = Carbon::now()->toDateString();
			$new_list[$key]['description'] = 'Room Charge';
			$new_list[$key]['days'] = $this->dateDiff($value->checkin, $value->checkout);
			$new_list[$key]['amount_per_day'] = $this->originalCost($value->room_type_id);
			$new_list[$key]['total'] = $this->totalCost($value->room_type_id, $this->dateDiff($value->checkin, $value->checkout));
			$new_list[$key]['cashier'] = Auth::user()->name;
			$new_list[$key]['guest'] = $this->guest($room_schedule_id)->name;
		}

		return response()->json([
			'list' => $new_list,
			'header' => $header
		]);
	}

	public function generate_official_receipt($room_schedule_id)
	{
		$query = DB::table('reservations')->where('room_schedule_id', $room_schedule_id)->update([
			'did_checkout' => 1
		]);

		if ($query) {
			$list = DB::table('room_schedules as rs')
			->select(
				'rs.room_type_id',
				'rs.checkin',
				'rs.checkout',
				'rs.timein',
				'rs.timeout',
				'rs.persons',
				'rs.days',
				'rs.total_cost',
				'rsv.reservation_id'
			)
			->leftJoin('reservations as rsv', 'rsv.room_schedule_id', '=', 'rs.room_schedule_id')
			->where([
				['rs.room_schedule_id', '=', $room_schedule_id]
			])
			->get();

		$header = [
			'guest' => $this->guest($room_schedule_id)->name,
			'email' => $this->guest($room_schedule_id)->email,
		];

		$new_list = [];
		foreach ($list as $key => $value) {
			$header['checkin'] = $value->checkin;
			$header['checkout'] = $value->checkout;
			$header['official_receipt_code'] = $value->reservation_id;
			
			$new_list[$key]['date'] = Carbon::now()->toDateString();
			$new_list[$key]['description'] = 'Room Charge';
			$new_list[$key]['days'] = $this->dateDiff($value->checkin, $value->checkout);
			$new_list[$key]['amount_per_day'] = $this->originalCost($value->room_type_id);
			$new_list[$key]['total'] = $this->totalCost($value->room_type_id, $this->dateDiff($value->checkin, $value->checkout));
			$new_list[$key]['cashier'] = Auth::user()->name;
			$new_list[$key]['guest'] = $this->guest($room_schedule_id)->name;
		}

		$data = [
			'list' => $new_list,
			'header' => $header
		];
		$pdf = \PDF::loadView('pdf.official_receipt', $data)->setPaper('a4', 'portrait')->setWarnings(false);
		return $pdf->download('official_receipt.pdf');
		}
	}

	public function guest($room_schedule_id)
	{
		$query = DB::table('reservations as rsv')
			->select('u.*')
			->leftJoin('users as u', 'u.user_id', '=', 'rsv.user_id')
			->where('rsv.room_schedule_id', $room_schedule_id)
			->first();

		return $query;
	}

	public function dateDiff($start, $end) 
    {
        $start = \Carbon\Carbon::parse($start); 
		$end = \Carbon\Carbon::parse($end);
		return $end->diffInDays($start);
	}

	public function originalCost($room_type_id)
	{
		$room_rate = DB::table('room_types as rt')
            ->select('rr.cost')
            ->leftJoin('room_rates as rr', 'rr.room_type_id', '=', 'rt.room_type_id')
            ->where([
                ['rr.room_type_id', $room_type_id],
                ['rr.hours', 24]
            ])
            ->first();

        return (int) $room_rate->cost;
	}
	
	public function totalCost($room_type_id, $days)
    {
        $room_rate = DB::table('room_types as rt')
            ->select('rr.cost')
            ->leftJoin('room_rates as rr', 'rr.room_type_id', '=', 'rt.room_type_id')
            ->where([
                ['rr.room_type_id', $room_type_id],
                ['rr.hours', 24]
            ])
            ->first();

        return (int) $room_rate->cost * $days;
    }
}
