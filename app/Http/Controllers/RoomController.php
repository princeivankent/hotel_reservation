<?php

namespace App\Http\Controllers;

use App\RoomRate;
use App\Room;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RoomController extends Controller
{
	public function rooms()
	{
		return response()->json(Room::with('room_type')->oldest()->get()->toArray());
	}

	public function room($room_id)
	{
		return response()->json(Room::findOrFail($room_id));
	}

	public function store(Request $request)
	{
		$query = new Room;
		$query->room_type_id = $request->room_type_id;
		$query->room_code = $request->room_code;
		$query->save();

		return response()->json($query);
	}

	public function update(Request $request, $room_id)
	{
		$query = Room::findOrFail($room_id);
		$query->room_type_id = $request->room_type_id;
		$query->room_code = $request->room_code;
		$query->save();

		return response()->json($query);
	}

	public function delete($room_id)
	{
		$query = Room::findOrFail($room_id)->delete();

		return response()->json($query);
	}

	public function add_rooms(Request $request)
	{
		foreach ($request->all() as $key => $value) {
			$query = new Room;
			$query->room_type_id = $value['room_type_id'];
			$query->room_code = $value['room_code'];
			$query->save();
		}

		if ($query) return 200;
	}

	public function show_rooms($room_type_id)
	{
		return DB::table('rooms as r')
			->selectRaw('r.*, rsv.room_id as reservation_room_id')
			->leftJoin('reservations as rsv', 'rsv.room_id', '=', 'r.room_id')
			->where('r.room_type_id', $room_type_id)
			->get();
	}

	public function show_room_rates($room_type_id)
	{
		return response()->json(
			RoomRate::where([
					['room_type_id', '=', $room_type_id]
				])
				->oldest()
				->get()
		);	
	}
	
	public function toggle_maintenance($room_id, $status)
	{
		$query = DB::table('rooms')
			->where([
				'room_id' => $room_id
			])
			->update([
				'is_maintenance' => $status
			]);

		return $query;
	}
}
