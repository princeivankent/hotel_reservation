<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddOnType extends Model
{
    protected $fillable = ['description','cost','addon_type'];
    protected $primaryKey = 'add_on_type_id';
    public $timestamps = false;
    
    public function room_add_ons()
    {
        return $this->hasMany('App\RoomAddOn', 'add_on_type_id');
    }
}
