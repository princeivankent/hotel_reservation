<?php

namespace App\Http\Controllers;

use App\User;
use App\RoomSchedule;
use App\Reservation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ActiveBookingController extends Controller
{
	public function active_bookings()
	{
		$query = User::
			select(
				'users.user_id',
				'users.name',
				'users.email',
				'rt.room_name',
				'r.room_code',
				'rs.room_schedule_id',
				'rs.checkin',
				'rs.timein',
				'rs.checkout',
				'rs.timeout',
				'rs.persons',
				'rs.days',
				'rs.total_cost',
				'rs.payment_status',
				'rsv.is_acknowledged',
				'rsv.reservation_id',
				'rsv.did_checkout',
				'p.deposit_slip',
				'p.pay',
				'p.expiry',
				'p.is_approved'
			)
			->leftJoin('reservations as rsv', 'rsv.user_id', '=', 'users.user_id')
			->leftJoin('payments as p', 'p.payment_id', '=', 'rsv.payment_id')
			->leftJoin('room_schedules as rs', 'rs.room_schedule_id', '=', 'rsv.room_schedule_id')
			->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rs.room_type_id')
			->leftJoin('rooms as r', 'r.room_id', '=', 'rsv.room_id')
			->where([
				['r.is_maintenance', '=', 0],
				['rsv.did_checkout', '=', NULL],
				['rsv.is_cancelled', '=', 0]
			])
			->orWhere('rsv.did_checkout', 0)
			->with('reservations.room_addons.add_on_type')
			->latest('rsv.created_at')
			->get();

		foreach ($query as $key => $value) {
			if ($value->checkin < Carbon::now()->toDateString()
				&& $value->checkout < Carbon::now()->toDateString()
				&& $value->expiry < Carbon::now()->toDateString()
			) {
				$value->is_outdated = 1;
			}
			else {
				$value->is_outdated = 0;
			}

			$value->timein = Carbon::parse($value->timein)->format('h:i A');
			$value->timeout = Carbon::parse($value->timeout)->format('h:i A');
			$value->checkin = Carbon::parse($value->checkin)->format('M. d Y | D');
			$value->checkout = Carbon::parse($value->checkout)->format('M. d Y | D');
		}

		return response()->json($query);
	}

	public function active_booking($reservation_id)
	{
		$query = User::
			select(
				'users.user_id',
				'users.name',
				'users.email',
				'rt.room_name',
				'r.room_code',
				'rs.room_schedule_id',
				'rs.checkin',
				'rs.timein',
				'rs.checkout',
				'rs.timeout',
				'rs.persons',
				'rs.days',
				'rs.total_cost',
				'rs.payment_status',
				'rsv.is_acknowledged',
				'rsv.reservation_id',
				'rsv.did_checkout',
				'p.deposit_slip',
				'p.pay',
				'p.expiry',
				'p.is_approved'
			)
			->leftJoin('reservations as rsv', 'rsv.user_id', '=', 'users.user_id')
			->leftJoin('payments as p', 'p.payment_id', '=', 'rsv.payment_id')
			->leftJoin('room_schedules as rs', 'rs.room_schedule_id', '=', 'rsv.room_schedule_id')
			->leftJoin('room_types as rt', 'rt.room_type_id', '=', 'rs.room_type_id')
			->leftJoin('rooms as r', 'r.room_id', '=', 'rsv.room_id')
			->where([
				['r.is_maintenance', '=', 0],
				['rsv.reservation_id', $reservation_id],
				['rsv.is_cancelled', '=', 0]
			])
			->with('reservations.room_addons.add_on_type')
			->latest('rsv.created_at')
			->first();

		return response()->json($query);
	}

	public function approve_reservation($reservation_id)
	{
		$payment = DB::table('reservations as rsv')->select('payment_id')->where('reservation_id', $reservation_id)->first();
		$query = DB::table('payments')->where('payment_id', $payment->payment_id)->update(
			['is_approved' => 1]
		);

		return response()->json([
			'message' => 'Successfully Approved!'
		]);
	}

	public function acknowledge($reservation_id)
	{
		$query = Reservation::where('reservation_id', $reservation_id)->update(['is_acknowledged' => 1]);
		return response()->json($query);
	}

	public function booking_addons($room_schedule_id)
	{
		$query = DB::table('room_add_ons as rao')
			->select(
				'rao.room_add_on_id',
				'aot.description',
				'rao.quantity',
				'rao.cost'
			)
			->leftJoin('add_on_types as aot', 'aot.add_on_type_id', '=', 'rao.add_on_type_id')
			->where([
				'room_schedule_id' => $room_schedule_id
			])
			->oldest('rao.created_at')
			->get();

		return response()->json($query);
	}

	public function checkin_guest(Request $request, $reservation_id)
	{
		$query = Reservation::findOrFail($reservation_id);
		$query->did_checkout = $request->status;
		$query->save();

		return response()->json($query);
	}

	public function extend_time(Request $request, $room_schedule_id)
	{	
		$query = RoomSchedule::findOrFail($room_schedule_id);

		if (Carbon::parse('14:00:00')->toTimeString() < Carbon::parse($query->timeout)->toTimeString()) return response()->json(false);

		$query->timeout = Carbon::parse($query->timeout)->addHours($request->hours);
		$query->save();

		return response()->json($query);
	}
}
