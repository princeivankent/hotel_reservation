require('./bootstrap');

window.Vue = require('vue');
window.Vuetify = require('vuetify');
window.swal = require('sweetalert');

import 'material-design-icons-iconfont/dist/fonts/material-icons.css';
import 'vuetify/dist/vuetify.min.css';
import * as easings from 'vuetify/es5/util/easing-patterns'

Vue.use(Vuetify);
Vue.use(easings);

