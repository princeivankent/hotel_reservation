<v-layout
column
wrap
class="my-5"
align-center
>
    <v-flex xs12 sm4 class="my-3">
        <div class="text-xs-center">
            <h2 class="headline">Feel free to find you perfect home</h2>
            <span class="subheading">
                All our rooms are fully air-conditioned, with hot & cold shower, free 2 bottles of water, TV and with FREE toiletrees.
            </span>
        </div>
    </v-flex>
    
    <v-flex xs12>
        <v-container grid-list-xl>
            <v-layout row wrap>
                <v-flex md4 v-for="(item, index) in items" v-bind:key="item.room_type_id">
                    <v-card class="elevation-8" color="#342010">
                        <v-img :src="`{{ asset('uploaded-images') }}/${item.image}`" aspect-ratio="1.75" height="200px;"></v-img>
                        <v-card-title primary-title>
                            <div>
                                <h3 class="headline mb-0">@{{ item.room_name }}</h3>
                                <div class="grey--text text--darken-1">@{{ item.room_description }}</div>
                            </div>
                        </v-card-title>
            
                        <v-card-actions>
                            <v-spacer></v-spacer>
                            <v-btn v-on:click="createBooking(item.room_type_id)" flat color="orange">Explore</v-btn>
                        </v-card-actions>
                    </v-card>
                </v-flex>
            </v-layout>
        </v-container>
    </v-flex>
</v-layout>