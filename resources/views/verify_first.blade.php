<!DOCTYPE html>
<html>
	<head>
		<title>{{ config('app.name', 'Laravel') }}</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<script src="{{ asset('js/vuetify.js') }}"></script>
		<link rel="stylesheet" href="{{ asset('libraries/admin/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('libraries/admin/css/ionicons.min.css') }}">
		<style>
			[v-cloak] > * {display: none;}
			a {
				text-decoration: none;
				margin: 10px;
			}
		</style>
		@stack('styles')
	</head>
	<body>
		<div id="app" v-cloak>
			<v-app dark class="cover-image white--text" style="background-image: url('/images/background.jpg')">
				<v-toolbar fixed app color="#21130A">
					<a href="{{ url('/') }}">
                        <v-toolbar-title class="hidden-sm-and-down orange--text darken-1">PARADISE HOTEL</v-toolbar-title>
                    </a>

                    <v-spacer></v-spacer>

                    <v-toolbar-items>
                        <v-btn flat v-bind:color="home_tab" href="{{ url('/') }}">Home</v-btn>
                        <v-btn flat v-bind:color="about_tab" href="{{ url('/about_us') }}">About Us</v-btn>
                        <v-btn flat v-bind:color="faq_tab" href="{{ url('frequently_asked_questions') }}">FAQ</v-btn>
                    </v-toolbar-items>

                    <v-divider class="mr-3" inset vertical dark></v-divider>

                    @if (Route::has('login'))
                        @auth
                        <v-menu offset-y>
                            <v-btn
                                slot="activator"
                                color="success"
                                dark
                                @click.stop="drawer = !drawer"
                            >
                                <v-icon class="mr-1">ion ion-ios-contact</v-icon>&nbsp;
                                {{ Auth::user()->name }}
                            </v-btn>
                        </v-menu>
                        
                        @else
                            <h4>
                                <a href="{{ route('login') }}">Login</a>
                                <a href="{{ route('register') }}">Register</a>
                            </h4>
                        @endauth
                    @endif
				</v-toolbar>

				<v-content id="header">
					@yield('content')
					@include('hotel_templates.navigation_drawer')
				</v-content>
			</v-app>
		</div>
		<script src="{{ asset('libraries/admin/js/moment.min.js') }}"></script>
		<script>
			Vue.mixin({
				data() {
					return {
						base_url: "{{ url('/') }}",
						drawer: false,
						home_tab: 'grey',
						about_tab: 'grey',
						faq_tab: 'grey',
					}
				},
				filters: {
					phCurrency(value) {
						if (!value) return ''
						value = parseInt(value);
						return '₱ ' + value.toLocaleString('en');
					},
					timeFormat(time) {
						return moment(time, "HH:mm A").format('hh:mm A');
					},
					dateFormat(value) {
						if (!value) return ''
						value = value.toString()
						return moment(value).format('MMMM D, YYYY')
					},
					dateTimeFormat(value) {
						if (!value) return ''
						value = value.toString()
						return moment(value).format('MMMM D, YYYY | h:mm:ss a')
					}
				}
			})
		</script>
		@stack('scripts')
	</body>
</html>
