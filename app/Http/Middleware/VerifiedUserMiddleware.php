<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class VerifiedUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset(Auth::user()->user_id)) {
            if (Auth::user()->is_verified == 0) return redirect()->route('verify_first');
        }

        return $next($request);
    }
}