<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $fillable = ['room_name','description','room_capacity'];
    protected $primaryKey = 'room_type_id';
    public $timestamps = false;

    public function rooms()
    {
        return $this->hasMany('App\Room', 'room_type_id');
    }

    public function room_rates()
    {
        return $this->hasMany('App\RoomRate', 'room_type_id');
    }

    public function room_images()
    {
        return $this->hasMany('App\RoomImage', 'room_type_id');
    }
}
