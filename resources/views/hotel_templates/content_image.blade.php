{{-- <v-carousel>
    <v-carousel-item
        v-for="(item,i) in items"
        :key="i"
        :src="item.src"
    ></v-carousel-item>
</v-carousel> --}}

{{-- <v-card>
    <v-img
        src="{{ asset('images/designs/image_c1.JPG') }}" 
        height="600"
        class="grey darken-4"
    ></v-img>
    <v-card-title class="title">max-height with contain</v-card-title>
</v-card> --}}

<v-card flat class="transaparent">
    <v-carousel
        delimiter-icon="stop"
        prev-icon="mdi-arrow-left"
        next-icon="mdi-arrow-right"
        height="750"
    >
        <v-carousel-item
            v-for="(item,i) in main_images"
            :key="i"
            :src="item.src"
            class="grey darken-4">
        </v-carousel-item>
    </v-carousel>
</v-card>