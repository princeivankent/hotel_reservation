<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomAddOn extends Model
{
    protected $fillable = ['add_on_type_id','room_schedule_id','quantity','cost'];
    protected $primaryKey = 'room_add_on_id';
    public $timestamps = false;

    public function room_type()
    {
        return $this->hasMany('App\RoomSchedule', 'room_schedule_id');
    }

    public function add_on_type()
    {
        return $this->belongsTo('App\AddOnType', 'add_on_type_id');
    }
}
