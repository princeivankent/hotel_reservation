<div class="modal fade" id="booking_details">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<i class="fa fa-book"></i>
					Your Booking Details
				</h4>
			</div>
			<div class="modal-body">
				<div class="box box-default box-shadow">
					<div class="box-header with-border">
						<h1 class="box-title">
							<i class="fa fa-list"></i>
							Room Details
						</h1>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="">Room Type</label>
							<p class="text-primary">@{{ room_type.room_name }}</p>
						</div>
						<div class="form-group">
							<label for="">Amenities</label>
							<p class="text-primary">@{{ room_type.room_description }}</p>
						</div>
						<div class="form-group">
							<label for="" style="margin-right: 5px;">Room Cost</label>
							<div v-if="cost == 'Select room rate below'" class="label label-primary">@{{ cost | phCurrency }}</div>
							<div v-else class="label label-primary">@{{ cost | phCurrency }}</div>
						</div>
						<div class="form-group clearfix">
							<label for="">Days</label>
							<div class="label label-primary">@{{ days }}</div>
						</div>
						<div class="form-group pull-right">
							<label for="">Total</label>
							<hr style="margin: 0px 0px;">
							<p class="text-danger text-bold">@{{ days*cost | phCurrency }}</p>
						</div>
					</div>
				</div>
				
				<div v-if="pickedAddons.length > 0" class="box box-default box-shadow">
					<div class="box-header with-border">
						<h1 class="box-title">
							<i class="fa fa-plus"></i>
							Addons
						</h1>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center" width="40">#</th>
									<th class="text-center">DESCRIPTION</th>
									<th class="text-center">QUANTITY</th>
									<th class="text-center">COST</th>
								</tr>
							</thead>
							<tbody>
								<tr 
								v-for="(item, index) in pickedAddons"
								v-bind:key="index">
									<td class="text-center">@{{ index+1 }}</td>
									<td class="text-center text-primary">@{{ item.description }}</td>
									<td class="text-center text-primary">@{{ item.quantity }}</td>
									<td class="text-center text-danger">@{{ item.cost | phCurrency }}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th class="text-success text-right">Total Addons:</th>
									<th class="text-danger text-center">@{{ computed_cost | phCurrency }}</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				
				<div class="box box-default box-shadow">
					<div class="box-header with-border clearfix">
						<h1 class="box-title pull-right">
							Grand Total: <span class="text-success">@{{ grand_total | phCurrency }}</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success" v-on:click="proceedToStripe">
					<i class="fa fa-success"></i>
					PROCEED TO CARD DETAILS FORM
				</button>
			</div>
		</div>
	</div>
</div>
