<div class="modal fade" tabindex="-1" role="dialog" id="function_room_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@{{ formTitle }}</h4>
            </div>
            <div class="modal-body">
                <form v-on:keyup.enter="saveForm">
                    <input type="hidden" class="form-control" v-model="form.function_room_id">
                    <div class="form-group">
                        <label for="name">Function Room Name</label>
                        <input type="text" class="form-control" v-model="form.name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="7" v-model="form.description"></textarea>
    
                        <span v-if="errors.description" class="text-danger">
                            <strong>
                                @{{ errors.description[0] }}
                            </strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="capacity">Capacity</label>
                        <input type="text" class="form-control" v-model="form.capacity">
    
                        <span v-if="errors.capacity" class="text-danger">
                            <strong>
                                @{{ errors.capacity[0] }}
                            </strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="cost">Cost</label>
                        <input type="text" class="form-control" v-model="form.cost">
    
                        <span v-if="errors.cost" class="text-danger">
                            <strong>
                                @{{ errors.cost[0] }}
                            </strong>
                        </span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button v-on:click="saveForm" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>