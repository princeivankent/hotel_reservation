<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $fillable = ['mission_vision','history','about_our_hotel'];
    protected $primaryKey = 'page_content_id';
    protected $table = 'page_content';
    public $timestamps = false;
}
