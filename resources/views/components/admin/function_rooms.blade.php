@extends('layouts.app')

@push('styles')
<link rel="stylesheet" href="{{ url('libraries/admin/css/viewer.css') }}">
<style>
    a {
        cursor: pointer;
    }
</style>
@endpush

@section('content')
    <section class="content-header">
        <h1>
            Function Rooms
        </h1>
    </section>
    
    <section class="content container-fluid">
        <div class="box">
            <div class="box-header with-border">
                <h1 class="box-title">List of Function Rooms</h1>
                <button 
                class="btn btn-primary pull-right" v-on:click="showForm">
                    <i class="fa fa-plus"></i>
                    Add Function Room
                </button>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" width="90">&nbsp;</th>
                            <th class="text-center">Function Room</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Capacity</th>
                            <th class="text-center">Images</th>
                            <th class="text-center">Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items" v-bind:key="index">
                            <td class="text-center">
                                <div class="input-group">
                                    <div class="input-group-btn" style="padding: -2px 20px;">
                                        <button type="button" class="btn btn-sm btn-defualt dropdown-toggle" data-toggle="dropdown">
                                            ACTION &nbsp;
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu box-shadow table-menu">
                                            <li>
                                                <a v-on:click="editItem(item.function_room_id)">
                                                    <i class="fa fa-pencil text-primary"></i>&nbsp;
                                                    edit
                                                </a>
                                            </li>
                                            <li>
                                                <a v-on:click="deleteItem(item.function_room_id)">
                                                    <i class="fa fa-trash text-danger"></i>&nbsp;
                                                    delete
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">@{{ item.name }}</td>
                            <td class="text-center">@{{ item.description }}</td>
                            <td class="text-center">@{{ item.capacity }}</td>
                            <td class="text-center" width="100">
                                <a class="btn btn-default btn-sm" v-on:click="showImages(item.function_room_id)">
                                    <i class="fa fa-image"></i>
                                    browse images
                                </a>
                            </td>
                            <td class="text-center text-danger">@{{ item.cost | phCurrency }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('components.admin.modals.function_room_modal')
    @include('components.admin.modals.function_images')
@endsection

@push('scripts')
    <script src="{{ url('libraries/admin/js/viewer.js') }}"></script>
    <script>
        document.querySelector('#function_room_tab').setAttribute('class', 'active');
        new Vue({
            el: '#app', 
            mixins: [mixin],
            data() {
                return {
                    dialog: false,
                    items: [],
                    isEdit: false,
                    form: {},
                    errors: {},
                    image: '',
                    images: [],
                    function_room_id: 0
                }
            },
            computed: {
                formTitle () {
                    return this.isEdit == false ? 'New Item' : 'Edit Item';
                }
            },
            created() {
                this.getItems();
            },
            methods: {
                showImages(function_room_id) {
                    axios.get(`${this.base_url}/admin/function_rooms/get/${function_room_id}`)
                    .then(({data}) => {
                        this.images = data.function_images;
                        $('#function_image_modal').modal('show');
                        this.function_room_id = function_room_id;

                        if (this.images) {
                            setTimeout(() => {
                                var viewer = new Viewer(document.getElementById('images'));
                                viewer.update();
                            });
                        }
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },
                image_dir (dir, file) {
					var locator = dir + '/' + file;
					return locator;
				},
                onImageChange(e) {
					let files = e.target.files || e.dataTransfer.files;
					if (!files.length)
						return;
					this.createImage(files[0]);
				},
				createImage(file) {
					let reader = new FileReader();
					let vm = this;
					reader.onload = (e) => {
						vm.image = e.target.result;
					};
					reader.readAsDataURL(file);
				},
				uploadImage(){
					var params = {
						image: this.image,
						function_room_id: this.function_room_id
					}
					axios.post(`${this.base_url}/admin/function_rooms/store_image`, params)
					.then(({data}) => {
						this.showImages(this.function_room_id);
						toastr.success('You have successfully uploaded an image', 'Success!');
					})
					.catch((err) => {
						toastr.error(err.response.data, 'Something went wrong:');
                        console.log(err.response.data);
					});
				},
				
                deleteImage(function_image_id) {
                    confirm('Are you sure you want to delete this item?') && 
					axios.delete(`${this.base_url}/admin/function_images/delete_image/${function_image_id}`)
					.then(({data}) => {
						toastr.success('Successfully deleted');
						this.showImages(this.function_room_id);
					})
					.catch((err) => {
						console.log(err.response);
					});
                },

                getItems: function() {
                    axios.get(`${this.base_url}/admin/function_rooms/get`)
                    .then(({data}) => {
                        this.items = data;

                        setTimeout(() => {
                            $('#table').DataTable();   
                        });
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                editItem: function(function_room_id) {
                    this.isEdit = true;

                    axios.get(`${this.base_url}/admin/function_rooms/get/${function_room_id}`)
                    .then(({data}) => {
                        this.form = Object.assign({}, data);
                        $('#function_room_modal').modal('show');
                    })
                    .catch((err) => {
                        console.log(err.response);
                    });
                },

                deleteItem: function(function_room_id, index) {
                    swal({
						title: "Delete this function room?",
						text: "Note: There's no way to undo changes",
						icon: "warning",
						dangerMode: true,
						buttons: {
							cancel: true,
							confirm: 'Proceed'
						}
					})
					.then(res => {
						if (res) {
							axios.delete(`${this.base_url}/admin/function_rooms/destroy/${function_room_id}`)
                            .then(({data}) => {
                                this.getItems();
                                swal({
                                    title: "Alright!",
                                    text: 'Successfully deleted.',
                                    icon: "success",
                                    timer: 3000
                                });
                            })
                            .catch((err) => {
                                console.log(err.response);
                            });
						}
					});
                },

                showForm() {
                    $('#function_room_modal').modal('show');
                    this.form = {};
                    this.isEdit = false;
                },

                close() {
                    $('#function_room_modal').modal('hide');
                    this.errors = {};
                    this.form = {};
                    this.isEdit = false;
                },

                saveForm: function() {
                    if (this.isEdit == true) {
                        axios.put(`${this.base_url}/admin/function_rooms/update/${this.form.function_room_id}`, this.form)
                        .then(({data}) => {
                            this.getItems();
                            this.close();
                            swal({
                                title: "Alright!",
                                text: 'Successfully saved.',
                                icon: "success",
                                timer: 3000
                            });
                        })
                        .catch((err) => {
                            console.log(err.response);
                            this.errors = err.response.data.errors;
                        });
                    } else {
                        axios.post(`${this.base_url}/admin/function_rooms/store`, this.form)
                        .then(({data}) => {
                            this.getItems();
                            this.close();
                            swal({
                                title: "Alright!",
                                text: 'Successfully saved.',
                                icon: "success",
                                timer: 3000
                            });
                        })
                        .catch((err) => {
                            console.log(err.response);
                            this.errors = err.response.data.errors;
                            toastr.error(err.response);
                        });
                    }
                },
            }
        });
    </script>
@endpush