<?php

namespace App\Http\Controllers;

use Image;
use App\FunctionImage;
use App\FunctionRoom;
use Illuminate\Http\Request;

class FunctionRoomController extends Controller
{
    public function index()
    {
        return response()->json(
            FunctionRoom::with([
                'function_reservations',
                'function_images'
            ])
            ->get()
        );
    }

    public function show($function_room_id)
    {
        return response()->json(
            FunctionRoom::with([
                'function_reservations',
                'function_images'
            ])
            ->findOrFail($function_room_id)
        );
    }

    public function store(Request $request)
    {
        $query = new FunctionRoom;
        $query->name = $request->name;
        $query->description = $request->description;
        $query->capacity = $request->capacity;
        $query->cost = $request->cost;
        $query->save();

        return response()->json($query);
    }

    public function update(Request $request, $function_room_id)
    {
        $query = FunctionRoom::findOrFail($function_room_id);
        $query->name = $request->name;
        $query->description = $request->description;
        $query->capacity = $request->capacity;
        $query->cost = $request->cost;
        $query->save();

        return response()->json($query);
    }

    public function store_image(Request $request)
    {
        $query = new FunctionImage;
        $query->function_room_id = $request->function_room_id;
        
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($image)->save(public_path('uploaded-images/').$name);
            $query->image = $name;
        }

        $query->save();

        return response()->json($query);
    }

    public function destroy($function_room_id)
    {
        $query = FunctionRoom::findOrFail($function_room_id);
        $query->delete();

        return response()->json($query);
    }

    public function delete_image($function_image_id)
    {
        $query = FunctionImage::findOrFail($function_image_id);
        $query->delete();

        return response()->json($query);
    }
}
